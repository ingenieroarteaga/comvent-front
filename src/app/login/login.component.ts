import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenStorage } from '../auth/token.storage';
import { AuthService } from '../auth/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  constructor(private router: Router, private authService: AuthService, private token: TokenStorage) {
  }
  username: string;
  password: string;

  ngOnInit() {
  }

  

  ingresar(): void {
    this.authService.attemptAuth(this.username, this.password).subscribe(
      data => {
        this.token.saveToken(data.token);
        window.location.reload();
        this.router.navigate(['inicio']);
      }
    );

  }

}
