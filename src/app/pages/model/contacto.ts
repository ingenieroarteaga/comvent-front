import { TipoContacto } from "../clasificadores/tipo-contacto/tipo-contacto";
import { Persona } from "./persona/persona";

export class Contacto {
    public id: number;
    public descripcion: string;
    public tipoContacto: TipoContacto;
    public personaList: Persona[];
    
}
