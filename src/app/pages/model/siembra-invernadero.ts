import { RemitoDetalle } from "./remito-detalle";
import { Invernadero } from "../herramienta/invernadero/invernadero";
import { SiembraDetalle } from "./siembra-detalle";

export class SiembraInvernadero {
    public id: number;
    public fecha: Date;
    public plantas: number;
    public remitoDetalleList: RemitoDetalle[];
    public invernadero: Invernadero;
    public siembraDetalle: SiembraDetalle;

}
