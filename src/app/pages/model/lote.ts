import { Variedad } from "../herramienta/variedad/variedad";
import { Batch } from "./batch/batch";

export class Lote {
    public id: number;
    public numero: string;
    public descripcion: string;
    public variedad: Variedad;
    public batchList: Batch[];
 
}
