import { Fletero } from "../../personas/fletero/fletero";
import { TipoDocumento } from "../../clasificadores/tipo-documento/tipo-documento";
import { Cliente } from "../../personas/cliente/cliente";
import { Proveedor } from "../../personas/proveedor/proveedor";
import { TipoPersona } from "../tipo-persona/tipo-persona";
import { ProveedorBandeja } from "../../personas/proveedor-bandeja/proveedor-bandeja";
import { PersonaDomicilio } from "../persona-domicilio";
import { Contacto } from "../contacto";

export class Persona {
    public id: number;
    public nombre: string;
    public apellido: string;
    public cuit: number;
    public dni: number;
    public fechaNac: Date;
    public mail: string;
    public telefono: string;
    public contactoReferencia: string;
    public contactoList: Contacto[];
    public fleteroList: Fletero[];
    public tipoDocumento: TipoDocumento;
    public tipoPersona: TipoPersona;
    public proveedorBandejaList: ProveedorBandeja[];
    public clienteList: Cliente[];
    public personaDomicilioList: PersonaDomicilio[];
    public proveedorList: Proveedor[];

}
