import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Persona } from './persona';

@Injectable()
export class PersonaService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getPersonaByCuit(cuit: number): Observable<Persona> {
    return this.http.get<Persona>(`${this.url + 'persona-cuit'}/${cuit}`)
  }

}
