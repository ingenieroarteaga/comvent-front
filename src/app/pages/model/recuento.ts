import { SiembraDetalle } from './siembra-detalle';
import { RecuentoDetalle } from './recuento-detalle';

export class Recuento {
    public id: number;
    public fecha: Date;
    public cantidad: number;
    public observacion: string;
    public siembraDetalle: SiembraDetalle;
    public recuentoDetalleList: RecuentoDetalle[];


}
