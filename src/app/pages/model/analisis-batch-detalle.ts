import { AnalisisBatch } from "../produccion/analisis-batch/analisis-batch";
import { TipoAnalisisBatch } from "../herramienta/tipo-analisis-batch/tipo-analisis-batch";

export class AnalisisBatchDetalle {
    public id: number;
    public fecha: Date;
    public valor: number;
    public observaciones: string;
    public analisisBatch: AnalisisBatch;
    public tipoAnalisisBatch: TipoAnalisisBatch;

}
