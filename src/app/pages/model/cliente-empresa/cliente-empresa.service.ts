import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ClienteEmpresa } from './cliente-empresa';
import { map } from 'rxjs/operators';

@Injectable()
export class ClienteEmpresaService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getClientesEmpresas(): Observable<ClienteEmpresa[]> {
    return this.http.get(this.url + 'clientes-empresas').pipe(
      map(response => response as ClienteEmpresa[])
    );
  }

  getClientesEmpresasActivos(): Observable<ClienteEmpresa[]> {
    return this.http.get(this.url + 'clientes-empresas-activos').pipe(
      map(response => response as ClienteEmpresa[])
    );
  }

}
