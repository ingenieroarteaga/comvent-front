import { Cliente } from "../../personas/cliente/cliente";
import { Empresa } from "../../personas/empresa/empresa";
import { Remito } from "../../produccion/remito/remito";
import { Pedido } from "../../produccion/pedido/pedido";

export class ClienteEmpresa {
    public id: number;
    public fechaAlta: Date;
    public fechaBaja: Date;
    public cliente: Cliente;
    public empresa: Empresa;
    public remitoList: Remito[];
    public pedidoList: Pedido[];
}
