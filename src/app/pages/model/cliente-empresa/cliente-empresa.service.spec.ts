import { TestBed, inject } from '@angular/core/testing';
import { ClienteEmpresaService } from './cliente-empresa.service';

describe('ClienteEmpresaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClienteEmpresaService]
    });
  });

  it('should be created', inject([ClienteEmpresaService], (service: ClienteEmpresaService) => {
    expect(service).toBeTruthy();
  }));
});
