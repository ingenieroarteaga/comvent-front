import { PersonaDomicilioPk } from "./persona-domicilio-pk";
import { Persona } from "./persona/persona";
import { Domicilio } from "./domicilio";

export class PersonaDomicilio {
    public id: PersonaDomicilioPk;
    public activo: number;
    public domicilio: Domicilio;
    public persona: Persona;

}
