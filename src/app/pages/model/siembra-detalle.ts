import { Recuento } from './recuento';
import { SiembraInvernadero } from './siembra-invernadero';
import { Siembra } from '../produccion/siembra/siembra';
import { PedidoDetalle } from './pedido-detalle/pedido-detalle';
import { Identificador } from '../produccion/identificador/identificador';

export class SiembraDetalle {
    public id: number;
    public bandejasOrdenadas: number;
    public finalSiembra: Date;
    public bandejasSembradas: number;
    public cantidadSemillas: number;
    public recuentoList: Recuento[];
    public siembraInvernaderoList: SiembraInvernadero[];
    public pedidoDetalle: PedidoDetalle;
    public identificador: Identificador;
    public siembra: Siembra;

}
