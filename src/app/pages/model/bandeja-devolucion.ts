import { BandejaCuenta } from "./bandeja-cuenta";
import { CuentaFleteroBand } from "../administracion/cuenta-fletero-band/cuenta-fletero-band";

export class BandejaDevolucion {
    public id: number;
    public fecha: Date;
    public cantidad: number;
    public bandejaCuenta: BandejaCuenta;
    public cuentaFleteroBand: CuentaFleteroBand;
}
