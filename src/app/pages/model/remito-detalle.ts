import { Remito } from "../produccion/remito/remito";
import { SiembraInvernadero } from "./siembra-invernadero";
import { BandejaCuenta } from "./bandeja-cuenta";

export class RemitoDetalle {
    public id: number;
    public cantidadPlantas: number;
    public montoPlanta: number;
    public recuentoFinca: number;
    public remito: Remito;
    public siembraInvernadero: SiembraInvernadero;
    public bandejaCuentaList: BandejaCuenta[];

}
