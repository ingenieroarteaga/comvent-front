import { Distrito } from "../clasificadores/distrito/distrito";
import { TipoDomicilio } from "../clasificadores/tipo-domicilio/tipo-domicilio";
import { Finca } from "../personas/finca/finca";
import { Sucursal } from "../herramienta/sucursal/sucursal";
import { Entidad } from "../herramienta/entidad/entidad";
import { PersonaDomicilio } from "./persona-domicilio";

export class Domicilio {
    public id: number;
    public calle: string;
    public numero: number;
    public piso: string;
    public departamento: string;
    public referencia: string;
    public coordenadas: string;
    public distrito: Distrito;
    public tipoDomicilio: TipoDomicilio;
    public fincaList: Finca[];
    public personaDomicilioList: PersonaDomicilio[];
    public sucursalList: Sucursal[];
    public entidadList: Entidad[];
 
}
