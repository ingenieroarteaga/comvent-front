import { Recuento } from './recuento';

export class RecuentoDetalle {
    public id: number;
    public cantidad: number;
    public recuento: Recuento;

}
