import { Compra } from "../produccion/compra/compra";
import { CuentaProveedor } from "../administracion/cuenta-proveedor/cuenta-proveedor";

export class CuentaProveedorDet {
    public id: number;
    public fecha: Date;
    public monto: number;
    public montoPago: number;
    public fechaBaja: Date;
    public compra: Compra;
    public cuentaProveedor: CuentaProveedor;
}
