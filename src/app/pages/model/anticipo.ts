import { Pago } from "../administracion/pago/pago";
import { Pedido } from "../produccion/pedido/pedido";

export class Anticipo {
    public id: number;
    public fecha: Date;
    public monto: number;
    public observacion: string;
    public fehcaBaja: Date;
    public pago: Pago;
    public pedido: Pedido;
}
