import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BandejaStock } from './bandeja-stock';

@Injectable()
export class BandejaStockService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getBandejaStocks(): Observable<BandejaStock[]> {
    return this.http.get(this.url + 'bandejas-stocks').pipe(
      map(response => response as BandejaStock[])
    );
  }

  

}
