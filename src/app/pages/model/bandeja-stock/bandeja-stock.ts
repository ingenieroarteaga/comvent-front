import { PedidoDetalle } from '../pedido-detalle/pedido-detalle';
import { Bandeja } from '../../herramienta/bandeja/bandeja';
import { Sucursal } from '../../herramienta/sucursal/sucursal';
import { BandejaCuenta } from '../bandeja-cuenta';
import { Identificador } from '../../produccion/identificador/identificador';


export class BandejaStock {
    public id: number;
    public cantidad: number;
    public pedidoDetalleList: PedidoDetalle[];
    public bandeja: Bandeja;
    public sucursal: Sucursal;
    public bandejaCuentaList: BandejaCuenta[];
    public identificadorList: Identificador[];

}
