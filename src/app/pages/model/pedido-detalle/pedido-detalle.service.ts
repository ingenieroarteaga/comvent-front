import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { PedidoDetalle } from './pedido-detalle';

@Injectable()
export class PedidoDetalleService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getPedidoDetalles(): Observable<PedidoDetalle[]> {
    return this.http.get(this.url + 'pedidos-detalles').pipe(
      map(response => response as PedidoDetalle[])
    );
  }

  getPedidoDetallesPendientes(): Observable<PedidoDetalle[]> {
    return this.http.get(this.url + 'pedidos-detalles-pendientes').pipe(
      map(response => response as PedidoDetalle[])
    );
  }

}
