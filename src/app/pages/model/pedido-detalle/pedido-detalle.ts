import { Pedido } from "../../produccion/pedido/pedido";
import { BandejaStock } from "../bandeja-stock/bandeja-stock";
import { Variedad } from "../../herramienta/variedad/variedad";
import { SiembraDetalle } from "../siembra-detalle";

export class PedidoDetalle {
    public id: number;
    public semanaEntrega: string;
    public semanaSiembra: string;
    public dias: number;
    public porcGerminacion: number;
    public cantidadPlantas: number;
    public cantidadBandeja: number;
    public precioUnitario: number;
    public precioProduccion: number;
    public subTotal: number;
    public siembraDetalle: SiembraDetalle;
    public pedido: Pedido;
    public bandejaStock: BandejaStock;
    public variedad: Variedad;

}
