import { TestBed, inject } from '@angular/core/testing';
import { PedidoDetalleService } from './pedido-detalle.service';

describe('PedidoDetalleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PedidoDetalleService]
    });
  });

  it('should be created', inject([PedidoDetalleService], (service: PedidoDetalleService) => {
    expect(service).toBeTruthy();
  }));
});
