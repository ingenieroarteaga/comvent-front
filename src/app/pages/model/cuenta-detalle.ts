import { Cuenta } from "../administracion/cuenta/cuenta";
import { Remito } from "../produccion/remito/remito";

export class CuentaDetalle {
    public id: number;
    public fecha: Date;
    public monto: number;
    public montoPago: number;
    public fechaBaja: Date;
    public cuenta: Cuenta;
    public remito: Remito;
}
