import { Compra } from "../produccion/compra/compra";
import { Batch } from "./batch/batch";

export class CompraDetalle {
    public id: number;
    public cantEnvases: number;
    public cantidad: number;
    public precioUnitario: number;
    public porcBonif: number;
    public montoBonif: number;
    public subTotal: number;
    public porcIva: number;
    public montoIva: number;
    public subTotalIva: number;
    public batch: Batch;
    public compra: Compra;
}
