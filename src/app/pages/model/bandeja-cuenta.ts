import { BandejaDevolucion } from "./bandeja-devolucion";
import { BandejaStock } from "./bandeja-stock/bandeja-stock";
import { CuentaFleteroBand } from "../administracion/cuenta-fletero-band/cuenta-fletero-band";
import { RemitoDetalle } from "./remito-detalle";

export class BandejaCuenta {
    public id: number;
    public cantidad: number;
    public cantidadDevuelta: number;
    public bandejaDevolucionList: BandejaDevolucion[];
    public bandejaStock: BandejaStock;
    public cuentaFleteroBand: CuentaFleteroBand;
    public remitoDetalle: RemitoDetalle;
}
