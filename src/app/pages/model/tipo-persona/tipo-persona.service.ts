import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { TipoPersona } from './tipo-persona';
@Injectable()
export class TipoPersonaService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getTipoPersonas(): Observable<TipoPersona[]> {
    return this.http.get(this.url + 'tipos-personas').pipe(
      map(response => response as TipoPersona[])
    );
  }

  getTipoPersona(id: number): Observable<TipoPersona> {
    return this.http.get<TipoPersona>(`${this.url + 'tipo-persona'}/${id}`)
  }

  create(tipoPersona: TipoPersona): Observable<TipoPersona> {
    return this.http.post<TipoPersona>(this.url + 'tipo-persona', tipoPersona, { headers: this.httpHeaders })
  }

  update(tipoPersona: TipoPersona): Observable<TipoPersona> {
    return this.http.put<TipoPersona>(`${this.url + 'tipo-persona'}/${tipoPersona.id}`, tipoPersona, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<TipoPersona> {
    return this.http.delete<TipoPersona>(`${this.url + 'tipo-persona'}/${id}`, { headers: this.httpHeaders })
  }
}
