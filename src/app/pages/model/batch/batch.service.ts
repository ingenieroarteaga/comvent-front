import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Batch } from './batch';


@Injectable()
export class BatchService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getBatchs(): Observable<Batch[]> {
    return this.http.get(this.url + 'batchs').pipe(
      map(response => response as Batch[])
    );
  }

  getBatchByNumero(numero: string): Observable<Batch> {
    return this.http.get<Batch>(`${this.url + 'batch-numero'}/${numero}`)
  }


}
