import { Calendario } from "../../herramienta/calendario/calendario";
import { Lote } from "../lote";
import { AnalisisBatch } from "../../produccion/analisis-batch/analisis-batch";
import { StockSemilla } from "../../produccion/stock-semilla/stock-semilla";
import { CompraDetalle } from "../compra-detalle";

export class Batch {
    public id: number;
    public numero: string;
    public descripcion: string;
    public cantidad: number;
    public podGerminacion: number;
    public pureza: number;
    public fechaPrueba: Date;
    public capacidadEnvases: number;
    public tratamiento: string;
    public calendario: Calendario;
    public lote: Lote;
    public analisisBatchList: AnalisisBatch[];
    public stockSemillaList: StockSemilla[];
    public compraDetalleList: CompraDetalle[];
    
 
}
