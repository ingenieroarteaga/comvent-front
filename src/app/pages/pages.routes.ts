import {  RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { InicioComponent } from './inicio/inicio.component';


//ADMINISTRACION
import { AdministracionComponent } from './administracion/administracion.component';
import { CajaComponent } from './administracion/caja/caja.component';
import { ComprobanteComponent } from './administracion/comprobante/comprobante.component';
import { CuentaComponent } from './administracion/cuenta/cuenta.component';
import { CuentaFleteroComponent } from './administracion/cuenta-fletero/cuenta-fletero.component';
import { CuentaFleteroBandComponent } from './administracion/cuenta-fletero-band/cuenta-fletero-band.component';
import { CuentaProveedorComponent } from './administracion/cuenta-proveedor/cuenta-proveedor.component';
import { ListaPrecioComponent } from './administracion/lista-precio/lista-precio.component';
import { PagoComponent } from './administracion/pago/pago.component';
import { PagoProveedorComponent } from './administracion/pago-proveedor/pago-proveedor.component';

// Produccion
import { CompraComponent } from './produccion/compra/compra.component';
import { ModalCompra } from './produccion/compra/modal-compra.component';
import { DetalleCompraComponent } from './produccion/compra/detalle-compra.component';
import { ProduccionComponent } from './produccion/produccion.component';
import { PedidoComponent } from './produccion/pedido/pedido.component';
import { ModalPedido } from './produccion/pedido/modal-pedido.component';
import { DetallePedidoComponent } from './produccion/pedido/detalle-pedido.component';
import { RemitoComponent } from './produccion/remito/remito.component';
import { SiembraComponent } from './produccion/siembra/siembra.component';
import { ModalSiembra } from './produccion/siembra/modal-siembra.component';
import { DetalleSiembraComponent } from './produccion/siembra/detalle-siembra.component';
import { StockSemillaComponent } from './produccion/stock-semilla/stock-semilla.component';
import { ModalStockSemilla } from './produccion/stock-semilla/modal-stock-semilla.component';
import { ModalStockInicial } from './produccion/stock-semilla/modal-stock-inicial.component';
import { AnalisisBatchComponent } from './produccion/analisis-batch/analisis-batch.component';
import { ModalAnalisisBatch } from './produccion/analisis-batch/modal-analisis-batch.component';


// Clasificadores
import { PaisComponent } from './clasificadores/pais/pais.component';
import { ClasificadoresComponent } from './clasificadores/clasificadores.component';
import { ModalPais } from './clasificadores/pais/modal-pais.component';
import { ProvinciaComponent } from './clasificadores/provincia/provincia.component';
import { ModalProvincia } from './clasificadores/provincia/modal-provincia.component';
import { LocalidadComponent } from './clasificadores/localidad/localidad.component';
import { ModalLocalidad } from './clasificadores/localidad/modal-localidad.component';
import { ZonaComponent } from './clasificadores/zona/zona.component';
import { ModalZona } from './clasificadores/zona/modal-zona.component';
import { DistritoComponent } from './clasificadores/distrito/distrito.component';
import { ModalDistrito } from './clasificadores/distrito/modal-distrito.component';
import { TipoDomicilioComponent } from './clasificadores/tipo-domicilio/tipo-domicilio.component';
import { ModalTipoDomicilio } from './clasificadores/tipo-domicilio/modal-tipo-domicilio.component';
import { TipoContactoComponent } from './clasificadores/tipo-contacto/tipo-contacto.component';
import { TipoDocumentoComponent } from './clasificadores/tipo-documento/tipo-documento.component';
import { ModalTipoDocumento } from './clasificadores/tipo-documento/modal-tipo-documento.component';
import { ModalTipoContacto } from './clasificadores/tipo-contacto/modal-tipo-contacto.component';


// Personas
import { PersonasComponent } from './personas/personas.component';
import { EmpresaComponent } from './personas/empresa/empresa.component';
import { ModalEmpresa } from './personas/empresa/modal-empresa.component';
import { ClienteComponent } from './personas/cliente/cliente.component';
import { ModalCliente } from './personas/cliente/modal-cliente.component';
import { ProveedorComponent } from './personas/proveedor/proveedor.component';
import { ModalProveedor } from './personas/proveedor/modal-proveedor.component';
import { ProveedorBandejaComponent } from './personas/proveedor-bandeja/proveedor-bandeja.component';
import { ModalProveedorBandeja } from './personas/proveedor-bandeja/modal-proveedor-bandeja.component';
import { FleteroComponent } from './personas/fletero/fletero.component';
import { ModalFletero } from './personas/fletero/modal-fletero.component';
import { FincaComponent } from './personas/finca/finca.component';
import { ModalFinca } from './personas/finca/modal-finca.component';


// Herramientas
import { HerramientaComponent } from './herramienta/herramienta.component';
import { TipoAnalisisBatchComponent } from './herramienta/tipo-analisis-batch/tipo-analisis-batch.component';
import { ModalTipoAnalisisBatch } from './herramienta/tipo-analisis-batch/modal-tipo-analisis-batch.component';
import { BandejaComponent } from './herramienta/bandeja/bandeja.component';
import { ModalBandeja } from './herramienta/bandeja/modal-bandeja.component';
import { TipoBandejaComponent } from './herramienta/tipo-bandeja/tipo-bandeja.component';
import { ModalTipoBandeja } from './herramienta/tipo-bandeja/modal-tipo-bandeja.component';
import { CalendarioComponent } from './herramienta/calendario/calendario.component';
import { ModalCalendario } from './herramienta/calendario/modal-calendario.component';
import { EspecieComponent } from './herramienta/especie/especie.component';
import { ModalEspecie } from './herramienta/especie/modal-especie.component';
import { VariedadComponent } from './herramienta/variedad/variedad.component';
import { ModalVariedad } from './herramienta/variedad/modal-variedad.component';
import { EntidadComponent } from './herramienta/entidad/entidad.component';
import { ModalEntidad } from './herramienta/entidad/modal-entidad.component';
import { SucursalComponent } from './herramienta/sucursal/sucursal.component';
import { ModalSucursal } from './herramienta/sucursal/modal-sucursal.component';
import { InvernaderoComponent } from './herramienta/invernadero/invernadero.component';
import { ModalInvernadero } from './herramienta/invernadero/modal-invernadero.component';
import { TipoMovStockSemComponent } from './herramienta/tipo-mov-stock-sem/tipo-mov-stock-sem.component';
import { ModalTipoMovStockSem } from './herramienta/tipo-mov-stock-sem/modal-tipo-mov-stock-sem.component';


// Perfil
import { PerfilComponent } from './perfil/perfil.component';
import { IdentificadorComponent } from './produccion/identificador/identificador.component';
import { ModalIdentificador } from './produccion/identificador/modal-identificador.component';
import { DetalleIdentificadorComponent } from './produccion/identificador/detalle-identificador.component';



const pagesRoutes: Routes = [
    {
        path: '', component: PagesComponent,
        children: [
            { path: 'administracion', component: AdministracionComponent },
            { path: 'inicio', component: InicioComponent },

            /////////// PERSONAS ////////////////////////////////////////////
            {
                path: 'personas',
                component: PersonasComponent,
                children: [
                    //
                    {
                        path: 'empresas',
                        component: EmpresaComponent
                    },
                    {
                        path: 'empresa',
                        component: EmpresaComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalEmpresa
                            },
                            {
                                path: 'editar/:id',
                                component: ModalEmpresa
                            }
                        ]
                    },
                    //
                    {
                        path: 'clientes',
                        component: ClienteComponent
                    },
                    {
                        path: 'cliente',
                        component: ClienteComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalCliente
                            },
                            {
                                path: 'editar/:id',
                                component: ModalCliente
                            }
                        ]
                    },
                    //
                    {
                        path: 'proveedores',
                        component: ProveedorComponent
                    },
                    {
                        path: 'proveedor',
                        component: ProveedorComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalProveedor
                            },
                            {
                                path: 'editar/:id',
                                component: ModalProveedor
                            }
                        ]
                    },
                    //
                    {
                        path: 'proveedores-bandejas',
                        component: ProveedorBandejaComponent
                    },
                    {
                        path: 'proveedor-bandeja',
                        component: ProveedorBandejaComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalProveedorBandeja
                            },
                            {
                                path: 'editar/:id',
                                component: ModalProveedorBandeja
                            }
                        ]
                    },
                    //
                    {
                        path: 'fleteros',
                        component: FleteroComponent
                    },
                    {
                        path: 'fletero',
                        component: FleteroComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalFletero
                            },
                            {
                                path: 'editar/:id',
                                component: ModalFletero
                            }
                        ]
                    },
                    //
                    {
                        path: 'fincas',
                        component: FincaComponent
                    },
                    {
                        path: 'finca',
                        component: FincaComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalFinca
                            },
                            {
                                path: 'editar/:id',
                                component: ModalFinca
                            }
                        ]
                    }
                ]
            },
            //// CLASIFICADORES ////////////////////////////////////////////
            {
                path: 'clasificadores',
                component: ClasificadoresComponent,
                children: [
                    {
                        path: 'paises',
                        component: PaisComponent
                    },
                    {
                        path: 'pais',
                        component: PaisComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalPais
                            },
                            {
                                path: 'editar/:id',
                                component: ModalPais
                            }
                        ]
                    },
                    //
                    {
                        path: 'provincias',
                        component: ProvinciaComponent
                    },
                    {
                        path: 'provincia',
                        component: ProvinciaComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalProvincia
                            },
                            {
                                path: 'editar/:id',
                                component: ModalProvincia
                            }
                        ]
                    },
                    //
                    {
                        path: 'localidades',
                        component: LocalidadComponent
                    },
                    {
                        path: 'localidad',
                        component: LocalidadComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalLocalidad
                            },
                            {
                                path: 'editar/:id',
                                component: ModalLocalidad
                            }
                        ]
                    },
                    //
                    {
                        path: 'zonas',
                        component: ZonaComponent
                    },
                    {
                        path: 'zona',
                        component: ZonaComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalZona
                            },
                            {
                                path: 'editar/:id',
                                component: ModalZona
                            }
                        ]
                    },
                    //
                    {
                        path: 'distritos',
                        component: DistritoComponent
                    },
                    {
                        path: 'distrito',
                        component: DistritoComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalDistrito
                            },
                            {
                                path: 'editar/:id',
                                component: ModalDistrito
                            }
                        ]
                    },
                    //
                    {
                        path: 'tipos-domicilios',
                        component: TipoDomicilioComponent
                    },
                    {
                        path: 'tipo-domicilio',
                        component: TipoDomicilioComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalTipoDomicilio
                            },
                            {
                                path: 'editar/:id',
                                component: ModalTipoDomicilio
                            }
                        ]
                    },
                    //
                    {
                        path: 'tipos-contactos',
                        component: TipoContactoComponent
                    },
                    {
                        path: 'tipo-contacto',
                        component: TipoContactoComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalTipoContacto
                            },
                            {
                                path: 'editar/:id',
                                component: ModalTipoContacto
                            }
                        ]
                    },
                    //
                    {
                        path: 'tipos-documentos',
                        component: TipoDocumentoComponent
                    },
                    {
                        path: 'tipo-documento',
                        component: TipoDocumentoComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalTipoDocumento
                            },
                            {
                                path: 'editar/:id',
                                component: ModalTipoDocumento
                            }
                        ]
                    }
                ]
            },
            /////////// HERRAMIENTAS ///////////////////////////////////
            {
                path: 'herramientas',
                component: HerramientaComponent,
                children: [
                    {
                        path: 'bandejas',
                        component: BandejaComponent
                    },
                    {
                        path: 'bandeja',
                        component: BandejaComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalBandeja
                            },
                            {
                                path: 'editar/:id',
                                component: ModalBandeja
                            }
                        ]
                    },
                    ///// Tipo Bandeja
                    {
                        path: 'tipos-bandejas',
                        component: TipoBandejaComponent
                    },
                    {
                        path: 'tipo-bandeja',
                        component: TipoBandejaComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalTipoBandeja
                            },
                            {
                                path: 'editar/:id',
                                component: ModalTipoBandeja
                            }
                        ]
                    },
                    ///// Tipo Analisis Batch
                    {
                        path: 'tipos-analisis-batchs',
                        component: TipoAnalisisBatchComponent
                    },
                    {
                        path: 'tipo-analisis-batch',
                        component: TipoAnalisisBatchComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalTipoAnalisisBatch
                            },
                            {
                                path: 'editar/:id',
                                component: ModalTipoAnalisisBatch
                            }
                        ]
                    },
                    ///// Tipo Movimiento Stock Semilla
                    {
                        path: 'tipos-movs-stocks-sem',
                        component: TipoMovStockSemComponent
                    },
                    {
                        path: 'tipo-mov-stock-sem',
                        component: TipoMovStockSemComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalTipoMovStockSem
                            },
                            {
                                path: 'editar/:id',
                                component: ModalTipoMovStockSem
                            }
                        ]
                    },
                    ////// Calendario
                    {
                        path: 'calendarios',
                        component: CalendarioComponent
                    },
                    {
                        path: 'calendario',
                        component: CalendarioComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalCalendario
                            },
                            {
                                path: 'editar/:id',
                                component: ModalCalendario
                            }
                        ]
                    },
                    ///// Especie
                    {
                        path: 'especies',
                        component: EspecieComponent
                    },
                    {
                        path: 'especie',
                        component: EspecieComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalEspecie
                            },
                            {
                                path: 'editar/:id',
                                component: ModalEspecie
                            }
                        ]
                    },
                    //
                    {
                        path: 'variedades',
                        component: VariedadComponent
                    },
                    {
                        path: 'variedad',
                        component: VariedadComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalVariedad
                            },
                            {
                                path: 'editar/:id',
                                component: ModalVariedad
                            }
                        ]
                    },
                    //
                    {
                        path: 'entidades',
                        component: EntidadComponent
                    },
                    {
                        path: 'entidad',
                        component: EntidadComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalEntidad
                            },
                            {
                                path: 'editar/:id',
                                component: ModalEntidad
                            }
                        ]
                    },
                    //
                    {
                        path: 'sucursales',
                        component: SucursalComponent
                    },
                    {
                        path: 'sucursal',
                        component: SucursalComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalSucursal
                            },
                            {
                                path: 'editar/:id',
                                component: ModalSucursal
                            }
                        ]
                    },
                    //
                    {
                        path: 'invernaderos',
                        component: InvernaderoComponent
                    },
                    {
                        path: 'invernadero',
                        component: InvernaderoComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalInvernadero
                            },
                            {
                                path: 'editar/:id',
                                component: ModalInvernadero
                            }
                        ]
                    }
                ]
            },

            ////////////////////////////////////////////////////////////////
            /////////// PRODUCCION /////////////////////////////////////////
            ////////////////////////////////////////////////////////////////
            {
                path: 'produccion',
                component: ProduccionComponent,
                children: [
                    //////////// Compra //////////
                    {
                        path: 'compras',
                        component: CompraComponent
                    },
                    {
                        path: 'compra',
                        component: CompraComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalCompra
                            },
                            {
                                path: 'editar/:id',
                                component: ModalCompra
                            }
                        ]
                    },
                    {
                        path: 'compra-detalle/:id',
                        component: DetalleCompraComponent
                    },
                    /////////// Pedidos
                    {
                        path: 'pedidos',
                        component: PedidoComponent
                    },
                    {
                        path: 'pedido',
                        component: PedidoComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalPedido
                            },
                            {
                                path: 'editar/:id',
                                component: ModalPedido
                            }
                        ]
                    },
                    {
                        path: 'pedido-detalle/:id',
                        component: DetallePedidoComponent
                    },
                    //// Stock Semillas
                    {
                        path: 'stock-semillas',
                        component: StockSemillaComponent
                    },
                    {
                        path: 'stock-semilla',
                        component: StockSemillaComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalStockSemilla
                            },
                            {
                                path: 'editar/:id',
                                component: ModalStockSemilla
                            },
                            // inicial
                            {
                                path: 'inicial',
                                component: ModalStockInicial
                            }

                        ]
                    },
                    ///// Identificadores
                    {
                        path: 'identificadores',
                        component: IdentificadorComponent
                    },
                    {
                        path: 'identificador',
                        component: IdentificadorComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalIdentificador
                            },
                            {
                                path: 'editar/:id',
                                component: ModalIdentificador
                            }
                        ]
                    },
                    {
                        path: 'identificador-detalle/:id',
                        component: DetalleIdentificadorComponent
                    },
                    ///// Analisis BAtch
                    {
                        path: 'analisis-batchs',
                        component: AnalisisBatchComponent
                    },
                    {
                        path: 'analisis-batch',
                        component: AnalisisBatchComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalAnalisisBatch
                            },
                            {
                                path: 'editar/:id',
                                component: ModalAnalisisBatch
                            }
                        ]
                    },
                    ///// REMITOS
                    {
                        path: 'remitos',
                        component: RemitoComponent
                    },
                    {
                        path: 'remito',
                        component: RemitoComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalTipoBandeja
                            },
                            {
                                path: 'editar/:id',
                                component: ModalTipoBandeja
                            }
                        ]
                    },
                    //
                    {
                        path: 'siembras',
                        component: SiembraComponent
                    },
                    {
                        path: 'siembra',
                        component: SiembraComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalSiembra
                            },
                            {
                                path: 'editar/:id',
                                component: ModalSiembra
                            }
                        ]
                    },
                    {
                        path: 'siembra-detalle/:id',
                        component: DetalleSiembraComponent
                    }
                ]
            },
            /////////// ADMINISTRACION ///////////////////////////////////

            {
                path: 'administracion',
                component: AdministracionComponent,
                children: [
                    {
                        path: 'cajas',
                        component: CajaComponent
                    },
                    {
                        path: 'caja',
                        component: CajaComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalBandeja
                            },
                            {
                                path: 'editar/:id',
                                component: ModalBandeja
                            }
                        ]
                    },
                    //
                    {
                        path: 'comprobantes',
                        component: ComprobanteComponent
                    },
                    {
                        path: 'comprobante',
                        component: ComprobanteComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalEspecie
                            },
                            {
                                path: 'editar/:id',
                                component: ModalEspecie
                            }
                        ]
                    },
                    //
                    {
                        path: 'cuentas',
                        component: CuentaComponent
                    },
                    {
                        path: 'cuenta',
                        component: CuentaComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalVariedad
                            },
                            {
                                path: 'editar/:id',
                                component: ModalVariedad
                            }
                        ]
                    },
                    //
                    {
                        path: 'cuentas-fleteros',
                        component: CuentaFleteroComponent
                    },
                    {
                        path: 'cuenta-fletero',
                        component: CuentaFleteroComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalBandeja
                            },
                            {
                                path: 'editar/:id',
                                component: ModalBandeja
                            }
                        ]
                    },
                    //

                    {
                        path: 'cuentas-fleteros-band',
                        component: CuentaFleteroBandComponent
                    },
                    {
                        path: 'cuenta-fletero-band',
                        component: CuentaFleteroBandComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalEntidad
                            },
                            {
                                path: 'editar/:id',
                                component: ModalEntidad
                            }
                        ]
                    },
                    //
                    {
                        path: 'cuentas-proveedores',
                        component: CuentaProveedorComponent
                    },
                    {
                        path: 'cuenta-proveedor',
                        component: CuentaProveedorComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalSucursal
                            },
                            {
                                path: 'editar/:id',
                                component: ModalSucursal
                            }
                        ]
                    },
                    //
                    {
                        path: 'listas-precios',
                        component: ListaPrecioComponent
                    },
                    {
                        path: 'lista-precio',
                        component: ListaPrecioComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalInvernadero
                            },
                            {
                                path: 'editar/:id',
                                component: ModalInvernadero
                            }
                        ]
                    },
                    {
                        path: 'pagos',
                        component: PagoComponent
                    },
                    {
                        path: 'pago',
                        component: PagoComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalInvernadero
                            },
                            {
                                path: 'editar/:id',
                                component: ModalInvernadero
                            }
                        ]
                    },
                    {
                        path: 'pagos-proveedores',
                        component: PagoProveedorComponent
                    },
                    {
                        path: 'pago-proveedor',
                        component: PagoProveedorComponent,
                        children: [
                            {
                                path: 'agregar',
                                component: ModalInvernadero
                            },
                            {
                                path: 'editar/:id',
                                component: ModalInvernadero
                            }
                        ]
                    }
                ]
            },

            {
                path: 'perfil',
                component: PerfilComponent
            },
            { path: '', redirectTo: '/login', pathMatch: 'full' },

        ]
    }
];

export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );

