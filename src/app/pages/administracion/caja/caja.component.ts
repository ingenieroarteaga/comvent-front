import { GLOBAL } from '../../global';
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Caja } from './caja';
import { CajaService } from './caja.service';

@Component({
  selector: 'app-caja',
  templateUrl: './caja.component.html',
  styleUrls: ['./caja.component.css'],
  providers: [CajaService]
})
export class CajaComponent implements OnInit {
  public titulo: string;
  cajas: Caja[];


  constructor(
    private CajaService:CajaService,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private router: Router

  ) { }

  ngOnInit() {
    this.CajaService.getCajas().subscribe(
    cajas => this.cajas = cajas
    );
  }

}
