import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Caja } from './caja';

@Injectable()
export class CajaService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getCajas(): Observable<Caja[]> {
    return this.http.get(this.url + 'cajas').pipe(
      map(response => response as Caja[])
    );
  }

  getCaja(id: number): Observable<Caja> {
    return this.http.get<Caja>(`${this.url + 'caja'}/${id}`)
  }

  create(caja: Caja): Observable<Caja> {
    return this.http.post<Caja>(this.url + 'caja', caja, { headers: this.httpHeaders })
  }

  update(caja: Caja): Observable<Caja> {
    return this.http.put<Caja>(this.url + 'caja', caja, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Caja> {
    return this.http.delete<Caja>(`${this.url + 'caja'}/${id}`, { headers: this.httpHeaders })
  }

}
