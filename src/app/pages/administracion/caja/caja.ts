import { PagoProveedor } from "../pago-proveedor/pago-proveedor";
import { Pago } from "../pago/pago";

export class Caja {
    public id: number;
    public fecha: Date;
    public debe: number;
    public haber: number;
    public pagoProveedorList: PagoProveedor[];
    public pagoList: Pago[];
}
