import { TestBed, inject } from '@angular/core/testing';

import { CuentaFleteroBandService } from './cuenta-fletero-band.service';

describe('CuentaFleteroBandService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CuentaFleteroBandService]
    });
  });

  it('should be created', inject([CuentaFleteroBandService], (service: CuentaFleteroBandService) => {
    expect(service).toBeTruthy();
  }));
});
