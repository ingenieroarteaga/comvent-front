import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuentaFleteroBandComponent } from './cuenta-fletero-band.component';

describe('CuentaFleteroBandComponent', () => {
  let component: CuentaFleteroBandComponent;
  let fixture: ComponentFixture<CuentaFleteroBandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuentaFleteroBandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuentaFleteroBandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
