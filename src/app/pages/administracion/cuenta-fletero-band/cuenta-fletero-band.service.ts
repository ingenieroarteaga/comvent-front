import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { CuentaFleteroBand } from './cuenta-fletero-band';

@Injectable()
export class CuentaFleteroBandService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getCuentasFleterosBand(): Observable<CuentaFleteroBand[]> {
    return this.http.get(this.url + 'cuentas-fleteros-band').pipe(
      map(response => response as CuentaFleteroBand[])
    );
  }

  getCuentaFleteroBand(id: number): Observable<CuentaFleteroBand> {
    return this.http.get<CuentaFleteroBand>(`${this.url + 'cuenta-fletero-band'}/${id}`)
  }

  create(cuentaFleteroBand: CuentaFleteroBand): Observable<CuentaFleteroBand> {
    return this.http.post<CuentaFleteroBand>(this.url + 'cuenta-fletero-band', cuentaFleteroBand, { headers: this.httpHeaders })
  }

  update(cuentaFleteroBand: CuentaFleteroBand): Observable<CuentaFleteroBand> {
    return this.http.put<CuentaFleteroBand>(this.url + 'cuenta-fletero-band', cuentaFleteroBand, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<CuentaFleteroBand> {
    return this.http.delete<CuentaFleteroBand>(`${this.url + 'cuenta-fletero-band'}/${id}`, { headers: this.httpHeaders })
  }

}
