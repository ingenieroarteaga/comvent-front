import { Fletero } from "../../personas/fletero/fletero";
import { BandejaDevolucion } from "../../model/bandeja-devolucion";
import { BandejaCuenta } from "../../model/bandeja-cuenta";

export class CuentaFleteroBand {
    public id: number;
    public fecha: Date;
    public debe: number;
    public haber: number;
    public fletero: Fletero;
    public bandejaDevolucionList: BandejaDevolucion[];
    public bandejaCuentaList: BandejaCuenta[];

}
