import { Fletero } from "../../personas/fletero/fletero";

export class CuentaFletero {
    public id: number;
    public fecha: Date;
    public monto: number;
    public observacion: string;
    public fletero: Fletero;
}
