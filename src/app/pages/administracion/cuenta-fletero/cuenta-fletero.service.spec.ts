import { TestBed, inject } from '@angular/core/testing';

import { CuentaFleteroService } from './cuenta-fletero.service';

describe('CuentaFleteroService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CuentaFleteroService]
    });
  });

  it('should be created', inject([CuentaFleteroService], (service: CuentaFleteroService) => {
    expect(service).toBeTruthy();
  }));
});
