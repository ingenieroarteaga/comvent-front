import { Component, OnInit } from '@angular/core';
import {CuentaFletero} from "./cuenta-fletero";
import {CuentaFleteroService} from "./cuenta-fletero.service";
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-cuenta-fletero',
  templateUrl: './cuenta-fletero.component.html',
  styleUrls: ['./cuenta-fletero.component.css'],
  providers: [CuentaFleteroService]
})
export class CuentaFleteroComponent implements OnInit {

  public titulo:string;
  cuentasFleteros: CuentaFletero[];

  constructor(
    private CuentasFleterosService:CuentaFleteroService,
    private _route: ActivatedRoute,
    private _router: Router

  ) { }

  ngOnInit() {
    this.CuentasFleterosService.getCuentaFleteros().subscribe(
      cuentasFleteros => this.cuentasFleteros = cuentasFleteros
    );
  }

}
