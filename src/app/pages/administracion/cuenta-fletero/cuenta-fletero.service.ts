import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { CuentaFletero } from './cuenta-fletero';

@Injectable()
export class CuentaFleteroService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getCuentaFleteros(): Observable<CuentaFletero[]> {
    return this.http.get(this.url + 'cuentas-fleteros').pipe(
      map(response => response as CuentaFletero[])
    );
  }

  getCuentaFletero(id: number): Observable<CuentaFletero> {
    return this.http.get<CuentaFletero>(`${this.url + 'cuenta-fletero'}/${id}`)
  }

  create(cuentaFletero: CuentaFletero): Observable<CuentaFletero> {
    return this.http.post<CuentaFletero>(this.url + 'cuenta-fletero', cuentaFletero, { headers: this.httpHeaders })
  }

  update(cuentaFletero: CuentaFletero): Observable<CuentaFletero> {
    return this.http.put<CuentaFletero>(this.url + 'cuenta-fletero', cuentaFletero, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<CuentaFletero> {
    return this.http.delete<CuentaFletero>(`${this.url + 'cuenta-fletero'}/${id}`, { headers: this.httpHeaders })
  }

}
