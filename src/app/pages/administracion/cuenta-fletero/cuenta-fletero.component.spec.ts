import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuentaFleteroComponent } from './cuenta-fletero.component';

describe('CuentaFleteroComponent', () => {
  let component: CuentaFleteroComponent;
  let fixture: ComponentFixture<CuentaFleteroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuentaFleteroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuentaFleteroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
