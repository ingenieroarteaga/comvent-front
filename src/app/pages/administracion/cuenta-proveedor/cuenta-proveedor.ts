import { PagoProveedor } from "../pago-proveedor/pago-proveedor";
import { Proveedor } from "../../personas/proveedor/proveedor";

export class CuentaProveedor {
    public id: number;
    public debe: number;
    public haber: number;
    public pagoProveedorList: PagoProveedor[];
    public proveedor: Proveedor;
    public cuentaProveedorDetList: CuentaProveedor[];
}
