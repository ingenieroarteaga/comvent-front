import { Component, OnInit } from '@angular/core';
import {CuentaProveedor} from "./cuenta-proveedor";
import {CuentaProveedorService} from "./cuenta-proveedor.service";
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-cuenta-proveedor',
  templateUrl: './cuenta-proveedor.component.html',
  styleUrls: ['./cuenta-proveedor.component.css'],
  providers: [CuentaProveedorService]
})
export class CuentaProveedorComponent implements OnInit {

  public titulo:string;
  public cuentasProveedores: CuentaProveedor[];

  constructor(
    private CuentaProveedorService:CuentaProveedorService,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.CuentaProveedorService.getCuentaProveedores().subscribe(
      cuentasProveedores => this.cuentasProveedores = cuentasProveedores
    );
  }

}
