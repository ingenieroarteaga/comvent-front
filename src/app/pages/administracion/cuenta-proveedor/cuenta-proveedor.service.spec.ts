import { TestBed, inject } from '@angular/core/testing';

import { CuentaProveedorService } from './cuenta-proveedor.service';

describe('CuentaProveedorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CuentaProveedorService]
    });
  });

  it('should be created', inject([CuentaProveedorService], (service: CuentaProveedorService) => {
    expect(service).toBeTruthy();
  }));
});
