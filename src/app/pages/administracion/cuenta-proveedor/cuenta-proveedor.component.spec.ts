import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuentaProveedorComponent } from './cuenta-proveedor.component';

describe('CuentaProveedorComponent', () => {
  let component: CuentaProveedorComponent;
  let fixture: ComponentFixture<CuentaProveedorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuentaProveedorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuentaProveedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
