import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { CuentaProveedor } from './cuenta-proveedor';
@Injectable()
export class CuentaProveedorService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getCuentaProveedores(): Observable<CuentaProveedor[]> {
    return this.http.get(this.url + 'cuentas-proveedores').pipe(
      map(response => response as CuentaProveedor[])
    );
  }

  getCuentaProveedor(id: number): Observable<CuentaProveedor> {
    return this.http.get<CuentaProveedor>(`${this.url + 'cuenta-proveedor'}/${id}`)
  }

  create(cuentaProveedor: CuentaProveedor): Observable<CuentaProveedor> {
    return this.http.post<CuentaProveedor>(this.url + 'cuenta-proveedor', cuentaProveedor, { headers: this.httpHeaders })
  }

  update(cuentaProveedor: CuentaProveedor): Observable<CuentaProveedor> {
    return this.http.put<CuentaProveedor>(this.url + 'cuenta-proveedor', cuentaProveedor, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<CuentaProveedor> {
    return this.http.delete<CuentaProveedor>(`${this.url + 'cuenta-proveedor'}/${id}`, { headers: this.httpHeaders })
  }

}
