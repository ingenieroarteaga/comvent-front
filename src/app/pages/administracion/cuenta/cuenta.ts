import { CuentaDetalle } from "../../model/cuenta-detalle";
import { Pago } from "../pago/pago";

export class Cuenta {
    public id: number;
    public debe: number;
    public haber: number;
    public cuentaDetalleList: CuentaDetalle[];
    public pagoList: Pago[];
}
