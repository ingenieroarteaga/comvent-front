import { Component, OnInit } from '@angular/core';
import {CuentaService} from './cuenta.service';
import {Cuenta} from "./cuenta";
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-cuenta',
  templateUrl: './cuenta.component.html',
  styleUrls: ['./cuenta.component.css'],
  providers: [CuentaService]

})
export class CuentaComponent implements OnInit {

  public titulo: string;
  cuentas: Cuenta[];

  constructor(
    private CuentaService: CuentaService,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.CuentaService.getCuentas().subscribe(
      cuentas => this.cuentas = cuentas
    );
  }

}
