import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Cuenta } from './cuenta';

@Injectable()
export class CuentaService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getCuentas(): Observable<Cuenta[]> {
    return this.http.get(this.url + 'cuentas').pipe(
      map(response => response as Cuenta[])
    );
  }

  getCuenta(id: number): Observable<Cuenta> {
    return this.http.get<Cuenta>(`${this.url + 'cuenta'}/${id}`)
  }

  create(cuenta: Cuenta): Observable<Cuenta> {
    return this.http.post<Cuenta>(this.url + 'cuenta', cuenta, { headers: this.httpHeaders })
  }

  update(cuenta: Cuenta): Observable<Cuenta> {
    return this.http.put<Cuenta>(this.url + 'cuenta', cuenta, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Cuenta> {
    return this.http.delete<Cuenta>(`${this.url + 'cuenta'}/${id}`, { headers: this.httpHeaders })
  }

}
