import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ListaPrecio } from './lista-precio';

@Injectable()
export class ListaPrecioService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })


  constructor( private http: HttpClient ) {
    this.url = GLOBAL.url;
  }

  getListaPrecios(): Observable<ListaPrecio[]> {
    return this.http.get(this.url + 'listas-precios').pipe(
      map(response => response as ListaPrecio[])
    );
  }

  getListaPrecio(id: number): Observable<ListaPrecio> {
    return this.http.get<ListaPrecio>(`${this.url + 'lista-precio'}/${id}`)
  }

  create(listaPrecio: ListaPrecio): Observable<ListaPrecio> {
    return this.http.post<ListaPrecio>(this.url + 'lista-precio', listaPrecio, { headers: this.httpHeaders })
  }

  update(listaPrecio: ListaPrecio): Observable<ListaPrecio> {
    return this.http.put<ListaPrecio>(this.url + 'lista-precio', listaPrecio, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<ListaPrecio> {
    return this.http.delete<ListaPrecio>(`${this.url + 'lista-precio'}/${id}`, { headers: this.httpHeaders })
  }

}
