import { Empresa } from "../../personas/empresa/empresa";
import { Variedad } from "../../herramienta/variedad/variedad";

export class ListaPrecio {
    public id: number;
    public monto: number;
    public empresa: Empresa;
    public variedad: Variedad;
}
