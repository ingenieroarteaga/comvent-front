import { GLOBAL } from '../../global';
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ListaPrecio } from './lista-precio';
import { ListaPrecioService } from './lista-precio.service';


@Component({
  selector: 'app-lista-precio',
  templateUrl: './lista-precio.component.html',
  styleUrls: ['./lista-precio.component.css'],
  providers:[ListaPrecioService]
})
export class ListaPrecioComponent implements OnInit {

  public titulo: string;
  listas: ListaPrecio[];


  constructor(
    private ListaPrecioService:ListaPrecioService,
    private route: ActivatedRoute,
    private router: Router

  ) { }

  ngOnInit() {
    this.ListaPrecioService.getListaPrecios().subscribe(
    listas => this.listas = listas
    );
  }

}
