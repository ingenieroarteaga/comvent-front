import { TestBed, inject } from '@angular/core/testing';

import { ListaPrecioService } from './lista-precio.service';

describe('ListaPrecioService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListaPrecioService]
    });
  });

  it('should be created', inject([ListaPrecioService], (service: ListaPrecioService) => {
    expect(service).toBeTruthy();
  }));
});
