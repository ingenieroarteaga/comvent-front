import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagoProveedorComponent } from './pago-proveedor.component';

describe('PagoProveedorComponent', () => {
  let component: PagoProveedorComponent;
  let fixture: ComponentFixture<PagoProveedorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagoProveedorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagoProveedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
