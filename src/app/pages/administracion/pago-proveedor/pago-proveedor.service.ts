import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { PagoProveedor } from './pago-proveedor';

@Injectable()
export class PagoProveedorService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getPagoProveedores(): Observable<PagoProveedor[]> {
    return this.http.get(this.url + 'pagos-proveedores').pipe(
      map(response => response as PagoProveedor[])
    );
  }

  getPagoProveedor(id: number): Observable<PagoProveedor> {
    return this.http.get<PagoProveedor>(`${this.url + 'pago-proveedor'}/${id}`)
  }

  create(pagoProveedor: PagoProveedor): Observable<PagoProveedor> {
    return this.http.post<PagoProveedor>(this.url + 'pago-proveedor', pagoProveedor, { headers: this.httpHeaders })
  }

  update(pagoProveedor: PagoProveedor): Observable<PagoProveedor> {
    return this.http.post<PagoProveedor>(this.url + 'pago-proveedor', pagoProveedor, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<PagoProveedor> {
    return this.http.delete<PagoProveedor>(`${this.url + 'pago-proveedor'}/${id}`, { headers: this.httpHeaders })
  }

}
