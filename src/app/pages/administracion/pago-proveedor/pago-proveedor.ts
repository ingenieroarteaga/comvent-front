import { Caja } from "../caja/caja";
import { CuentaProveedor } from "../cuenta-proveedor/cuenta-proveedor";

export class PagoProveedor {
    public id: number;
    public fecha: Date;
    public monto: number;
    public observacion: string;
    public fechaBaja: Date;
    public caja: Caja;
    public cuentaProveedor: CuentaProveedor;
}
