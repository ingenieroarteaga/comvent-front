import { Component, OnInit } from '@angular/core';
import {PagoProveedor} from "./pago-proveedor";
import {PagoProveedorService} from "./pago-proveedor.service";
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-pago-proveedor',
  templateUrl: './pago-proveedor.component.html',
  styleUrls: ['./pago-proveedor.component.css'],
  providers: [PagoProveedorService]
})
export class PagoProveedorComponent implements OnInit {

  public titulo:String;
  PagosProveedores: PagoProveedor[];

  constructor(
    private PagoProveedorService:PagoProveedorService,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.PagoProveedorService.getPagoProveedores().subscribe(
      PagosProveedores => this.PagosProveedores = PagosProveedores
    );
  }

}
