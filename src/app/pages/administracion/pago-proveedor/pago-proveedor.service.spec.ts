import { TestBed, inject } from '@angular/core/testing';

import { PagoProveedorService } from './pago-proveedor.service';

describe('PagoProveedorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PagoProveedorService]
    });
  });

  it('should be created', inject([PagoProveedorService], (service: PagoProveedorService) => {
    expect(service).toBeTruthy();
  }));
});
