import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Comprobante } from './comprobante';

@Injectable()
export class ComprobanteService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getComprobantes(): Observable<Comprobante[]> {
    return this.http.get(this.url + 'comprobantes').pipe(
      map(response => response as Comprobante[])
    );
  }

  getComprobante(id: number): Observable<Comprobante> {
    return this.http.get<Comprobante>(`${this.url + 'comprobante'}/${id}`)
  }

  create(comprobante: Comprobante): Observable<Comprobante> {
    return this.http.post<Comprobante>(this.url + 'comprobante', comprobante, { headers: this.httpHeaders })
  }

  update(comprobante: Comprobante): Observable<Comprobante> {
    return this.http.put<Comprobante>(this.url + 'comprobante', comprobante, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Comprobante> {
    return this.http.delete<Comprobante>(`${this.url + 'comprobante'}/${id}`, { headers: this.httpHeaders })
  }

}
