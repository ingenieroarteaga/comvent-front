import { Remito } from "../../produccion/remito/remito";

export class Comprobante {
    public id: number;
    public descripcion: string;
    public numero: number;
    public remitoList: Remito[];

}
