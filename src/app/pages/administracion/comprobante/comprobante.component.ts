import { GLOBAL } from '../../global';
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Comprobante } from './comprobante';
import { ComprobanteService } from './comprobante.service';

@Component({
  selector: 'app-comprobante',
  templateUrl: './comprobante.component.html',
  styleUrls: ['./comprobante.component.css'],
  providers: [ComprobanteService]
})
export class ComprobanteComponent implements OnInit {

  public titulo: string;
  comprobantes: Comprobante[]

  constructor(
    private ComprobanteService:ComprobanteService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.ComprobanteService.getComprobantes().subscribe(
    comprobantes => this.comprobantes = comprobantes
    );
  }

}
