import { Component, OnInit } from '@angular/core';
import {Pago} from "./pago";
import {PagoService} from "./pago.service";
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  styleUrls: ['./pago.component.css'],
  providers: [PagoService]
})
export class PagoComponent implements OnInit {

  public titulo:string;
  pagos: Pago[];

  constructor(
    private PagoService:PagoService,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.PagoService.getPagos().subscribe(
      pagos => this.pagos = pagos
    );
  }

}
