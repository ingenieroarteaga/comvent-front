import { Caja } from "../caja/caja";
import { Cuenta } from "../cuenta/cuenta";
import { Anticipo } from "../../model/anticipo";

export class Pago {
    public id: number;
    public fecha: Date;
    public monto: number;
    public observacion: string;
    public fehcaBaja: Date;
    public caja: Caja;
    public cuenta: Cuenta;
    public anticipoList: Anticipo[];

}
