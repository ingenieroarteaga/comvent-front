import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Pago } from './pago';

@Injectable()
export class PagoService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getPagos(): Observable<Pago[]> {
    return this.http.get(this.url + 'pagos').pipe(
      map(response => response as Pago[])
    );
  }

  getPago(id: number): Observable<Pago> {
    return this.http.get<Pago>(`${this.url + 'pago'}/${id}`)
  }

  create(pago: Pago): Observable<Pago> {
    return this.http.post<Pago>(this.url + 'pago', pago, { headers: this.httpHeaders })
  }

  update(pago: Pago): Observable<Pago> {
    return this.http.put<Pago>(this.url + 'pago', pago, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Pago> {
    return this.http.delete<Pago>(`${this.url + 'pago'}/${id}`, { headers: this.httpHeaders })
  }

}
