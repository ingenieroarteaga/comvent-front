import { Component, OnInit } from '@angular/core';
import { Params, Router, RouterModule, ActivatedRoute } from '@angular/router';
import { Distrito } from './distrito';
import { DistritoService } from './distrito.service';
import { FormControl } from "@angular/forms";

@Component({
  selector: 'app-distritos',
  templateUrl: './distrito.component.html',
  styleUrls: ['./distrito.component.css'],
  providers: [DistritoService]
})
export class DistritoComponent implements OnInit {
  distritos: Distrito[];
  myDistrito: FormControl = new FormControl();
  p : number;

  constructor(private distritoService: DistritoService,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.distritoService.getDistritos().subscribe(
      distritos => this.distritos = distritos
    );
  }
}
