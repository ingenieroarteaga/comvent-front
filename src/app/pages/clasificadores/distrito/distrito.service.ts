import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Distrito } from './distrito';
@Injectable()
export class DistritoService {
  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getDistritos(): Observable<Distrito[]> {
    return this.http.get(this.url + 'distritos').pipe(
      map(response => response as Distrito[])
    );
  }

  getDistrito(id: number): Observable<Distrito> {
    return this.http.get<Distrito>(`${this.url + 'distrito'}/${id}`)
  }

  create(distrito: Distrito): Observable<Distrito> {
    return this.http.post<Distrito>(this.url + 'distrito', distrito, { headers: this.httpHeaders })
  }

  update(distrito: Distrito): Observable<Distrito> {
    return this.http.put<Distrito>(this.url + 'distrito', distrito, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Distrito> {
    return this.http.delete<Distrito>(`${this.url + 'distrito'}/${id}`, { headers: this.httpHeaders })
  }


}
