import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Distrito } from "./distrito";
import { DistritoService } from "./distrito.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
import { Localidad } from "../localidad/localidad";
import { LocalidadService } from "../localidad/localidad.service";
//
import { Observable } from "rxjs";
import { FormControl } from "@angular/forms";
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';

@Component({
  selector: "modal-distrito",
  templateUrl: "./modal-distrito.html",
  providers: [DistritoService, LocalidadService]
})
export class ModalDistrito implements OnInit {
  public distrito: Distrito = new Distrito();
  public titulo: string = "Crear Distrito";
  public isSaving: boolean = false;
  localidades: Localidad[];
  //
  filteredLocalidades: Observable<any[]>;
  myControl: FormControl = new FormControl();
  valStr:string;

  constructor(
    private distritoService: DistritoService,
    private localidadService: LocalidadService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.localidadService
      .getLocalidades()
      .subscribe(localidades => (this.localidades = localidades));
    /////// Autocomplete ///////////////////////////////
    this.autoLocalidad();
    /////// fin autocomplete /////////
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.distritoService
          .getDistrito(id)
          .subscribe(distrito => (this.distrito = distrito));
      }
    });
  }

  save(): void {
    this.isSaving = true;
    if (typeof this.distrito.id === "undefined" || this.distrito.id === null) {
      this.distritoService.create(this.distrito).subscribe(distrito => {
        this.close();
        //swal('Nuevo Distrito', `Distrito ${distrito.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.distritoService.update(this.distrito).subscribe(distrito => {
        this.close();
        //swal("Distrito Actualizado", `Distrito ${distrito.descripcion} actualizado con éxito!`,"success");
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["/clasificadores/distritos"]);
  }

  ///////// autocomplete /////////////////
  autoLocalidad(){
          
    this.filteredLocalidades = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filter(val))
      );
  }
  filter(val) {
    if(typeof val.descripcion === 'undefined'){
      this.valStr = val;
    }else{
      this.valStr = val.descripcion;
    }
    if (this.localidades) {
      return this.localidades.filter(localidades =>
        localidades.descripcion.toLowerCase().includes(this.valStr.toLowerCase())
        || localidades.provincia.descripcion.toLowerCase().includes(this.valStr.toLowerCase())
      );
    }
  }

  public getDisplayFn() {
    return (val) => this.display(val);
 }
  private display(loc): string {
    //access component "this" here
    return loc ? loc.descripcion + " - " +  loc.provincia.descripcion : loc;
 }

  private selected(loc){
    console.log(loc);
  }

  /////////Fin autocomplete //////////////////////////////////////

}
