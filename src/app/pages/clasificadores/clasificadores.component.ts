import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-clasificadores',
  templateUrl: './clasificadores.component.html',
  styleUrls: ['./clasificadores.component.css']
})
export class ClasificadoresComponent implements OnInit {
  bodyClasses = 'skin-green sidebar-mini';
  body: HTMLBodyElement = document.getElementsByTagName('body')[0];
  constructor(
    private _route:ActivatedRoute,
    private _router:Router
  ) { }

  ngOnInit() {
    this.body.classList.add('skin-green');
    this.body.classList.add('sidebar-mini');
  }

}
