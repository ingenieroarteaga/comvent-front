import { Pais } from "../pais/pais";
import { Localidad } from "../localidad/localidad";

export class Provincia {
    
    public id: number;
    public descripcion: string;
    public fechaBaja: Date;
    public pais: Pais;
    public localidadList: Localidad[];
  
}

