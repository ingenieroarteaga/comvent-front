import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
import { Provincia } from "./provincia";
import { ProvinciaService } from "./provincia.service";
import { Pais } from "../pais/pais";
import { PaisService } from "../pais/pais.service";

@Component({
  selector: "modal-provincia",
  templateUrl: "./modal-provincia.html",
  providers: [ProvinciaService, PaisService]
})
export class ModalProvincia implements OnInit {
  public provincia: Provincia = new Provincia();
  public titulo: string = "Crear Provincia";
  public isSaving: boolean = false;
  paises: Pais[];

  constructor(
    private provinciaService: ProvinciaService,
    private paisService: PaisService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.paisService.getPaises().subscribe(paises => (this.paises = paises));
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.provinciaService
          .getProvincia(id)
          .subscribe(provincia => (this.provincia = provincia));
      }
    });
  }

  comparePais(t1: Pais, t2: Pais): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  save(): void {
    this.isSaving = true;
    if (
      typeof this.provincia.id === "undefined" ||
      this.provincia.id === null
    ) {
      this.provinciaService.create(this.provincia).subscribe(provincia => {
        this.close();
        //swal('Nuevo Provincia', `Provincia ${provincia.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.provinciaService.update(this.provincia).subscribe(provincia => {
        this.close();
        //swal('Provincia Actualizado', `Provincia ${provincia.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["clasificadores/provincias"]);
  }
}
