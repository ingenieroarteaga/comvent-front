import { ProvinciaService } from './provincia.service';
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Provincia } from './provincia';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from "@angular/forms";

@Component({
  selector: 'app-provincia',
  templateUrl: './provincia.component.html',
  styleUrls: ['./provincia.component.css'],
  providers: [ProvinciaService]
})
export class ProvinciaComponent implements OnInit {
public titulo: string;
  provincias: Provincia[];
  myProvincia: FormControl = new FormControl();
  p : number;
  
  constructor(
    private provinciaService: ProvinciaService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.provinciaService.getProvincias().subscribe(
      provincias => this.provincias = provincias
    );
  }
}
