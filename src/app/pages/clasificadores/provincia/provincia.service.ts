import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Provincia } from './provincia';


@Injectable()
export class ProvinciaService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getProvincias(): Observable<Provincia[]> {
    return this.http.get(this.url + 'provincias').pipe(
      map(response => response as Provincia[])
    );
  }

  getProvincia(id: number): Observable<Provincia> {
    return this.http.get<Provincia>(`${this.url + 'provincia'}/${id}`)
  }

  create(provincia: Provincia): Observable<Provincia> {
    return this.http.post<Provincia>(this.url + 'provincia', provincia, { headers: this.httpHeaders })
  }

  update(provincia: Provincia): Observable<Provincia> {
    return this.http.put<Provincia>(this.url + 'provincia', provincia, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Provincia> {
    return this.http.delete<Provincia>(`${this.url + 'provincia'}/${id}`, { headers: this.httpHeaders })
  }

}