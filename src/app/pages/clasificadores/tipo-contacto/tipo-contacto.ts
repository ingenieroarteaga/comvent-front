import { Contacto } from "../../model/contacto";

export class TipoContacto {
    public id: number;
    public descripcion: string;
    public fechaBaja: Date;
    public contactoList: Contacto[];
 
}
