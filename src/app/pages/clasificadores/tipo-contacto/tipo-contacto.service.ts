import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { TipoContacto } from './tipo-contacto';
@Injectable()
export class TipoContactoService {
  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getTipoContactos(): Observable<TipoContacto[]> {
    return this.http.get(this.url + 'tipos-contactos').pipe(
      map(response => response as TipoContacto[])
    );
  }

  getTipoContacto(id: number): Observable<TipoContacto> {
    return this.http.get<TipoContacto>(`${this.url + 'tipo-contacto'}/${id}`)
  }

  create(tipoContacto: TipoContacto): Observable<TipoContacto> {
    return this.http.post<TipoContacto>(this.url + 'tipo-contacto', tipoContacto, { headers: this.httpHeaders })
  }

  update(tipoContacto: TipoContacto): Observable<TipoContacto> {
    return this.http.put<TipoContacto>(this.url + 'tipo-contacto', tipoContacto, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<TipoContacto> {
    return this.http.delete<TipoContacto>(`${this.url + 'tipo-contacto'}/${id}`, { headers: this.httpHeaders })
  }


}
