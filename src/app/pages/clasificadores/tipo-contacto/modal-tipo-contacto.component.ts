import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { TipoContacto } from "./tipo-contacto";
import { TipoContactoService } from "./tipo-contacto.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
@Component({
  selector: "modal-tipo-contacto",
  templateUrl: "./modal-tipo-contacto.html",
  providers: [TipoContactoService]
})
export class ModalTipoContacto implements OnInit {
  public tipoContacto: TipoContacto = new TipoContacto();
  public titulo: string = "Crear Tipo de Contacto";
  public isSaving: boolean = false;

  constructor(
    private tipoContactoService: TipoContactoService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.tipoContactoService
          .getTipoContacto(id)
          .subscribe(tipoContacto => (this.tipoContacto = tipoContacto));
      }
    });
  }

  save(): void {
    this.isSaving = true;
    if (
      typeof this.tipoContacto.id === "undefined" ||
      this.tipoContacto.id === null
    ) {
      this.tipoContactoService
        .create(this.tipoContacto)
        .subscribe(tipoContacto => {
            this.close();
          //swal('Nuevo TipoContacto', `TipoContacto ${tipoContacto.descripcion} creado con éxito!`, 'success');
          this.refresh();
        });
    } else {
      this.tipoContactoService
        .update(this.tipoContacto)
        .subscribe(tipoContacto => {
          this.close();
          //swal('TipoContacto Actualizado', `TipoContacto ${tipoContacto.descripcion} actualizado con éxito!`, 'success');
          this.refresh();
        });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["clasificadores/tipos-contactos"]);
  }
}
