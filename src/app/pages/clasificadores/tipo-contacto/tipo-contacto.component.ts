import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { TipoContactoService } from './tipo-contacto.service';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TipoContacto } from './tipo-contacto';
import { FormControl } from "@angular/forms";

@Component({
  selector: 'app-tipo-contacto',
  templateUrl: './tipo-contacto.component.html',
  styleUrls: ['./tipo-contacto.component.css'],
  providers: [TipoContactoService]
})
export class TipoContactoComponent implements OnInit {
  public titulo: String;
  public tipoContactos: TipoContacto[];
  myContacto: FormControl = new FormControl();
  p:number;

  constructor(
    private tipoContactoService: TipoContactoService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.tipoContactoService.getTipoContactos().subscribe(
      tipoContactos => this.tipoContactos = tipoContactos
    );
  }
}
