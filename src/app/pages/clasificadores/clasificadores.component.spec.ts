import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClasificadoresComponent } from './clasificadores.component';

describe('ClasificadoresComponent', () => {
  let component: ClasificadoresComponent;
  let fixture: ComponentFixture<ClasificadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClasificadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClasificadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
