import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { TipoDocumento } from './tipo-documento';
@Injectable()
export class TipoDocumentoService {
  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getTipoDocumentos(): Observable<TipoDocumento[]> {
    return this.http.get(this.url + 'tipos-documentos').pipe(
      map(response => response as TipoDocumento[])
    );
  }

  getTipoDocumento(id: number): Observable<TipoDocumento> {
    return this.http.get<TipoDocumento>(`${this.url + 'tipo-documento'}/${id}`)
  }

  create(tipoDocumento: TipoDocumento): Observable<TipoDocumento> {
    return this.http.post<TipoDocumento>(this.url + 'tipo-documento', tipoDocumento, { headers: this.httpHeaders })
  }

  update(tipoDocumento: TipoDocumento): Observable<TipoDocumento> {
    return this.http.put<TipoDocumento>(this.url + 'tipo-documento', tipoDocumento, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<TipoDocumento> {
    return this.http.delete<TipoDocumento>(`${this.url + 'tipo-documento'}/${id}`, { headers: this.httpHeaders })
  }


}
