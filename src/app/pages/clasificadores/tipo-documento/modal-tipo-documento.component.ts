import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { TipoDocumento } from "./tipo-documento";
import { TipoDocumentoService } from "./tipo-documento.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
@Component({
  selector: "modal-tipo-documento",
  templateUrl: "./modal-tipo-documento.html",
  providers: [TipoDocumentoService]
})
export class ModalTipoDocumento implements OnInit {
  public tipoDocumento: TipoDocumento = new TipoDocumento();
  public titulo: string = "Crear Tipo de Documento";
  public isSaving: boolean = false;

  constructor(
    private tipoDocumentoService: TipoDocumentoService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.tipoDocumentoService
          .getTipoDocumento(id)
          .subscribe(tipoDocumento => (this.tipoDocumento = tipoDocumento));
      }
    });
  }

  save(): void {
    this.isSaving = true;
    if (
      typeof this.tipoDocumento.id === "undefined" ||
      this.tipoDocumento.id === null
    ) {
      this.tipoDocumentoService
        .create(this.tipoDocumento)
        .subscribe(tipoDocumento => {
          this.close();
          //swal('Nuevo TipoDocumento', `TipoDocumento ${tipoDocumento.descripcion} creado con éxito!`, 'success');
          this.refresh();
        });
    } else {
      this.tipoDocumentoService
        .update(this.tipoDocumento)
        .subscribe(tipoDocumento => {
          this.close();
          //swal('TipoDocumento Actualizado', `TipoDocumento ${tipoDocumento.descripcion} actualizado con éxito!`, 'success');
          this.refresh();
        });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["clasificadores/tipos-documentos"]);
  }
}
