import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { TipoDocumentoService } from './tipo-documento.service';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TipoDocumento } from './tipo-documento';
import { FormControl } from "@angular/forms";
@Component({
  selector: 'app-tipo-documento',
  templateUrl: './tipo-documento.component.html',
  styleUrls: ['./tipo-documento.component.css'],
  providers: [TipoDocumentoService]
})
export class TipoDocumentoComponent implements OnInit {
  public titulo: String;
  public tipoDocumentos: TipoDocumento[];
  myTipoDocumento: FormControl = new FormControl();
  p:number;
  
  constructor(
    private tipoDocumentoService: TipoDocumentoService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.tipoDocumentoService.getTipoDocumentos().subscribe(
      tipoDocumentos => this.tipoDocumentos = tipoDocumentos
    );
  }
}
