import { Persona } from "../../model/persona/persona";

export class TipoDocumento {
    public id: number;
    public descripcion: string;
    public idFe: number;
    public fechaBaja: Date;
    public personaList: Persona[];
}
