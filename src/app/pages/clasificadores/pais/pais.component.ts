import { GLOBAL } from '../../global';
import { PaisService } from './pais.service';
import { Pais } from './pais';
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from "@angular/forms";
@Component({
  selector: 'app-pais',
  templateUrl: './pais.component.html',
  styleUrls: ['./pais.component.css'],
  providers: [PaisService]
})
export class PaisComponent implements OnInit {
  public titulo: string;
  paises: Pais[];
  myPais: FormControl = new FormControl();
  p : number;

  constructor(
    private paisService: PaisService,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.paisService.getPaises().subscribe(
      paises => this.paises = paises
    );
  }

  
}
