import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Pais } from './pais';
@Injectable()
export class PaisService {
  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getPaises(): Observable<Pais[]> {
    return this.http.get(this.url + 'paises').pipe(
      map(response => response as Pais[])
    );
  }

  getPais(id: number): Observable<Pais> {
    return this.http.get<Pais>(`${this.url + 'pais'}/${id}`)
  }

  create(pais: Pais): Observable<Pais> {
    return this.http.post<Pais>(this.url + 'pais', pais, { headers: this.httpHeaders })
  }

  update(pais: Pais): Observable<Pais> {
    return this.http.put<Pais>(this.url + 'pais', pais, { headers: this.httpHeaders })
    //return this.http.put<Pais>(`${this.url + 'pais'}/${pais.id}`, pais, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Pais> {
    return this.http.delete<Pais>(`${this.url + 'pais'}/${id}`, { headers: this.httpHeaders })
  }


}
