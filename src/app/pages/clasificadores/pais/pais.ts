import { Provincia } from "../provincia/provincia";

export class Pais {
        
        public id: number;
        public descripcion: string;
        public fechaBaja: number;
        public provinciaList: Provincia[];
}
