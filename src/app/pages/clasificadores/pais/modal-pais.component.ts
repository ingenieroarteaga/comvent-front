import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Pais } from "./pais";
import { PaisService } from "./pais.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
@Component({
  selector: "modal-pais",
  templateUrl: "./modal-pais.html",
  providers: [PaisService]
})
export class ModalPais implements OnInit {
  public pais: Pais = new Pais();
  public titulo: string = "Crear País";
  public isSaving: boolean = false;

  constructor(
    private paisService: PaisService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.paisService.getPais(id).subscribe(pais => (this.pais = pais));
      }
    });
  }

  save(): void {
    this.isSaving = true;
    if (typeof this.pais.id === 'undefined' || this.pais.id === null) {
      this.paisService.create(this.pais).subscribe(pais => {
        this.close();
        //swal('Nuevo Pais', `Pais ${pais.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.paisService.update(this.pais).subscribe(pais => {
        this.close();
        //swal('Pais Actualizado', `Pais ${pais.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(['clasificadores/paises']);
  }
}
