import { Domicilio } from "../../model/domicilio";

export class TipoDomicilio {
    public id: number;
    public descripcion: string;
    public fechaBaja: Date;
    public domicilioList: Domicilio[];
 
}
