import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { TipoDomicilioService } from './tipo-domicilio.service';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TipoDomicilio } from './tipo-domicilio';
import { FormControl } from "@angular/forms";

@Component({
  selector: 'app-tipo-domicilio',
  templateUrl: './tipo-domicilio.component.html',
  styleUrls: ['./tipo-domicilio.component.css'],
  providers: [TipoDomicilioService]
})
export class TipoDomicilioComponent implements OnInit {
  public titulo: String;
  public tipoDomicilios: TipoDomicilio[];
  myDomicilio: FormControl = new FormControl();

  constructor(
    private tipoDomicilioService: TipoDomicilioService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.tipoDomicilioService.getTipoDomicilios().subscribe(
      tipoDomicilios => this.tipoDomicilios = tipoDomicilios
    );
  }
}
