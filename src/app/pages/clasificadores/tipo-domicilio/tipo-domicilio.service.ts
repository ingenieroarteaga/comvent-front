import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { TipoDomicilio } from './tipo-domicilio';
@Injectable()
export class TipoDomicilioService {
  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getTipoDomicilios(): Observable<TipoDomicilio[]> {
    return this.http.get(this.url + 'tipos-domicilios').pipe(
      map(response => response as TipoDomicilio[])
    );
  }

  getTipoDomicilio(id: number): Observable<TipoDomicilio> {
    return this.http.get<TipoDomicilio>(`${this.url + 'tipo-domicilio'}/${id}`)
  }

  create(tipoDomicilio: TipoDomicilio): Observable<TipoDomicilio> {
    return this.http.post<TipoDomicilio>(this.url + 'tipo-domicilio', tipoDomicilio, { headers: this.httpHeaders })
  }

  update(tipoDomicilio: TipoDomicilio): Observable<TipoDomicilio> {
    return this.http.put<TipoDomicilio>(this.url + 'tipo-domicilio', tipoDomicilio, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<TipoDomicilio> {
    return this.http.delete<TipoDomicilio>(`${this.url + 'tipo-domicilio'}/${id}`, { headers: this.httpHeaders })
  }


}
