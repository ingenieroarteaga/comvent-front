import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { TipoDomicilio } from "./tipo-domicilio";
import { TipoDomicilioService } from "./tipo-domicilio.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
@Component({
  selector: "modal-tipo-domicilio",
  templateUrl: "./modal-tipo-domicilio.html",
  providers: [TipoDomicilioService]
})
export class ModalTipoDomicilio implements OnInit {
  public tipoDomicilio: TipoDomicilio = new TipoDomicilio();
  public titulo: string = "Crear Tipo de Domicilio";
  public isSaving: boolean = false;

  constructor(
    private tipoDomicilioService: TipoDomicilioService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.tipoDomicilioService
          .getTipoDomicilio(id)
          .subscribe(tipoDomicilio => (this.tipoDomicilio = tipoDomicilio));
      }
    });
  }

  save(): void {
    this.isSaving = true;
    if (
      typeof this.tipoDomicilio.id === "undefined" ||
      this.tipoDomicilio.id === null
    ) {
      this.tipoDomicilioService
        .create(this.tipoDomicilio)
        .subscribe(tipoDomicilio => {
          this.close();
          //swal('Nuevo TipoDomicilio', `TipoDomicilio ${tipoDomicilio.descripcion} creado con éxito!`, 'success');
          this.refresh();
        });
    } else {
      this.tipoDomicilioService
        .update(this.tipoDomicilio)
        .subscribe(tipoDomicilio => {
            this.close();
          //swal('TipoDomicilio Actualizado', `TipoDomicilio ${tipoDomicilio.descripcion} actualizado con éxito!`, 'success');
          this.refresh();
        });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["clasificadores/tipos-domicilios"]);
  }
}
