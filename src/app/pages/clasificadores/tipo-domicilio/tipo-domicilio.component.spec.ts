import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoDomicilioComponent } from './tipo-domicilio.component';

describe('TipoDomicilioComponent', () => {
  let component: TipoDomicilioComponent;
  let fixture: ComponentFixture<TipoDomicilioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoDomicilioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoDomicilioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
