import { Distrito } from "../distrito/distrito";
import { Provincia } from "../provincia/provincia";
import { Zona } from "../zona/zona";

export class Localidad {
    public id: number;
    public descripcion: string;
    public fechaBaja: Date;
    public distritoList: Distrito[];
    public provincia: Provincia;
    public zona: Zona;
 
}
