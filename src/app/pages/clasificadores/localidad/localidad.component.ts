import { GLOBAL } from '../../global';
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Localidad } from './localidad';
import { LocalidadService } from './localidad.service';
import { FormControl } from "@angular/forms";


@Component({
  selector: 'app-localidades',
  templateUrl: './localidad.component.html',
  styleUrls: ['./localidad.component.css'],
  providers: [LocalidadService]
})
export class LocalidadComponent implements OnInit {
  
  public titulo: string;
  localidades: Localidad[];
  myLocalidad: FormControl = new FormControl();
  p : number;
  
  constructor(
    private localidadService: LocalidadService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.localidadService.getLocalidades().subscribe(
      localidades => this.localidades = localidades
    );

  }


}

