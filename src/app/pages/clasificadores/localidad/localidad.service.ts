import { Injectable } from '@angular/core';
import { GLOBAL } from '../../global';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Localidad } from './localidad';

@Injectable()
export class LocalidadService {
  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getLocalidades(): Observable<Localidad[]> {
    return this.http.get(this.url + 'localidades').pipe(
      map(response => response as Localidad[])
    );
  }

  getLocalidad(id: number): Observable<Localidad> {
    return this.http.get<Localidad>(`${this.url + 'localidad'}/${id}`)
  }

  create(localidad: Localidad): Observable<Localidad> {
    return this.http.post<Localidad>(this.url + 'localidad', localidad, { headers: this.httpHeaders })
  }

  update(localidad: Localidad): Observable<Localidad> {
    return this.http.put<Localidad>(this.url + 'localidad', localidad, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Localidad> {
    return this.http.delete<Localidad>(`${this.url + 'localidad'}/${id}`, { headers: this.httpHeaders })
  }

}
