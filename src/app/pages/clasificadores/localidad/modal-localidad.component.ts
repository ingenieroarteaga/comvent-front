import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
import { Pais } from "../pais/pais";
import { Localidad } from "./localidad";
import { PaisService } from "../pais/pais.service";
import { LocalidadService } from "./localidad.service";
import { ProvinciaService } from "../provincia/provincia.service";
import { Provincia } from "../provincia/provincia";
import { Zona } from "../zona/zona";
import { ZonaService } from "../zona/zona.service";

@Component({
  selector: "modal-localidad",
  templateUrl: "./modal-localidad.html",
  providers: [PaisService, ProvinciaService, LocalidadService, ZonaService]
})
export class ModalLocalidad implements OnInit {
  public localidad: Localidad = new Localidad();
  public titulo: string = "Crear Localidad";
  public isSaving: boolean = false;
  paises: Pais[];
  provincias: Provincia[];
  zonas: Zona[];

  constructor(
    private localidadService: LocalidadService,
    private provinciaService: ProvinciaService,
    private paisService: PaisService,
    private zonaService: ZonaService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.paisService.getPaises().subscribe(paises => (this.paises = paises));
    this.provinciaService
      .getProvincias()
      .subscribe(provincias => (this.provincias = provincias));
    this.zonaService.getZonas().subscribe(zonas => (this.zonas = zonas));
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.localidadService
          .getLocalidad(id)
          .subscribe(localidad => (this.localidad = localidad));
      }
    });
  }

  comparePais(t1: Pais, t2: Pais): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  compareProvincia(t1: Provincia, t2: Provincia): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  compareZona(t1: Zona, t2: Zona): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  save(): void {
    this.isSaving = true;
    if (
      typeof this.localidad.id === "undefined" ||
      this.localidad.id === null
    ) {
      this.localidadService.create(this.localidad).subscribe(localidad => {
        this.close();
        //swal('Nuevo Localidad', `Localidad ${localidad.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.localidadService.update(this.localidad).subscribe(localidad => {
        this.close();
        //swal("Localidad Actualizado", `Localidad ${localidad.descripcion} actualizado con éxito!`,"success");
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["clasificadores/localidades"]);
  }
}
