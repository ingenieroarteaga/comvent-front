import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { ZonaService } from './zona.service';
import { Zona } from './zona';
import { FormControl } from "@angular/forms";

@Component({
  selector: 'app-zonas',
  templateUrl: './zona.component.html',
  styleUrls: ['./zona.component.css'],
  providers: [ZonaService]
})
export class ZonaComponent implements OnInit {
  zonas: Zona[];
  myZona: FormControl = new FormControl();
  p : number;
  
  constructor(private zonaService: ZonaService,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.zonaService.getZonas().subscribe(
      zonas => this.zonas = zonas
    );
  }
}
