import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Zona } from "./zona";
import { ZonaService } from "./zona.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
@Component({
  selector: "modal-zona",
  templateUrl: "./modal-zona.html",
  providers: [ZonaService]
})
export class ModalZona implements OnInit {
  public zona: Zona = new Zona();
  public titulo: string = "Crear Zona";
  public isSaving: boolean = false;

  constructor(
    private zonaService: ZonaService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.zonaService.getZona(id).subscribe(zona => (this.zona = zona));
      }
    });
  }

  save(): void {
    this.isSaving = true;
    if (typeof this.zona.id === "undefined" || this.zona.id === null) {
      this.zonaService.create(this.zona).subscribe(zona => {
        this.close();
        //swal('Nuevo Zona', `Zona ${zona.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.zonaService.update(this.zona).subscribe(zona => {
        this.close();
        //swal('Zona Actualizado', `Zona ${zona.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["clasificadores/zonas"]);
  }

}
