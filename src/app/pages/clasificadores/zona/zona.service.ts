import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Zona } from './zona';


@Injectable()
export class ZonaService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getZonas(): Observable<Zona[]> {
    return this.http.get(this.url + 'zonas').pipe(
      map(response => response as Zona[])
    );
  }

  getZona(id: number): Observable<Zona> {
    return this.http.get<Zona>(`${this.url + 'zona'}/${id}`)
  }

  create(zona: Zona): Observable<Zona> {
    return this.http.post<Zona>(this.url + 'zona', zona, { headers: this.httpHeaders })
  }

  update(zona: Zona): Observable<Zona> {
    return this.http.put<Zona>(this.url + 'zona', zona, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Zona> {
    return this.http.delete<Zona>(`${this.url + 'zona'}/${id}`, { headers: this.httpHeaders })
  }

}