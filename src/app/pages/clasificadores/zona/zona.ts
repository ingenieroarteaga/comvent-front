import { Localidad } from "../localidad/localidad";

export class Zona {
    public id: number;
    public descripcion: string;
    public localidadList: Localidad[];
 
}
