import { SharedModule } from '../shared/shared.module';
import { NgModule } from '@angular/core';
import { MatButtonModule, MatCheckboxModule, MatTableModule, MatToolbarModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { NgbModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatAutocompleteModule, MatFormFieldModule } from '@angular/material';
import { InicioComponent } from './inicio/inicio.component';
import { PagesComponent } from './pages.component';
import { PAGES_ROUTES } from './pages.routes';
import { PerfilComponent } from './perfil/perfil.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
////// PIPES
import { FilterPipe } from '../pipes/filter.pipe';
import { FilterPedidoDetallePipe } from '../pipes/filterPedidoDetalle.pipe';
import { FilterProvincia } from '../pipes/clasificadores/provincia.pipe';
import { FilterLocalidad } from '../pipes/clasificadores/localidad.pipe';
import { FilterDistrito } from '../pipes/clasificadores/distrito.pipe';
import { FilterBandeja } from '../pipes/herramientas/bandeja.pipe';
import { FilterVariedad } from '../pipes/herramientas/variedad.pipe';
import { FilterEntidad } from '../pipes/herramientas/entidad.pipe';
import { FilterSucursal } from '../pipes/herramientas/sucursal.pipe';
import { FilterCalendario } from '../pipes/herramientas/calendario.pipe';
import { FilterInvernadero } from '../pipes/herramientas/invernadero.pipe';
import { FilterEmpresa } from '../pipes/personas/empresa.pipe';
import { FilterClienteProdutor } from '../pipes/personas/cliente-productor.pipe';
import { FilterProveedor } from '../pipes/personas/proveedor.pipe';
import { FilterProveedorBandeja } from '../pipes/personas/proveedorBandeja.pipe';
import { FilterFletero } from '../pipes/personas/fletero.pipe';
import { FilterFinca } from '../pipes/personas/finca.pipe';

// Administracion
import { AdministracionComponent } from './administracion/administracion.component';
import { CuentaComponent } from './administracion/cuenta/cuenta.component';
import { CuentaFleteroComponent } from './administracion/cuenta-fletero/cuenta-fletero.component';
import { CuentaProveedorComponent } from './administracion/cuenta-proveedor/cuenta-proveedor.component';
import { ListaPrecioComponent } from './administracion/lista-precio/lista-precio.component';
import { PagoComponent } from './administracion/pago/pago.component';
import { PagoProveedorComponent } from './administracion/pago-proveedor/pago-proveedor.component';
import { ComprobanteComponent } from './administracion/comprobante/comprobante.component';
import { CuentaFleteroBandComponent } from './administracion/cuenta-fletero-band/cuenta-fletero-band.component';
import { CajaComponent } from './administracion/caja/caja.component';


// Producción
import { ProduccionComponent } from './produccion/produccion.component';
import { CompraComponent } from './produccion/compra/compra.component';
import { ModalCompra } from './produccion/compra/modal-compra.component';
import { DetalleCompraComponent } from './produccion/compra/detalle-compra.component';
import { RemitoComponent } from './produccion/remito/remito.component';
import { PedidoComponent } from './produccion/pedido/pedido.component';
import { ModalPedido } from './produccion/pedido/modal-pedido.component';
import { DetallePedidoComponent } from './produccion/pedido/detalle-pedido.component';
import { SiembraComponent } from './produccion/siembra/siembra.component';
import { ModalSiembra } from './produccion/siembra/modal-siembra.component';
import { DetalleSiembraComponent } from './produccion/siembra/detalle-siembra.component';
import { StockSemillaComponent } from './produccion/stock-semilla/stock-semilla.component';
import { ModalStockSemilla } from './produccion/stock-semilla/modal-stock-semilla.component';
import { ModalStockInicial } from './produccion/stock-semilla/modal-stock-inicial.component';
import { AnalisisBatchComponent } from './produccion/analisis-batch/analisis-batch.component';
import { ModalAnalisisBatch } from './produccion/analisis-batch/modal-analisis-batch.component';
import { IdentificadorComponent } from './produccion/identificador/identificador.component';
import { ModalIdentificador } from './produccion/identificador/modal-identificador.component';
import { DetalleIdentificadorComponent } from './produccion/identificador/detalle-identificador.component';


// Clasificadores
import { ClasificadoresComponent } from './clasificadores/clasificadores.component';
import { ProvinciaComponent } from './clasificadores/provincia/provincia.component';
import { ModalProvincia } from './clasificadores/provincia/modal-provincia.component';
import { PaisComponent } from './clasificadores/pais/pais.component';
import { ModalPais } from './clasificadores/pais/modal-pais.component';
import { LocalidadComponent } from './clasificadores/localidad/localidad.component';
import { ModalLocalidad } from './clasificadores/localidad/modal-localidad.component';
import { DistritoComponent } from './clasificadores/distrito/distrito.component';
import { ModalDistrito } from './clasificadores/distrito/modal-distrito.component';
import { ZonaComponent } from './clasificadores/zona/zona.component';
import { ModalZona } from './clasificadores/zona/modal-zona.component';
import { TipoContactoComponent } from './clasificadores/tipo-contacto/tipo-contacto.component';
import { ModalTipoContacto } from './clasificadores/tipo-contacto/modal-tipo-contacto.component';
import { TipoDomicilioComponent } from './clasificadores/tipo-domicilio/tipo-domicilio.component';
import { ModalTipoDomicilio } from './clasificadores/tipo-domicilio/modal-tipo-domicilio.component';
import { TipoDocumentoComponent } from './clasificadores/tipo-documento/tipo-documento.component';
import { ModalTipoDocumento } from './clasificadores/tipo-documento/modal-tipo-documento.component';


// Personas
import { PersonasComponent } from './personas/personas.component';
import { EmpresaComponent } from './personas/empresa/empresa.component';
import { ModalEmpresa } from './personas/empresa/modal-empresa.component';
import { ClienteComponent } from './personas/cliente/cliente.component';
import { ModalCliente } from './personas/cliente/modal-cliente.component';
import { ProveedorComponent } from './personas/proveedor/proveedor.component';
import { ModalProveedor } from './personas/proveedor/modal-proveedor.component';
import { ProveedorBandejaComponent } from './personas/proveedor-bandeja/proveedor-bandeja.component';
import { ModalProveedorBandeja } from './personas/proveedor-bandeja/modal-proveedor-bandeja.component';
import { FleteroComponent } from './personas/fletero/fletero.component';
import { ModalFletero } from './personas/fletero/modal-fletero.component';
import { FincaComponent } from './personas/finca/finca.component';
import { ModalFinca } from './personas/finca/modal-finca.component';


// Herramientas
import { HerramientaComponent } from './herramienta/herramienta.component';
import { BandejaComponent } from './herramienta/bandeja/bandeja.component';
import { ModalBandeja } from './herramienta/bandeja/modal-bandeja.component';
import { TipoBandejaComponent } from './herramienta/tipo-bandeja/tipo-bandeja.component';
import { ModalTipoBandeja } from './herramienta/tipo-bandeja/modal-tipo-bandeja.component';
import { EntidadComponent } from './herramienta/entidad/entidad.component';
import { ModalEntidad } from './herramienta/entidad/modal-entidad.component';
import { VariedadComponent } from './herramienta/variedad/variedad.component';
import { ModalVariedad } from './herramienta/variedad/modal-variedad.component';
import { InvernaderoComponent } from './herramienta/invernadero/invernadero.component';
import { ModalInvernadero } from './herramienta/invernadero/modal-invernadero.component';
import { SucursalComponent } from './herramienta/sucursal/sucursal.component';
import { ModalSucursal } from './herramienta/sucursal/modal-sucursal.component';
import { EspecieComponent } from './herramienta/especie/especie.component';
import { ModalEspecie } from './herramienta/especie/modal-especie.component';
import { CalendarioComponent } from './herramienta/calendario/calendario.component';
import { ModalCalendario } from './herramienta/calendario/modal-calendario.component';
import { ModalTipoAnalisisBatch } from './herramienta/tipo-analisis-batch/modal-tipo-analisis-batch.component';
import { TipoAnalisisBatchComponent } from './herramienta/tipo-analisis-batch/tipo-analisis-batch.component';
import { TipoMovStockSemComponent } from './herramienta/tipo-mov-stock-sem/tipo-mov-stock-sem.component';
import { ModalTipoMovStockSem } from './herramienta/tipo-mov-stock-sem/modal-tipo-mov-stock-sem.component';


import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { BrowserModule } from '@angular/platform-browser';


////




@NgModule({

    declarations: [
        InicioComponent,
        AdministracionComponent,
        PagesComponent,
        PersonasComponent,
        ClasificadoresComponent,
        HerramientaComponent,
        ProduccionComponent,
        AdministracionComponent,
        // Pipes
        FilterPipe,
        FilterPedidoDetallePipe,
        FilterProvincia,
        FilterLocalidad,
        FilterDistrito,
        FilterBandeja,
        FilterVariedad,
        FilterEntidad,
        FilterSucursal,
        FilterCalendario,
        FilterInvernadero,
        FilterEmpresa,
        FilterClienteProdutor,
        FilterProveedor,
        FilterProveedorBandeja,
        FilterFletero,
        FilterFinca,

        // Personas
        EmpresaComponent,
        ModalEmpresa,
        ClienteComponent,
        ModalCliente,
        ProveedorComponent,
        ModalProveedor,
        ProveedorBandejaComponent,
        ModalProveedorBandeja,
        FleteroComponent,
        ModalFletero,
        FincaComponent,
        ModalFinca,
        // Clasificadores
        PaisComponent,
        ModalPais,
        ProvinciaComponent,
        ModalProvincia,
        LocalidadComponent,
        ModalLocalidad,
        ZonaComponent,
        ModalZona,
        DistritoComponent,
        ModalDistrito,
        TipoDomicilioComponent,
        ModalTipoDomicilio,
        TipoContactoComponent,
        ModalTipoContacto,
        TipoDocumentoComponent,
        ModalTipoDocumento,
        // Herramientas
        BandejaComponent,
        TipoBandejaComponent,
        ModalBandeja,
        ModalTipoBandeja,
        VariedadComponent,
        ModalVariedad,
        EntidadComponent,
        ModalEntidad,
        SucursalComponent,
        ModalSucursal,
        InvernaderoComponent,
        ModalInvernadero,
        EspecieComponent,
        ModalEspecie,
        CalendarioComponent,
        ModalCalendario,
        TipoAnalisisBatchComponent,
        ModalTipoAnalisisBatch,
        TipoMovStockSemComponent,
        ModalTipoMovStockSem,

        // produccion
        PedidoComponent,
        ModalPedido,
        DetallePedidoComponent,
        RemitoComponent,
        SiembraComponent,
        ModalSiembra,
        DetalleSiembraComponent,
        CompraComponent,
        ModalCompra,
        DetalleCompraComponent,
        StockSemillaComponent,
        ModalStockSemilla,
        ModalStockInicial,
        AnalisisBatchComponent,
        ModalAnalisisBatch,
        IdentificadorComponent,
        ModalIdentificador,
        DetalleIdentificadorComponent,

        // administracion
        CuentaComponent,
        CuentaFleteroComponent,
        CuentaProveedorComponent,
        ListaPrecioComponent,
        PagoComponent,
        PagoProveedorComponent,
        ComprobanteComponent,
        // perfil
        PerfilComponent,
        CajaComponent,
        CuentaFleteroBandComponent,

    ],

    exports: [
        InicioComponent,
        AdministracionComponent,
        PagesComponent,
        PersonasComponent,
        ClasificadoresComponent,
        HerramientaComponent,
        ProduccionComponent,
        AdministracionComponent,

        // Personas
        EmpresaComponent,
        ModalEmpresa,
        ClienteComponent,
        ModalCliente,
        ProveedorComponent,
        ModalProveedor,
        ProveedorBandejaComponent,
        ModalProveedorBandeja,
        FleteroComponent,
        ModalFletero,
        FincaComponent,
        ModalFinca,

        // Clasificadores
        PaisComponent,
        ModalPais,
        ProvinciaComponent,
        ModalProvincia,
        LocalidadComponent,
        ModalLocalidad,
        ZonaComponent,
        ModalZona,
        DistritoComponent,
        ModalDistrito,
        TipoDomicilioComponent,
        ModalTipoDomicilio,
        TipoContactoComponent,
        ModalTipoContacto,
        TipoDocumentoComponent,
        ModalTipoDocumento,

        // Herramientas
        BandejaComponent,
        TipoBandejaComponent,
        ModalBandeja,
        ModalTipoBandeja,
        VariedadComponent,
        ModalVariedad,
        EntidadComponent,
        ModalEntidad,
        SucursalComponent,
        ModalSucursal,
        InvernaderoComponent,
        ModalInvernadero,
        EspecieComponent,
        ModalEspecie,
        CalendarioComponent,
        ModalCalendario,
        TipoAnalisisBatchComponent,
        ModalTipoAnalisisBatch,
        TipoMovStockSemComponent,
        ModalTipoMovStockSem,

        // produccion
        PedidoComponent,
        ModalPedido,
        DetallePedidoComponent,
        RemitoComponent,
        SiembraComponent,
        ModalSiembra,
        DetalleSiembraComponent,
        CompraComponent,
        ModalCompra,
        StockSemillaComponent,
        ModalStockSemilla,
        ModalStockInicial,
        AnalisisBatchComponent,
        ModalAnalisisBatch,
        IdentificadorComponent,
        ModalIdentificador,
        DetalleIdentificadorComponent,

        // administracion
        CuentaComponent,
        CuentaFleteroComponent,
        CuentaProveedorComponent,
        ListaPrecioComponent,
        PagoComponent,
        PagoProveedorComponent,
        ComprobanteComponent,

        // perfil
        PerfilComponent,
        CajaComponent,
        CuentaFleteroBandComponent,

        //// Material
        MatTableModule,
        CommonModule,
        MatToolbarModule,
        MatInputModule,
    ],

    imports: [
        SharedModule,
        PAGES_ROUTES,
        HttpModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        NgbModule.forRoot(),
        //// Material
        MatButtonModule, MatCheckboxModule,
        MatInputModule, MatAutocompleteModule,
        MatFormFieldModule,
        MatTableModule, CommonModule, MatToolbarModule,
        NgxPaginationModule,
        BrowserModule


    ]



})

export class  PagesModule {}
