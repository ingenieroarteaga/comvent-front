import { Calendario } from '../../herramienta/calendario/calendario';
import { Sucursal } from '../../herramienta/sucursal/sucursal';
import { SiembraDetalle } from '../../model/siembra-detalle';
import { Identificador } from '../identificador/identificador';

export class Siembra {
    public id: number;
    public numero: number;
    public fecha: Date;
    public observacion: string;
    public fechaBaja: Date;
    public calendario: Calendario;
    public sucursal: Sucursal;
    public identificadorList: Identificador[];
    public siembraDetalleList: SiembraDetalle[];

}
