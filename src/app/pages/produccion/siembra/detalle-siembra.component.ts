import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SiembraService } from './siembra.service';
import { Siembra } from './siembra';
import { SiembraDetalle } from '../../model/siembra-detalle';

@Component({
  selector: 'app-detalle-siembra',
  templateUrl: './detalle-siembra.component.html',
  providers: [SiembraService]
})
export class DetalleSiembraComponent implements OnInit {

  public titulo: string;
  public siembra: Siembra;
  public detalleList: SiembraDetalle[];

  constructor(private siembraService: SiembraService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ){

  }

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.siembraService
          .getSiembra(id)
          .subscribe(
            siembra => (
              this.siembra = siembra, this.detalleList = siembra.siembraDetalleList
            )
          );
      }
    });
  }

}
