import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
import { Siembra } from "./siembra";
import { SiembraService } from "./siembra.service";
import { Bandeja } from "../../herramienta/bandeja/bandeja";
import { SiembraDetalle } from "../../model/siembra-detalle";
import { BandejaStock } from "../../model/bandeja-stock/bandeja-stock";
import { PedidoDetalle } from "../../model/pedido-detalle/pedido-detalle";
import { PedidoDetalleService } from "../../model/pedido-detalle/pedido-detalle.service";
import { FormControl } from "@angular/forms";
import { PedidoDetalleSiembra } from "./pedido-detalle-siembra";
import { StockSemilla } from "../stock-semilla/stock-semilla";
import { StockSemillaService } from "../stock-semilla/stock-semilla.service";
import { Observable } from "rxjs";
import { BandejaStockService } from "../../model/bandeja-stock/bandeja-stock.service";
import { startWith, map } from "../../../../../node_modules/rxjs/operators";
import { Identificador } from "../identificador/identificador";

//

@Component({
  selector: "modal-siembra",
  templateUrl: "./modal-siembra.html",
  providers: [
    SiembraService,
    PedidoDetalleService,
    StockSemillaService,
    BandejaStockService
    
  ]
})
export class ModalSiembra implements OnInit {
  public siembra: Siembra = new Siembra();
  public titulo: string = "Crear Siembra";
  public isSaving: boolean = false;
  
  // Detalle Pedidos
  public pedidoDetalles: PedidoDetalle[];
  public pedidoDetallesSiembra: PedidoDetalleSiembra[] = [];
  myControlPed: FormControl = new FormControl();
  public cantSeleccionados : number = 0;
  public cantPlantas : number = 0;
  public checkTodos : boolean = false;
  //
  ////// detalle  ///////////
  public detalleList: SiembraDetalle[] = [];
  public detalle: SiembraDetalle = new SiembraDetalle();
  //
  public identificador: Identificador = new Identificador();
  //
  //// autocomplete BandejaStock
   //
   public bandejaStocks: BandejaStock[];
   public bandejaStock: BandejaStock = new BandejaStock();
   //
   /// autocomplete StockSemilla
   //
   public stockSemillas: StockSemilla[];
   public stockSemilla: StockSemilla = new StockSemilla();
   //
   filteredStockSemillas: Observable<any[]>;
   myControlStockSem: FormControl = new FormControl();
   valStrStockSem: string;
  
  constructor(
    private siembraService: SiembraService,
    private pedidoDetalleService: PedidoDetalleService,
    private stockSemillaService: StockSemillaService,
    private bandejaStockService: BandejaStockService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.pedidoDetalleService.getPedidoDetallesPendientes()
      .subscribe(pedidoDetalles => (this.pedidoDetalles = pedidoDetalles, this.convertPDSFromPD()));
    ///
    this.stockSemillaService
      .getStockSemillas()
      .subscribe(stockSemillas => (this.stockSemillas = stockSemillas));
    this.autoStockSemilla();

    this.bandejaStockService
      .getBandejaStocks()
      .subscribe(bandejaStocks => (this.bandejaStocks = bandejaStocks));
    ///
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.siembraService
          .getSiembra(id)
          .subscribe(
            siembra => (
              (this.siembra = siembra),
              (this.detalleList = siembra.siembraDetalleList)
            )
          );
      }
    });
  }

  save(): void {
    if (
      typeof this.detalleList !== "undefined" &&
      this.detalleList !== null &&
      this.detalleList.length > 0
    ) {
      this.isSaving = true;
      // Detalle
      this.siembra.siembraDetalleList = this.detalleList;
      if (typeof this.siembra.id === "undefined" || this.siembra.id === null) {
        this.siembraService.create(this.siembra).subscribe(siembra => {
          this.close();
          //swal('Nuevo Siembra', `Siembra ${siembra.descripcion} creado con éxito!`, 'success');
          this.refresh();
        });
      } else {
        this.siembraService.update(this.siembra).subscribe(siembra => {
          this.close();
          //swal('Siembra Actualizado', `Siembra ${siembra.descripcion} actualizado con éxito!`, 'success');
          this.refresh();
        });
      }
    } else {
      swal(
        "Siembra",
        `Debe cargar todos los datos requeridos para realizar al siembra`,
        "error"
      );
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["produccion/siembras"]);
  }

  ////////// Select ///////////////////////////////////////
  compareBandeja(t1: Bandeja, t2: Bandeja): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  ////////////////////////////////////////////////////////////////////////////////
  ///////////////////// DETALLE //////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  
  /// convertir pedidoDetalleSiembra desde pedidoDetalle
  convertPDSFromPD(){
    var pds: PedidoDetalleSiembra = new PedidoDetalleSiembra();
    this.pedidoDetalles.forEach(pedidoDetalleFor => {
      pds.bandejaStock = pedidoDetalleFor.bandejaStock;
      pds.cantidadBandeja = pedidoDetalleFor.cantidadBandeja;
      pds.cantidadPlantas = pedidoDetalleFor.cantidadPlantas;
      pds.dias = pedidoDetalleFor.dias;
      pds.id = pedidoDetalleFor.id;
      pds.pedido = pedidoDetalleFor.pedido;
      pds.porcGerminacion = pedidoDetalleFor.porcGerminacion;
      pds.precioProduccion = pedidoDetalleFor.precioProduccion;
      pds.precioUnitario = pedidoDetalleFor.precioUnitario;
      pds.semanaEntrega = pedidoDetalleFor.semanaEntrega;
      pds.semanaSiembra = pedidoDetalleFor.semanaSiembra;
      pds.siembraDetalle = pedidoDetalleFor.siembraDetalle;
      pds.subTotal = pedidoDetalleFor.subTotal;
      pds.variedad = pedidoDetalleFor.variedad;
      //
      pds.check = false;
      this.pedidoDetallesSiembra.push(pds);
      pds = new PedidoDetalleSiembra();

    });
  }

  seleccionarTodos(){
    this.cantSeleccionados = 0;
    this.pedidoDetallesSiembra.forEach(detalle => {
      if(this.checkTodos){
        detalle.check = true;
        this.cantSeleccionados = this.cantSeleccionados + 1;
      }else{
        detalle.check = false;
      }
    });
  }

  checkDetalle(check){
    if(check){
      this.cantSeleccionados = this.cantSeleccionados + 1;
    }else{
      this.cantSeleccionados = this.cantSeleccionados - 1;
    }
    
  }

  seleccionarDetalle(){
    this.pedidoDetallesSiembra.forEach(element => {
      if(element.check){
        this.detalleList.push(this.convertDetalle(element));
        this.cantPlantas = this.cantPlantas + 1;
      }
    });
    /// Esto lo hago para eliminar mas de un registro seleccionado, sino no anda, se sale antes
    var v_vuelta = 0;
    while(v_vuelta < this.cantSeleccionados){
      for (let i = 0; i < this.pedidoDetallesSiembra.length; i++) {
        if(this.pedidoDetallesSiembra[i].check){
          this.pedidoDetallesSiembra.splice(i, 1);
        }
      }
      v_vuelta += 1;
    }
    // seteo los valores de nuevo
    this.cantSeleccionados = 0;
    this.checkTodos = false;
  }

  removeDetalle(index, p_det){
    this.detalleList.splice(index, 1);
    this.cantPlantas = this.cantPlantas - 1;
    p_det.pedidoDetalle.check = false;
    this.pedidoDetallesSiembra.push(p_det.pedidoDetalle);
    
  }

  ///////////////////////////////////////////////////////////////////////////////////////
  ////////// Add Detalle ///////////////////////////////////////////////////////////////
  convertDetalle(p_pedidoDetalle) {
    var retorno:SiembraDetalle = new SiembraDetalle();
    retorno.cantidadSemillas = p_pedidoDetalle.cantidadSemillas;
    retorno.bandejasOrdenadas = p_pedidoDetalle.cantidadBandeja;
    retorno.pedidoDetalle = p_pedidoDetalle;
    retorno.identificador = new Identificador();
    this.calcCantSem(retorno);
    return retorno;
  }

  ////////////// Calcular Cantidad de Semillas y ademas verifico que no exceda la cantidad de bandejas del pedido
  calcCantSem(p_det){
    if(p_det.bandejasOrdenadas > p_det.pedidoDetalle.cantidadBandeja){
      swal('Siembra', `La cantidad de Bandejas Ordenadas no puede exceder la de Bandejas Pedidas`, 'error');
      p_det.bandejasOrdenadas = p_det.pedidoDetalle.cantidadBandeja;

    }
    p_det.cantidadSemillas = p_det.bandejasOrdenadas * p_det.pedidoDetalle.bandejaStock.bandeja.capacidad;
    
  }



  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  ///////// autocomplete StockSemilla /////////////////
  autoStockSemilla() {
    this.filteredStockSemillas = this.myControlStockSem.valueChanges.pipe(
      startWith(""),
      map(val => this.filterStockSem(val))
    );
  }

  filterStockSem(val) {
    if(typeof val === 'undefined'){
      return this.stockSemillas;
    }
    console.log(val.toString());
    this.valStrStockSem = val.toString();
    if (this.stockSemillas) {
      return this.stockSemillas.filter(
        stockSemilla =>
          stockSemilla.batch.numero.toLowerCase().includes(this.valStrStockSem.toLowerCase()) ||
          stockSemilla.batch.lote.numero.toLowerCase().includes(this.valStrStockSem.toLowerCase())
      );
    }
  }

  public getDisplayFnStockSem() {
    return val => this.displayStockSem(val);
  }

  private displayStockSem(stockSemilla): string {
    //access component "this" here
    if (stockSemilla === null || typeof stockSemilla === "undefined" || typeof stockSemilla.batch === "undefined") {
      return null;
    } else {
      return stockSemilla
        ? stockSemilla.batch.numero 
        : stockSemilla;
    }
  }

  private selectedstockSem(stockSemilla) {
    console.log(stockSemilla);
  }

  ////////////////////
  public asignaVariedad(p_det) {
    console.log("Key Press");
    console.log(p_det);
  }

  /////////Fin autocomplete  StockSemilla //////////////////////////////////////
  ///////// Stock Bandeja ///////////////////////////
  compareBandejaStock(t1: BandejaStock, t2: BandejaStock): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }
  
  ///////////////////////////////////////////////////////////////////////////////
  //////////////// Funcion de semana a date /////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////
  
  getDateOfWeek(w, y) {
    var d = 1 + (w - 1) * 7; // 1st of January + 7 days for each week
    return new Date(y, 0, d);
  }

  getDateOfISOWeek(w, y) {
    var simple = new Date(y, 0, 1 + (w - 1) * 7);
    var dow = simple.getDay();
    var ISOweekStart = simple;
    if (dow <= 4) ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
    else ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
    return ISOweekStart;
  }

  private getWeekNumber(d: Date): number {
    // Copy date so don't modify original
    d = new Date(+d);
    d.setHours(0, 0, 0);
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setDate(d.getDate() + 4 - (d.getDay() || 7));
    // Get first day of year
    var yearStart = new Date(d.getFullYear(), 0, 1);
    // Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil((((d.valueOf() - yearStart.valueOf()) / 86400000) + 1) / 7);
    // Return array of year and week number
    return weekNo;
  }
  ///////////////// Fin de calculo de los week ///////////////////////////////////////////////


}
