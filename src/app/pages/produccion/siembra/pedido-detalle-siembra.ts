import { Siembra } from "./siembra";
import { Pedido } from "../pedido/pedido";
import { Variedad } from "../../herramienta/variedad/variedad";
import { BandejaStock } from "../../model/bandeja-stock/bandeja-stock";
import { SiembraDetalle } from "../../model/siembra-detalle";

export class PedidoDetalleSiembra {
    public id: number;
    public semanaEntrega: string;
    public semanaSiembra: string;
    public dias: number;
    public porcGerminacion: number;
    public cantidadPlantas: number;
    public cantidadBandeja: number;
    public precioUnitario: number;
    public precioProduccion: number;
    public subTotal: number;
    public siembraDetalle: SiembraDetalle;
    public pedido: Pedido;
    public bandejaStock: BandejaStock;
    public variedad: Variedad;
    //

    public check: boolean;

}
