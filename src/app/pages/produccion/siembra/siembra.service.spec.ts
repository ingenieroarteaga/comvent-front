import { TestBed, inject } from '@angular/core/testing';

import { SiembraService } from './siembra.service';

describe('SiembraService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SiembraService]
    });
  });

  it('should be created', inject([SiembraService], (service: SiembraService) => {
    expect(service).toBeTruthy();
  }));
});
