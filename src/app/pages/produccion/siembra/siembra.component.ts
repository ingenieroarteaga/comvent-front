import { Component, OnInit } from '@angular/core';
import { Siembra } from './siembra';
import { SiembraService } from './siembra.service';
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-siembra',
  templateUrl: './siembra.component.html',
  styleUrls: ['./siembra.component.css'],
  providers: [SiembraService]
})
export class SiembraComponent implements OnInit {

  public titulo: string = 'Siembras';
  siembras: Siembra[];
  p:number;

  constructor(private siembraService: SiembraService,
  private modalService: NgbModal,
  private _route: ActivatedRoute,
  private _router: Router)   {

  }

  ngOnInit() {
    this.siembraService.getSiembras().subscribe(
      siembras => this.siembras = siembras
    );
  }

}
