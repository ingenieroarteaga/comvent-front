import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Siembra } from './siembra';

@Injectable()
export class SiembraService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getSiembras(): Observable<Siembra[]> {
    return this.http.get(this.url + 'siembras').pipe(
      map(response => response as Siembra[])
    );
  }

  getSiembra(id: number): Observable<Siembra> {
    return this.http.get<Siembra>(`${this.url + 'siembra'}/${id}`)
  }

  create(siembra: Siembra): Observable<Siembra> {
    return this.http.post<Siembra>(this.url + 'siembra', siembra, { headers: this.httpHeaders })
  }

  update(siembra: Siembra): Observable<Siembra> {
    return this.http.put<Siembra>(this.url + 'siembra', siembra, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Siembra> {
    return this.http.delete<Siembra>(`${this.url + 'siembra'}/${id}`, { headers: this.httpHeaders })
  }

}
