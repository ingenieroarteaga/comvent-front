import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Remito } from './remito';

@Injectable()
export class RemitoService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getRemitos(): Observable<Remito[]> {
    return this.http.get(this.url + 'remitos').pipe(
      map(response => response as Remito[])
    );
  }

  getRemito(id: number): Observable<Remito> {
    return this.http.get<Remito>(`${this.url + 'remito'}/${id}`)
  }

  create(remito: Remito): Observable<Remito> {
    return this.http.post<Remito>(this.url + 'remito', remito, { headers: this.httpHeaders })
  }

  update(remito: Remito): Observable<Remito> {
    return this.http.put<Remito>(this.url + 'remito', remito, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Remito> {
    return this.http.delete<Remito>(`${this.url + 'remito'}/${id}`, { headers: this.httpHeaders })
  }

}
