import { Component, OnInit } from '@angular/core';
import { Remito } from './remito';
import { RemitoService } from './remito.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-remito',
  templateUrl: './remito.component.html',
  styleUrls: ['./remito.component.css'],
  providers: [RemitoService]
})
export class RemitoComponent implements OnInit {

  public titulo: string = "Remitos";
  remitos: Remito[];
  p:number;
  
  constructor(
    private remitoService: RemitoService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.remitoService.getRemitos().subscribe(
      remitos => this.remitos = remitos
    );
  }
}
