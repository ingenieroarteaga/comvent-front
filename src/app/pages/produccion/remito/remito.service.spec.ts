import { TestBed, inject } from '@angular/core/testing';

import { RemitoService } from './remito.service';

describe('RemitoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RemitoService]
    });
  });

  it('should be created', inject([RemitoService], (service: RemitoService) => {
    expect(service).toBeTruthy();
  }));
});
