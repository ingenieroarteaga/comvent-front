import { ClienteEmpresa } from "../../model/cliente-empresa/cliente-empresa";
import { Comprobante } from "../../administracion/comprobante/comprobante";
import { Finca } from "../../personas/finca/finca";
import { Fletero } from "../../personas/fletero/fletero";
import { CuentaDetalle } from "../../model/cuenta-detalle";
import { RemitoDetalle } from "../../model/remito-detalle";

export class Remito {
    public id: number;
    public fecha: Date;
    public numero: number;
    public monto: number;
    public clienteEmpresa: ClienteEmpresa;
    public comprobante: Comprobante;
    public finca: Finca;
    public fletero: Fletero;
    public cuentaDetalleList: CuentaDetalle[];
    public remitoDetalleList: RemitoDetalle[];
    
}
