import { Calendario } from '../../herramienta/calendario/calendario';
import { Siembra } from '../siembra/siembra';
import { StockSemilla } from '../stock-semilla/stock-semilla';
import { BandejaStock } from '../../model/bandeja-stock/bandeja-stock';
import { SiembraDetalle } from '../../model/siembra-detalle';

export class Identificador {
    public id: number;
    public numero: number;
    public calendario: Calendario;
    public siembra: Siembra;
    public stockSemilla: StockSemilla;
    public bandejaStock: BandejaStock;
    public siembraDetalleList: SiembraDetalle[];

}
