import { Component, OnInit } from '@angular/core';
import { IdentificadorService } from './identificador.service';
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Identificador } from './identificador';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IdentificadorDTO } from './identificador-dto';

@Component({
  selector: 'app-identificador',
  templateUrl: './identificador.component.html',
  styleUrls: ['./identificador.component.css'],
  providers: [IdentificadorService]
})

export class IdentificadorComponent implements OnInit {

  public titulo: string;
  identificadores: IdentificadorDTO[];
  p: number;

  constructor(private identificadorService: IdentificadorService,
  private modalService: NgbModal,
  private _route: ActivatedRoute,
  private _router: Router)   {


  }

  ngOnInit() {
    this.identificadorService.getIdentificadores().subscribe(
      identificadores => this.identificadores = identificadores
    );
  }

}
