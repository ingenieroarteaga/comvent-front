import { Component, OnInit } from '@angular/core';
import { IdentificadorService } from './identificador.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IdentificadorDTO } from './identificador-dto';

@Component({
  selector: 'app-detalle-identificador',
  templateUrl: './detalle-identificador.component.html',
  providers: [IdentificadorService]
})
export class DetalleIdentificadorComponent implements OnInit {

  public titulo: string;
  public identificador: IdentificadorDTO;

  constructor(private identificadorService: IdentificadorService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  // tslint:disable-next-line:one-line
  ){

  }

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.identificadorService
          .getIdentificador(id)
          .subscribe(
            identificador => (
              this.identificador = identificador
            )
          );
      }
    });
  }

}
