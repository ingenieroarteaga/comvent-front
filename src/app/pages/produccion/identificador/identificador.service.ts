import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Identificador } from './identificador';
import { IdentificadorDTO } from './identificador-dto';

@Injectable()
export class IdentificadorService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });



  constructor( private http: HttpClient ) {
    this.url = GLOBAL.url;
  }

  getIdentificadores(): Observable<IdentificadorDTO[]> {
    return this.http.get(this.url + 'identificadores').pipe(
      map(response => response as IdentificadorDTO[])
    );
  }

  getIdentificador(id: number): Observable<IdentificadorDTO> {
    return this.http.get<IdentificadorDTO>(`${this.url + 'identificador'}/${id}`);
  }

  create(identificador: Identificador): Observable<Identificador> {
    return this.http.post<Identificador>(this.url + 'identificador', identificador, { headers: this.httpHeaders });
  }

  update(identificador: Identificador): Observable<Identificador> {
    return this.http.put<Identificador>(this.url + 'identificador', identificador, { headers: this.httpHeaders });
  }

  delete(id: number): Observable<Identificador> {
    return this.http.delete<Identificador>(`${this.url + 'identificador'}/${id}`, { headers: this.httpHeaders });
  }

}
