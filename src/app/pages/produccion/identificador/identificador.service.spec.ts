import { TestBed, inject } from '@angular/core/testing';

import { IdentificadorService } from './identificador.service';

describe('IdentificadorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IdentificadorService]
    });
  });

  it('should be created', inject([IdentificadorService], (service: IdentificadorService) => {
    expect(service).toBeTruthy();
  }));
});
