import { ClienteEmpresa } from "../../model/cliente-empresa/cliente-empresa";
import { Sucursal } from "../../herramienta/sucursal/sucursal";
import { PedidoDetalle } from "../../model/pedido-detalle/pedido-detalle";
import { Anticipo } from "../../model/anticipo";

export class Pedido {
    public id: number;
    public numero: number;
    public fecha: Date;
    public monto: number;
    public fechaBaja: Date;
    public clienteEmpresa: ClienteEmpresa;
    public sucursal: Sucursal;
    public pedidoDetalleList: PedidoDetalle[];
    public anticipoList: Anticipo[];
    
}
