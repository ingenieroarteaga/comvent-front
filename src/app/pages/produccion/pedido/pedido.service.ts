import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Pedido } from './pedido';

@Injectable()
export class PedidoService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getPedidos(): Observable<Pedido[]> {
    return this.http.get(this.url + 'pedidos').pipe(
      map(response => response as Pedido[])
    );
  }

  getPedido(id: number): Observable<Pedido> {
    return this.http.get<Pedido>(`${this.url + 'pedido'}/${id}`)
  }

  create(pedido: Pedido): Observable<Pedido> {
    return this.http.post<Pedido>(this.url + 'pedido', pedido, { headers: this.httpHeaders })
  }

  update(pedido: Pedido): Observable<Pedido> {
    return this.http.put<Pedido>(this.url + 'pedido', pedido, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Pedido> {
    return this.http.delete<Pedido>(`${this.url + 'pedido'}/${id}`, { headers: this.httpHeaders })
  }

}
