import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PedidoService } from './pedido.service';
import { Pedido } from './pedido';
import { PedidoDetalle } from '../../model/pedido-detalle/pedido-detalle';

@Component({
  selector: 'app-detalle-pedido',
  templateUrl: './detalle-pedido.component.html',
  providers: [PedidoService]
})
export class DetallePedidoComponent implements OnInit {

  public titulo: string;
  public pedido: Pedido;
  public detalleList: PedidoDetalle[];

  constructor(private pedidoService: PedidoService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ){

  }

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.pedidoService
          .getPedido(id)
          .subscribe(
            pedido => (
              this.pedido = pedido, this.detalleList = pedido.pedidoDetalleList
            )
          );
      }
    });
  }

}
