import { Component, OnInit } from '@angular/core';
import { Pedido } from './pedido';
import { PedidoService } from './pedido.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.css'],
  providers: [PedidoService]
})
export class PedidoComponent implements OnInit {

  public titulo: string;
  pedidos: Pedido[];
  p:number;
  constructor(
    private pedidoService: PedidoService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.pedidoService.getPedidos().subscribe(
      pedidos => this.pedidos = pedidos
    );
  }

}
