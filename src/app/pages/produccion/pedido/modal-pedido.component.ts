import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
import { Pedido } from "./pedido";
import { PedidoService } from "./pedido.service";
import { ClienteEmpresaService } from "../../model/cliente-empresa/cliente-empresa.service";
import { ClienteEmpresa } from "../../model/cliente-empresa/cliente-empresa";
import { Observable } from "rxjs";
import { FormControl } from "@angular/forms";
import { startWith, map } from "rxjs/operators";
import { Anticipo } from "../../model/anticipo";
import { Bandeja } from "../../herramienta/bandeja/bandeja";
import { BandejaService } from "../../herramienta/bandeja/bandeja.service";
import { PedidoDetalle } from "../../model/pedido-detalle/pedido-detalle";
import { Variedad } from "../../herramienta/variedad/variedad";
import { VariedadService } from "../../herramienta/variedad/variedad.service";
import { BandejaStock } from "../../model/bandeja-stock/bandeja-stock";

@Component({
  selector: "modal-pedido",
  templateUrl: "./modal-pedido.html",
  providers: [
    PedidoService,
    ClienteEmpresaService,
    BandejaService,
    VariedadService
  ]
})
export class ModalPedido implements OnInit {
  public pedido: Pedido = new Pedido();
  public titulo: string = "Crear Pedido";
  public isSaving: boolean = false;
  //
  public anticipo: Anticipo = new Anticipo();
  ////
  public clienteEmpresa: ClienteEmpresa = new ClienteEmpresa();
  public clienteEmpresas: ClienteEmpresa[];
  //
  filteredClienteEmpresas: Observable<any[]>;
  myControl: FormControl = new FormControl();
  valStr: string;
  ////// detalle
  public detalleList: PedidoDetalle[];
  public detalle: PedidoDetalle = new PedidoDetalle();
  //
  public bandejas: Bandeja[];
  //public bandeja: Bandeja = new Bandeja();
  public bandejaStock: BandejaStock = new BandejaStock();
  //
  public variedades: Variedad[];
  public variedad: Variedad = new Variedad();
  //
  filteredVariedades: Observable<any[]>;
  myControlVar: FormControl = new FormControl();
  valStrVar: string;

  constructor(
    private pedidoService: PedidoService,
    private clienteEmpresaService: ClienteEmpresaService,
    private bandejaService: BandejaService,
    private variedadService: VariedadService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.variedadService
      .getVariedades()
      .subscribe(variedades => (this.variedades = variedades));
    this.autoVariedad();

    this.bandejaService
      .getBandejas()
      .subscribe(bandejas => (this.bandejas = bandejas));
    ///
    this.clienteEmpresaService
      .getClientesEmpresasActivos()
      .subscribe(clienteEmpresas => (this.clienteEmpresas = clienteEmpresas));
    this.autoClienteEmpresa();
    ///
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.pedidoService
          .getPedido(id)
          .subscribe(
            pedido => (
              (this.pedido = pedido),
              (this.anticipo = pedido.anticipoList[0]),
              (this.clienteEmpresa = pedido.clienteEmpresa),
              (this.detalleList = pedido.pedidoDetalleList)
            )
          );
      }
    });
  }

  save(): void {
    if (
      typeof this.clienteEmpresa !== "undefined" &&
      this.clienteEmpresa !== null &&
      typeof this.clienteEmpresa.id !== "undefined" &&
      this.clienteEmpresa.id !== null &&
      typeof this.detalleList !== "undefined" &&
      this.detalleList !== null &&
      this.detalleList.length > 0
    ) {
      this.isSaving = true;
      // Detalle y empresa
      this.pedido.pedidoDetalleList = this.detalleList;
      this.pedido.clienteEmpresa = this.clienteEmpresa;
      // Si tiene anticipo lo cargo
      if(typeof this.anticipo !== 'undefined' && this.anticipo !== null){
        this.pedido.anticipoList = [];
        this.pedido.anticipoList.push(this.anticipo);
      }
      // fin anticipo
      if (typeof this.pedido.id === "undefined" || this.pedido.id === null) {
        this.pedidoService.create(this.pedido).subscribe(pedido => {
          this.close();
          //swal('Nuevo Pedido', `Pedido ${pedido.descripcion} creado con éxito!`, 'success');
          this.refresh();
        });
      } else {
        this.pedidoService.update(this.pedido).subscribe(pedido => {
          this.close();
          //swal('Pedido Actualizado', `Pedido ${pedido.descripcion} actualizado con éxito!`, 'success');
          this.refresh();
        });
      }
    } else {
      swal(
        "Pedido",
        `Debe cargar todos los datos requeridos para realizar al pedido`,
        "error"
      );
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["produccion/pedidos"]);
  }

  ////////// Select ///////////////////////////////////////
  compareBandeja(t1: Bandeja, t2: Bandeja): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  //////////////////////////////////////////////////////////
  ///////// autocomplete Cliente Empresa /////////////////
  autoClienteEmpresa() {
    this.filteredClienteEmpresas = this.myControl.valueChanges.pipe(
      startWith(""),
      map(val => this.filter(val))
    );
  }
  filter(val) {
    if (typeof val === "undefined") {
      this.valStr = "";
    } else {
      this.valStr = val.toString();
    }
    if (this.clienteEmpresas) {
      return this.clienteEmpresas.filter(
        clienteEmpresas =>
          clienteEmpresas.empresa.cuit
            .toString()
            .toLowerCase()
            .includes(this.valStr.toLowerCase()) ||
          clienteEmpresas.empresa.razonSocial
            .toLowerCase()
            .includes(this.valStr.toLowerCase()) ||
          (clienteEmpresas.cliente.persona.nombre !== null &&
            clienteEmpresas.cliente.persona.nombre
              .toLowerCase()
              .includes(this.valStr.toLowerCase())) ||
          (clienteEmpresas.cliente.persona.apellido !== null &&
            clienteEmpresas.cliente.persona.apellido
              .toLowerCase()
              .includes(this.valStr.toLowerCase())) ||
          clienteEmpresas.cliente.persona.cuit
            .toString()
            .toLocaleLowerCase()
            .includes(this.valStr.toLowerCase())
      );
    }
  }

  public getDisplayFn() {
    return val => this.display(val);
  }
  private display(cliEmp): string {
    //access component "this" here
    if (
      cliEmp === null ||
      typeof cliEmp.cliente === "undefined" ||
      cliEmp.cliente === null ||
      typeof cliEmp.cliente.persona === "undefined" ||
      cliEmp.cliente.persona === null
    ) {
      return null;
    } else {
      var v_retorno = "Cliente: " + cliEmp.cliente.persona.nombre;
      if (cliEmp.cliente.persona.apellido !== null) {
        v_retorno = v_retorno + " " + cliEmp.cliente.persona.apellido;
      }
      v_retorno = v_retorno + " - " + cliEmp.cliente.persona.cuit;
      v_retorno = v_retorno + " | Empresa: " + cliEmp.empresa.razonSocial;
      v_retorno = v_retorno + " - " + cliEmp.empresa.cuit;
      return cliEmp ? v_retorno : cliEmp;
    }
  }

  private selected(cliEmp) {
    console.log(cliEmp);
  }

  /////////Fin autocomplete  Cliente Empresa //////////////////////////////////////

  ///////////////////// DETALLE //////////////////////////////////////////////////

  ///////// autocomplete Variedad /////////////////
  autoVariedad() {
    this.filteredVariedades = this.myControlVar.valueChanges.pipe(
      startWith(""),
      map(val => this.filterVar(val))
    );
  }
  filterVar(val) {
    this.valStrVar = val.toString();
    if (this.variedades) {
      return this.variedades.filter(
        variedades =>
          variedades.especie.descripcion
            .toLowerCase()
            .includes(this.valStrVar.toLowerCase()) ||
          variedades.descripcion
            .toLowerCase()
            .includes(this.valStrVar.toLowerCase())
      );
    }
  }

  public getDisplayFnVar() {
    return val => this.displayVar(val);
  }

  private displayVar(variedad): string {
    //access component "this" here
    if (variedad === null || typeof variedad.especie === "undefined") {
      return null;
    } else {
      return variedad
        ? variedad.especie.descripcion + " - " + variedad.descripcion
        : variedad;
    }
  }

  private selectedVar(variedad) {
    console.log(variedad);
  }

  /////////Fin autocomplete  Variedades //////////////////////////////////////

  /// Calcular cantidades ///
  calcCantidades() {
    if (typeof this.bandejaStock.bandeja !== 'undefined' &&
      this.bandejaStock.bandeja !== null &&
      this.bandejaStock.bandeja.capacidad > 0 &&
      this.detalle.cantidadPlantas > 0 &&
      this.detalle.porcGerminacion >= 0
    ) {
      var v_plantaPorc = this.detalle.cantidadPlantas * (this.detalle.porcGerminacion / 100);
      this.detalle.cantidadBandeja =
        (this.detalle.cantidadPlantas + v_plantaPorc) / this.bandejaStock.bandeja.capacidad;
        this.detalle.cantidadBandeja = Math.ceil(this.detalle.cantidadBandeja);
    } else {
      this.detalle.cantidadBandeja = 0;
    }
    this.calcMontos();
  }

  /// Calcular montos ///
  calcMontos() {
    if (this.detalle.cantidadPlantas > 0 && this.detalle.precioUnitario >= 0 && this.detalle.precioProduccion >= 0) {
      this.detalle.subTotal =
        this.detalle.cantidadPlantas * (this.detalle.precioUnitario + this.detalle.precioProduccion);
      this.detalle.subTotal = parseFloat(this.detalle.subTotal.toFixed(2));
    } else {
      this.detalle.subTotal = 0;
    }
  }

  ////////// Add Detalle //////////////
  addDetalle() {
    if (
      this.variedad == null ||
      this.bandejaStock == null ||
      this.detalle.cantidadBandeja == null ||
      this.detalle.precioUnitario == null ||
      this.detalle.precioProduccion == null ||
      this.detalle.cantidadPlantas == null
    ) {
      swal(
        "Agregar Detalle",
        `Debe completar todos los campos antes de guardar`,
        "error"
      );
    } else {
      if (typeof this.detalleList === "undefined") {
        this.detalleList = [];
      }
      this.detalle.bandejaStock = this.bandejaStock;
      this.detalle.variedad = this.variedad;
      this.detalleList.push(this.detalle);

      if (
        typeof this.pedido.monto == "undefined" &&
        this.pedido.monto == null
      ) {
        this.pedido.monto = 0;
      }
      this.pedido.monto = this.pedido.monto + this.detalle.subTotal;

      this.detalle = new PedidoDetalle();
      this.variedad = new Variedad();
      this.bandejaStock = new BandejaStock();
    }
  }

  //////////////// Funcion de semana a date /////////////////////////////////////
  calcSemanas(){
    var v_anio = this.detalle.semanaEntrega.substring(0,4);
    var v_semana = this.detalle.semanaEntrega.substring(6,8);
    //console.log(this.getDateOfWeek(v_semana, v_anio));
    //console.log(this.getDateOfISOWeek(v_semana, v_anio));
    var v_fechaEntrega = this.getDateOfISOWeek(v_semana, v_anio);
    var v_fechaSiembra = new Date(v_fechaEntrega);
    console.log(v_fechaEntrega);
    v_fechaSiembra.setDate(v_fechaSiembra.getDate() - this.detalle.dias);
    console.log(v_fechaSiembra);
    
    this.detalle.semanaSiembra = v_fechaSiembra.getFullYear() + "-W" + this.getWeekNumber(v_fechaSiembra);
  }

  getDateOfWeek(w, y) {
    var d = 1 + (w - 1) * 7; // 1st of January + 7 days for each week

    return new Date(y, 0, d);
  }

  getDateOfISOWeek(w, y) {
    var simple = new Date(y, 0, 1 + (w - 1) * 7);
    var dow = simple.getDay();
    var ISOweekStart = simple;
    if (dow <= 4) ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
    else ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
    return ISOweekStart;
  }

  private getWeekNumber(d: Date): number {
    // Copy date so don't modify original
    d = new Date(+d);
    d.setHours(0, 0, 0);
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setDate(d.getDate() + 4 - (d.getDay() || 7));
    // Get first day of year
    var yearStart = new Date(d.getFullYear(), 0, 1);
    // Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil((((d.valueOf() - yearStart.valueOf()) / 86400000) + 1) / 7);
    // Return array of year and week number
    return weekNo;
  }
  ///////////////// Fin de calculo de los week ///////////////////////////////////////////////


}
