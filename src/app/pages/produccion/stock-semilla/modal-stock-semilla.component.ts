import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
import { Observable } from "rxjs";
import { FormControl } from "@angular/forms";
import { startWith } from "rxjs/operators/startWith";
import { map } from "rxjs/operators/map";
import { Batch } from "../../model/batch/batch";
import { BatchService } from "../../model/batch/batch.service";
import { TipoMovStockSem } from "../../herramienta/tipo-mov-stock-sem/tipo-mov-stock-sem";
import { TipoMovStockSemService } from "../../herramienta/tipo-mov-stock-sem/tipo-mov-stock-sem.service";
import { StockSemillaMov } from "./stock-semilla-mov";
import { StockSemillaMovService } from "./stock-semilla-mov.service";
import { StockSemilla } from "./stock-semilla";


@Component({
  selector: "modal-stock-semilla",
  templateUrl: "./modal-stock-semilla.html",
  providers: [StockSemillaMovService, BatchService, TipoMovStockSemService]
})
export class ModalStockSemilla implements OnInit {
  public stockSemillaMov: StockSemillaMov = new StockSemillaMov();
  public titulo: string = "Mov. Stock de Semillas";
  public isSaving: boolean = false;
  //
  public cantPeso: number;
  public peso: number;
  //
  public batch: Batch = new Batch();
  public batchs: Batch[];
  //
  filteredBatchs: Observable<any[]>;
  myControlBatch: FormControl = new FormControl();
  valStrBatch: string;
  // Tipo Movimiento y Movimientos de Stock
  public tiposMovsStocksSem : TipoMovStockSem[];
  public tipoMovStockSem: TipoMovStockSem = new TipoMovStockSem();
  //
    

  constructor(
    private stockSemillaMovService: StockSemillaMovService,
    private batchService: BatchService,
    private tipoMovStockSemService: TipoMovStockSemService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.tipoMovStockSemService.getTipoMovStockSems().subscribe(tiposMovsStocksSem => (this.tiposMovsStocksSem = tiposMovsStocksSem));
    this.batchService.getBatchs().subscribe(batchs => (this.batchs = batchs));
    /////// Autocomplete ///////////////////////////////
    this.autoBatch();
    /////// fin autocomplete /////////
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.stockSemillaMovService
          .getStockSemillaMov(id)
          .subscribe(
            stockSemillaMov => (
              this.stockSemillaMov = stockSemillaMov,
              this.tipoMovStockSem = stockSemillaMov.tipoMovStockSem,
              this.batch = stockSemillaMov.stockSemilla.batch
            )
          );
      }
    });
  }

  /////////////////// SAVE //////////////////////////////////////////////////////////////
  save(): void {
    if(this.batch === null){
      swal('Stock Semilla', `Debe completar los campos obligatorios antes de guardar`, 'error');
    }else{
      this.isSaving = true;
      ////// Guardo el batch junto con la creacion del Stock
      //// Aca despues lo tengo que hacer variable cuando tenga varias sucursales tengo que editar esto
      var v_stockSemilla: StockSemilla = new StockSemilla();
      if(typeof this.batch.stockSemillaList !== 'undefined' && this.batch.stockSemillaList !== null && this.batch.stockSemillaList.length > 0){
        v_stockSemilla = this.batch.stockSemillaList[0];
      }else{
         v_stockSemilla.cantidad = this.stockSemillaMov.cantidad;
         v_stockSemilla.batch = this.batch;
         // v_stockSemilla.sucursal // Esta parte agrego despues
      }
      this.stockSemillaMov.stockSemilla = v_stockSemilla;
      console.log(this.stockSemillaMov.stockSemilla);
      ////// Fin Guardo el batch junto con la creacion del Stock
      this.stockSemillaMov.tipoMovStockSem = this.tipoMovStockSem;
      if (typeof this.stockSemillaMov.id === "undefined" || this.stockSemillaMov.id === null) {
        this.stockSemillaMovService.create(this.stockSemillaMov).subscribe(stockSemillaMov => {
          this.close();
          //swal('Nuevo StockSemilla', `StockSemilla ${stockSemillaMov.descripcion} creado con éxito!`, 'success');
          this.refresh();
        });
      } else {
        this.stockSemillaMovService.update(this.stockSemillaMov).subscribe(stockSemillaMov => {
          this.close();
          //swal('StockSemilla Actualizado', `StockSemilla ${stockSemillaMov.descripcion} actualizado con éxito!`, 'success');
          this.refresh();
        });
      }
    }
  }
  //////////////////////// FIN SAVE ///////////////////////////////////////////////////

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["produccion/stock-semillas"]);
  }

  
  ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////
  calcularCantidad(){
    if(this.cantPeso !== null && this.peso !== null){
      this.stockSemillaMov.cantidad = this.peso * this.cantPeso;
    }
  }

  ///////// autocomplete Batch /////////////////
  autoBatch() {
    this.filteredBatchs = this.myControlBatch.valueChanges.pipe(
      startWith(""),
      map(val => this.filterBatch(val))
    );
  }
  filterBatch(val) {
    this.valStrBatch = val.toString();
    if (this.batchs) {
      return this.batchs.filter(
        batchs =>
          batchs.lote.variedad.especie.descripcion
            .toLowerCase()
            .includes(this.valStrBatch.toLowerCase()) ||
            batchs.lote.variedad.descripcion
            .toLowerCase()
            .includes(this.valStrBatch.toLowerCase()) ||
            batchs.lote.numero
            .toLowerCase()
            .includes(this.valStrBatch.toLowerCase()) ||
          batchs.numero
            .toLowerCase()
            .includes(this.valStrBatch.toLowerCase())
      );
    }
  }

  public getDisplayFnBatch() {
    return val => this.displayBatch(val);
  }

  private displayBatch(batch): string {
    if (batch === null || typeof batch.lote === "undefined" || typeof batch.lote.variedad === "undefined" || 
        typeof batch.lote.variedad.especie === "undefined") {
      return null;
    } else {
      return batch
        ? batch.lote.variedad.especie.descripcion + " - " + batch.lote.variedad.descripcion + " - " + batch.lote.numero + " - " + batch.numero
        : batch;
    }
  }

  private selectedBatch(batch) {
    console.log(batch);
  }

  /////////Fin autocomplete batch //////////////////////////////////////

  ////////////// Tipo Mov Stock Semillas //////////////////////////////
  compareTipoMov(t1: TipoMovStockSem, t2: TipoMovStockSem): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }
  ////////////// Fin Tipo Mov Stock Semillas //////////////////////////////

  
}
