import { Batch } from "../../model/batch/batch";
import { Sucursal } from "../../herramienta/sucursal/sucursal";
import { StockSemillaMov } from "./stock-semilla-mov";

export class StockSemilla {
    public id: number;
    public cantidad: number;
    public batch: Batch;
    public sucursal: Sucursal;
    public stockSemillaMovList: StockSemillaMov[];
    
}
