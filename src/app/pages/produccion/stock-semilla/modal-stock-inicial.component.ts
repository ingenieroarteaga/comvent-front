import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
import { Observable } from "rxjs";
import { FormControl } from "@angular/forms";
import { startWith } from "rxjs/operators/startWith";
import { map } from "rxjs/operators/map";
import { StockSemilla } from "./stock-semilla";
import { StockSemillaService } from "./stock-semilla.service";
import { VariedadService } from "../../herramienta/variedad/variedad.service";
import { Variedad } from "../../herramienta/variedad/variedad";
import { Batch } from "../../model/batch/batch";
import { Lote } from "../../model/lote";
import { BatchService } from "../../model/batch/batch.service";

@Component({
  selector: "modal-stock-inicial",
  templateUrl: "./modal-stock-inicial.html",
  providers: [StockSemillaService, VariedadService, BatchService]
})
export class ModalStockInicial implements OnInit {
  public stockSemilla: StockSemilla = new StockSemilla();
  public titulo: string = "Stock Inicial";
  public isSaving: boolean = false;
  //
  public cantPeso: number;
  public peso: number;
  //
  public variedad: Variedad = new Variedad();
  public variedades: Variedad[];
  //
  filteredVariedades: Observable<any[]>;
  myControlVar: FormControl = new FormControl();
  valStrVar: string;
  //
  public batch : Batch = new Batch();
  public lote : Lote = new Lote();


  constructor(
    private stockSemillaService: StockSemillaService,
    private variedadService: VariedadService,
    private batchService: BatchService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.variedadService.getVariedades().subscribe(variedades => (this.variedades = variedades));
    /////// Autocomplete ///////////////////////////////
    this.autoVariedad();
    /////// fin autocomplete /////////
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.stockSemillaService
          .getStockSemilla(id)
          .subscribe(
            stockSemilla => (
              this.stockSemilla = stockSemilla,
              this.variedad = stockSemilla.batch.lote.variedad
            )
          );
      }
    });
  }

  /////////////////// SAVE //////////////////////////////////////////////////////////////
  save(): void {
    if(this.variedad === null){
      swal('Stock Semilla', `Debe completar los campos obligatorios antes de guardar`, 'error');
    }else{
      this.isSaving = true;
      //// Aca despues lo tengo que hacer variable cuando tenga varias sucursales tengo que editar esto
      // v_stockSemilla.sucursal // Esta parte agrego despues
      if(typeof this.batch.numero === 'undefined' || this.batch.numero == null){
        this.batch.numero = this.lote.numero;
        this.batch.descripcion = "Numero pasado desde lote";
      }
      this.lote.variedad = this.variedad;
      this.batch.lote = this.lote;
      this.stockSemilla.batch = this.batch;
      ////// Fin Guardo el batch junto con la creacion del Stock
      if (typeof this.stockSemilla.id === "undefined" || this.stockSemilla.id === null) {
        this.stockSemillaService.createInicial(this.stockSemilla).subscribe(stockSemilla => {
          this.close();
          //swal('Nuevo StockSemilla', `StockSemilla ${stockSemillaMov.descripcion} creado con éxito!`, 'success');
          this.refresh();
        });
      } else {
        /*this.stockSemillaService.update(this.stockSemilla).subscribe(stockSemilla => {
          this.close();
          this.refresh();
        });*/
        swal('Stock Inicial', `No se puede editar el Stock!`, 'warning');
      }
    }
  }
  //////////////////////// FIN SAVE ///////////////////////////////////////////////////

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["produccion/stock-semillas"]);
  }


  ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////
  ///////// autocomplete Variedad /////////////////
  autoVariedad() {
    this.filteredVariedades = this.myControlVar.valueChanges.pipe(
      startWith(""),
      map(val => this.filterVar(val))
    );
  }
  filterVar(val) {
    this.valStrVar = val.toString();
    if (this.variedades) {
      return this.variedades.filter(
        variedades =>
          variedades.especie.descripcion
            .toLowerCase()
            .includes(this.valStrVar.toLowerCase()) ||
          variedades.descripcion
            .toLowerCase()
            .includes(this.valStrVar.toLowerCase())
      );
    }
  }

  public getDisplayFnVar() {
    return val => this.displayVar(val);
  }

  private displayVar(variedad): string {
    //access component "this" here
    if (variedad === null || typeof variedad.especie === "undefined") {
      return null;
    } else {
      return variedad
        ? variedad.especie.descripcion + " - " + variedad.descripcion
        : variedad;
    }
  }

  private selectedVar(variedad) {
    console.log(variedad);
  }

  /////////Fin autocomplete  Variedades //////////////////////////////////////
  validaBatch(){
    this.batchService.getBatchByNumero(this.batch.numero).toPromise()
      .then(batch => {
        if(typeof batch !== 'undefined' && batch !== null){
          swal('Validación Batch', 'Ya existe un Batch con este número ingresado en el sistema', 'error');
          this.batch.numero = null;
        }
      });
  }

  
}
