import { Component, OnInit } from '@angular/core';
import { StockSemilla } from './stock-semilla';
import { StockSemillaService } from './stock-semilla.service';
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-stock-semilla',
  templateUrl: './stock-semilla.component.html',
  styleUrls: ['./stock-semilla.component.css'],
  providers: [StockSemillaService]
})
export class StockSemillaComponent implements OnInit {

  public titulo: string;
  stockSemillas: StockSemilla[];
  p:number;

  constructor(private stockSemillaService: StockSemillaService,
  private modalService: NgbModal,
  private _route: ActivatedRoute,
  private _router: Router)   {


  }

  ngOnInit() {
    this.stockSemillaService.getStockSemillas().subscribe(
      stockSemillas => this.stockSemillas = stockSemillas
    );
  }

}
