import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockSemillaComponent } from './stock-semilla.component';

describe('StockSemillaComponent', () => {
  let component: StockSemillaComponent;
  let fixture: ComponentFixture<StockSemillaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockSemillaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockSemillaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
