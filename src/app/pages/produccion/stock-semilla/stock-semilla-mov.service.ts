import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { StockSemillaMov } from './stock-semilla-mov';



@Injectable()

export class StockSemillaMovService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getStockSemillaMovs(): Observable<StockSemillaMov[]> {
    return this.http.get(this.url + 'stocks-semillas-movs').pipe(
      map(response => response as StockSemillaMov[])
    );
  }

  getStockSemillaMov(id: number): Observable<StockSemillaMov> {
    return this.http.get<StockSemillaMov>(`${this.url + 'stock-semilla-mov'}/${id}`)
  }

  create(sotckSemilla: StockSemillaMov): Observable<StockSemillaMov> {
    return this.http.post<StockSemillaMov>(this.url + 'stock-semilla-mov', sotckSemilla, { headers: this.httpHeaders })
  }

  update(sotckSemilla: StockSemillaMov): Observable<StockSemillaMov> {
    return this.http.put<StockSemillaMov>(this.url + 'stock-semilla-mov', sotckSemilla, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<StockSemillaMov> {
    return this.http.delete<StockSemillaMov>(`${this.url + 'stock-semilla-mov'}/${id}`, { headers: this.httpHeaders })
  }

}
