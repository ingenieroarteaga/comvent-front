import { StockSemilla } from "./stock-semilla";
import { TipoMovStockSem } from "../../herramienta/tipo-mov-stock-sem/tipo-mov-stock-sem";

export class StockSemillaMov {
    public id: number;
    public cantidad: number;
    public stockSemilla: StockSemilla;
    public tipoMovStockSem: TipoMovStockSem;
}
