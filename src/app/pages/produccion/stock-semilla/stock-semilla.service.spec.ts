import { TestBed, inject } from '@angular/core/testing';

import { StockSemillaService } from './stock-semilla.service';

describe('StockSemillaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StockSemillaService]
    });
  });

  it('should be created', inject([StockSemillaService], (service: StockSemillaService) => {
    expect(service).toBeTruthy();
  }));
});
