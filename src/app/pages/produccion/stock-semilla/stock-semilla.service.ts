import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { StockSemilla } from './stock-semilla';


@Injectable()

export class StockSemillaService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getStockSemillas(): Observable<StockSemilla[]> {
    return this.http.get(this.url + 'stock-semillas').pipe(
      map(response => response as StockSemilla[])
    );
  }

  getStockSemilla(id: number): Observable<StockSemilla> {
    return this.http.get<StockSemilla>(`${this.url + 'stock-semilla'}/${id}`)
  }

  create(sotckSemilla: StockSemilla): Observable<StockSemilla> {
    return this.http.post<StockSemilla>(this.url + 'stock-semilla', sotckSemilla, { headers: this.httpHeaders })
  }

  update(sotckSemilla: StockSemilla): Observable<StockSemilla> {
    return this.http.put<StockSemilla>(this.url + 'stock-semilla', sotckSemilla, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<StockSemilla> {
    return this.http.delete<StockSemilla>(`${this.url + 'stock-semilla'}/${id}`, { headers: this.httpHeaders })
  }

  //////////////////
  createInicial(sotckSemilla: StockSemilla): Observable<StockSemilla> {
    return this.http.post<StockSemilla>(this.url + 'stock-inicial', sotckSemilla, { headers: this.httpHeaders })
  }

  //////////////////
  getStockSemillaVariedadDisp(variedadId: number, sucursalId: number): Observable<StockSemilla> {
    return this.http.get<StockSemilla>(`${this.url + 'stock-variedad-disp'}/${variedadId}/${sucursalId}`)
  }

}
