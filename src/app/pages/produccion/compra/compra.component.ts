import { Component, OnInit } from '@angular/core';
import { CompraService } from './compra.service';
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Compra } from './compra';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-compra',
  templateUrl: './compra.component.html',
  styleUrls: ['./compra.component.css'],
  providers: [CompraService]
})

export class CompraComponent implements OnInit {

  public titulo: string;
  compras: Compra[];
  p:number;

  constructor(private compraService: CompraService,
  private modalService: NgbModal,
  private _route: ActivatedRoute,
  private _router: Router)   {


  }

  ngOnInit() {
    this.compraService.getCompras().subscribe(
      compras => this.compras = compras
    );
  }

}
