import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Compra } from "./compra";
import { CompraService } from "./compra.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
import { Observable } from "rxjs";
import { FormControl } from "@angular/forms";
import { startWith } from "rxjs/operators/startWith";
import { map } from "rxjs/operators/map";
import { ProveedorService } from "../../personas/proveedor/proveedor.service";
import { Proveedor } from "../../personas/proveedor/proveedor";
import { VariedadService } from "../../herramienta/variedad/variedad.service";
import { Variedad } from "../../herramienta/variedad/variedad";
import { Lote } from "../../model/lote";
import { Batch } from "../../model/batch/batch";
import { CompraDetalle } from "../../model/compra-detalle";
import { BatchService } from "../../model/batch/batch.service";


@Component({
  selector: "modal-compra",
  templateUrl: "./modal-compra.html",
  providers: [CompraService, ProveedorService, VariedadService, BatchService]
})
export class ModalCompra implements OnInit {
  public compra: Compra = new Compra();
  public titulo: string = "Crear Compra";
  public isSaving: boolean = false;
  //
  public proveedores: Proveedor[];
  public proveedor: Proveedor = new Proveedor();
  //
  filteredProveedores: Observable<any[]>;
  myControl: FormControl = new FormControl();
  valStr: string;
  ////////// Detalle
  //
  public detalle: CompraDetalle = new CompraDetalle();
  public detalleList: CompraDetalle[];
  public lote: Lote = new Lote();
  public batch: Batch = new Batch();
  //
  public variedades: Variedad[];
  public variedad: Variedad = new Variedad();
  //
  filteredVariedades: Observable<any[]>;
  myControlVar: FormControl = new FormControl();
  valStrVar: string;
  //

  constructor(
    private compraService: CompraService,
    private proveedorService: ProveedorService,
    private variedadService: VariedadService,
    private batchService: BatchService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.proveedorService
      .getProveedores()
      .subscribe(proveedores => (this.proveedores = proveedores));
    /// Detalle
    this.variedadService
      .getVariedades()
      .subscribe(variedades => (this.variedades = variedades));
    /////// Autocomplete ///////////////////////////////
    this.autoProveedor();
    this.autoVariedad();
    /////// fin autocomplete /////////
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.compraService
          .getCompra(id)
          .subscribe(
            compra => (
              this.compra = compra, this.proveedor = compra.proveedor, this.detalleList = compra.compraDetalleList
            )
          );
      }
    });
  }

  /////////////////// SAVE //////////////////////////////////////////////////////////////
  save(): void {
    if (typeof this.proveedor !== 'undefined' && this.proveedor !== null && 
      typeof this.detalleList !== 'undefined' && this.detalleList !== null && this.detalleList.length > 0){
      this.isSaving = true;
      this.compra.proveedor = this.proveedor;
      this.compra.compraDetalleList = this.detalleList;
      if (typeof this.compra.id === "undefined" || this.compra.id === null) {
        this.compraService.create(this.compra).subscribe(compra => {
          this.close();
          //swal('Nuevo Compra', `Compra ${compra.descripcion} creado con éxito!`, 'success');
          this.refresh();
        });
      } else {
        this.compraService.update(this.compra).subscribe(compra => {
          this.close();
          //swal('Compra Actualizado', `Compra ${compra.descripcion} actualizado con éxito!`, 'success');
          this.refresh();
        });
      }
    }else{
      swal('Compra', `Debe completar los campos requeridos y agregar al menos un detalle para guardar`, 'error');
    }
  }
  //////////////////////// FIN SAVE ///////////////////////////////////////////////////

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["produccion/compras"]);
  }

  ///////// autocomplete Proveedor /////////////////
  autoProveedor() {
    this.filteredProveedores = this.myControl.valueChanges.pipe(
      startWith(""),
      map(val => this.filter(val))
    );
  }
  filter(val) {
    if(typeof val === 'undefined'){
      this.valStr = "";
    }else{
      this.valStr = val.toString();
    }
    if (this.proveedores) {
      return this.proveedores.filter(
        proveedores =>
          proveedores.persona.nombre.toLowerCase().includes(this.valStr.toLowerCase()) ||
          ( proveedores.persona.apellido !== null && proveedores.persona.apellido
              .toLowerCase().includes(this.valStr.toLowerCase())
          ) ||
          proveedores.persona.cuit
            .toString().toLocaleLowerCase()
            .includes(this.valStr.toLowerCase())
      );
    }
  }

  public getDisplayFn() {
    return val => this.display(val);
  }
  private display(prov): string {
    //access component "this" here
    if (prov === null || typeof prov.persona === "undefined" || prov.persona === null) {
      return null;
    } else {
      var v_retorno = prov.persona.nombre;
      if(prov.persona.apellido !== null){
        v_retorno = v_retorno + " " + prov.persona.apellido;
      }
      v_retorno = v_retorno + " - " + prov.persona.cuit;
      return prov ? v_retorno : prov;
    }
  }

  private selected(prov) {
    console.log(prov);
  }

  /////////Fin autocomplete  Proveedor //////////////////////////////////////


  ///////////////////////////////////////////////////////////////////////////
  //////////////////////// DETALLE //////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////

  ///////// autocomplete Variedad /////////////////
  autoVariedad() {
    this.filteredVariedades = this.myControlVar.valueChanges.pipe(
      startWith(""),
      map(val => this.filterVar(val))
    );
  }
  filterVar(val) {
    this.valStrVar = val.toString();
    if (this.variedades) {
      return this.variedades.filter(
        variedades =>
          variedades.especie.descripcion
            .toLowerCase()
            .includes(this.valStrVar.toLowerCase()) ||
          variedades.descripcion
            .toLowerCase()
            .includes(this.valStrVar.toLowerCase())
      );
    }
  }

  public getDisplayFnVar() {
    return val => this.displayVar(val);
  }

  private displayVar(variedad): string {
    //access component "this" here
    if (variedad === null || typeof variedad.especie === "undefined") {
      return null;
    } else {
      return variedad
        ? variedad.especie.descripcion + " - " + variedad.descripcion
        : variedad;
    }
  }

  private selectedVar(variedad) {
    console.log(variedad);
  }

  /////////Fin autocomplete  Variedades //////////////////////////////////////


  /// Calcular cantidades ///
  calcCantidades(){
    if(this.detalle.cantEnvases > 0 && this.batch.capacidadEnvases > 0){
      this.detalle.cantidad = this.detalle.cantEnvases * this.batch.capacidadEnvases;
    }else{
      this.detalle.cantidad = 0;
    }
    this.calcMontos();
  }

  /// Calcular montos ///
  calcMontos(){
    if(this.detalle.cantEnvases > 0 && this.detalle.precioUnitario > 0){
      if(this.detalle.porcBonif > 0){
        this.detalle.montoBonif = (this.detalle.cantEnvases * this.detalle.precioUnitario) * (this.detalle.porcBonif / 100);
      }else{
        this.detalle.montoBonif = 0;
      }
      this.detalle.subTotal = (this.detalle.cantEnvases * this.detalle.precioUnitario) - this.detalle.montoBonif;
      if(this.detalle.porcIva > 0){
        this.detalle.montoIva = this.detalle.subTotal * (this.detalle.porcIva / 100);
      }else{
        this.detalle.montoIva = 0;
      }
      this.detalle.subTotalIva = this.detalle.subTotal + this.detalle.montoIva;
    }
  }

  ////////// Add Detalle //////////////
  addDetalle(){
    if(this.variedad == null || this.lote == null || this.batch == null || this.detalle.cantidad == null
      || this.detalle.precioUnitario == null || this.detalle.cantEnvases == null || this.batch.capacidadEnvases == null){
        swal('Agregar Detalle', `Debe completar todos los campos antes de guardar`, 'error');
      }else{
        if(typeof this.detalleList === 'undefined'){
          this.detalleList = [];
        }
        if(typeof this.batch.numero === 'undefined' || this.batch.numero == null){
          this.batch.numero = this.lote.numero;
          this.batch.descripcion = "Numero pasado desde lote";
        }
        this.lote.variedad = this.variedad;
        this.batch.lote = this.lote;
        this.detalle.batch = this.batch;
        this.detalleList.push(this.detalle);

        if(this.compra.montoIva == null && typeof this.compra.montoIva == 'undefined'){
          this.compra.montoIva = 0;
        }
        if(this.compra.monto == null && typeof this.compra.monto == 'undefined'){
          this.compra.monto = 0;
        }
        this.compra.montoIva = this.compra.montoIva + this.detalle.montoIva;
        this.compra.monto = this.compra.monto + this.detalle.subTotalIva;

        this.detalle = new CompraDetalle();
        this.variedad = new Variedad();
        this.lote = new Lote();
        this.batch = new Batch();
      }
    
  }


  //////////////////////////////////// BATCH
  /////////Fin autocomplete  Variedades //////////////////////////////////////
  validaBatch(){
    this.batchService.getBatchByNumero(this.batch.numero).toPromise()
      .then(batch => {
        if(typeof batch !== 'undefined' && batch !== null){
          swal('Validación Batch', 'Ya existe un Batch con este número ingresado en el sistema', 'warning');
          this.batch = batch;          
        }
      });
  }

  
  
}
