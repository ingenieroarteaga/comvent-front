import { Component, OnInit } from '@angular/core';
import { CompraService } from './compra.service';
import { Compra } from './compra';
import { ActivatedRoute, Router } from '@angular/router';
import { CompraDetalle } from '../../model/compra-detalle';
import { Proveedor } from '../../personas/proveedor/proveedor';

@Component({
  selector: 'app-detalle-compra',
  templateUrl: './detalle-compra.component.html',
  providers: [CompraService]
})
export class DetalleCompraComponent implements OnInit {

  public titulo: string;
  public compra: Compra;
  public detalleList: CompraDetalle[];

  constructor(private compraService: CompraService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ){

  }

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.compraService
          .getCompra(id)
          .subscribe(
            compra => (
              this.compra = compra, this.detalleList = compra.compraDetalleList
            )
          );
      }
    });
  }

}
