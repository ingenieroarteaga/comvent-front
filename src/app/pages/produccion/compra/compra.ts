import { Proveedor } from "../../personas/proveedor/proveedor";
import { CompraDetalle } from "../../model/compra-detalle";
import { CuentaProveedor } from "../../administracion/cuenta-proveedor/cuenta-proveedor";

export class Compra {
    public id: number;
    public fecha: Date;
    public numero: number;
    public monto: number;
    public montoIva: number;
    public fechaBaja: Date;
    public proveedor: Proveedor;
    public cuentaProveedorDetList: CuentaProveedor[];
    public compraDetalleList: CompraDetalle[];
}
