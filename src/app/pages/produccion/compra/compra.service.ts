import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Compra } from './compra';

@Injectable()
export class CompraService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getCompras(): Observable<Compra[]> {
    return this.http.get(this.url + 'compras').pipe(
      map(response => response as Compra[])
    );
  }

  getCompra(id: number): Observable<Compra> {
    return this.http.get<Compra>(`${this.url + 'compra'}/${id}`)
  }

  create(compra: Compra): Observable<Compra> {
    return this.http.post<Compra>(this.url + 'compra', compra, { headers: this.httpHeaders })
  }

  update(compra: Compra): Observable<Compra> {
    return this.http.put<Compra>(this.url + 'compra', compra, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Compra> {
    return this.http.delete<Compra>(`${this.url + 'compra'}/${id}`, { headers: this.httpHeaders })
  }

}
