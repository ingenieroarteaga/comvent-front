import { Batch } from "../../model/batch/batch";
import { AnalisisBatchDetalle } from "../../model/analisis-batch-detalle";

export class AnalisisBatch {
    public id: number;
    public fecha: Date;
    public pesoEnvase: number;
    public pesoMuestra: number;
    public cantSemillasMuestra: number;
    public batch: Batch;
    public analisisBatchDetalleList: AnalisisBatchDetalle[];
    
}
