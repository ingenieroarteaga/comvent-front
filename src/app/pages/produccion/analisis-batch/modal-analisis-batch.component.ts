import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
import { Observable } from "rxjs";
import { FormControl } from "@angular/forms";
import { startWith } from "rxjs/operators/startWith";
import { map } from "rxjs/operators/map";
import { VariedadService } from "../../herramienta/variedad/variedad.service";
import { Variedad } from "../../herramienta/variedad/variedad";
import { Lote } from "../../model/lote";
import { Batch } from "../../model/batch/batch";
import { AnalisisBatchService } from "./analisis-batch.service";
import { AnalisisBatch } from "./analisis-batch";


@Component({
  selector: "modal-analisis-batch",
  templateUrl: "./modal-analisis-batch.html",
  providers: [AnalisisBatchService, VariedadService]
})
export class ModalAnalisisBatch implements OnInit {
  public analisisBatch: AnalisisBatch = new AnalisisBatch();
  public titulo: string = "Crear Analisis Batch";
  public isSaving: boolean = false;
  //
  public lote: Lote = new Lote();
  public batch: Batch = new Batch();
  //
  public variedades: Variedad[];
  public variedad: Variedad = new Variedad();
  //
  filteredVariedades: Observable<any[]>;
  myControlVar: FormControl = new FormControl();
  valStrVar: string;
  //

  constructor(
    private analisisBatchService: AnalisisBatchService,
    private variedadService: VariedadService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.variedadService
      .getVariedades()
      .subscribe(variedades => (this.variedades = variedades));
    /////// Autocomplete ///////////////////////////////
    this.autoVariedad();
    /////// fin autocomplete /////////
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.analisisBatchService
          .getAnalisisBatch(id)
          .subscribe(
            analisisBatch => (
              this.analisisBatch = analisisBatch
            )
          );
      }
    });
  }

  /////////////////// SAVE //////////////////////////////////////////////////////////////
  save(): void {
    this.isSaving = true;
    if (typeof this.analisisBatch.id === "undefined" || this.analisisBatch.id === null) {
      this.analisisBatchService.create(this.analisisBatch).subscribe(analisisBatch => {
        this.close();
        //swal('Nuevo AnalisisBatch', `AnalisisBatch ${analisisBatch.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.analisisBatchService.update(this.analisisBatch).subscribe(analisisBatch => {
        this.close();
        //swal('AnalisisBatch Actualizado', `AnalisisBatch ${analisisBatch.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }
  //////////////////////// FIN SAVE ///////////////////////////////////////////////////

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["produccion/analisis-batchs"]);
  }

  
  //////////////////////////////////////////////////
  ///////// autocomplete Variedad /////////////////
  autoVariedad() {
    this.filteredVariedades = this.myControlVar.valueChanges.pipe(
      startWith(""),
      map(val => this.filterVar(val))
    );
  }
  filterVar(val) {
    this.valStrVar = val.toString();
    if (this.variedades) {
      return this.variedades.filter(
        variedades =>
          variedades.especie.descripcion
            .toLowerCase()
            .includes(this.valStrVar.toLowerCase()) ||
          variedades.descripcion
            .toLowerCase()
            .includes(this.valStrVar.toLowerCase())
      );
    }
  }

  public getDisplayFnVar() {
    return val => this.displayVar(val);
  }

  private displayVar(variedad): string {
    //access component "this" here
    if (variedad === null || typeof variedad.especie === "undefined") {
      return null;
    } else {
      return variedad
        ? variedad.especie.descripcion + " - " + variedad.descripcion
        : variedad;
    }
  }

  private selectedVar(variedad) {
    console.log(variedad);
  }

  /////////Fin autocomplete  Proveedor //////////////////////////////////////


  
  
  
}
