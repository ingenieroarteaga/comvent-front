import { Component, OnInit } from '@angular/core';
import { AnalisisBatch } from './analisis-batch';
import { AnalisisBatchService } from './analisis-batch.service';
import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-analisis-batch',
  templateUrl: './analisis-batch.component.html',
  styleUrls: ['./analisis-batch.component.css'],
  providers: [AnalisisBatchService]
})
export class AnalisisBatchComponent implements OnInit {

  public titulo: string;
  analisisBatchs: AnalisisBatch[];
  p:number;

  constructor(private analisisBatchService: AnalisisBatchService,
  private modalService: NgbModal,
  private _route: ActivatedRoute,
  private _router: Router)   {


  }

  ngOnInit() {
    this.analisisBatchService.getAnalisisBatchs().subscribe(
      analisisBatchs => this.analisisBatchs = analisisBatchs
    );
  }

}
