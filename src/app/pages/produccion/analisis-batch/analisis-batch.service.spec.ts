import { TestBed, inject } from '@angular/core/testing';

import { AnalisisBatchService } from './analisis-batch.service';

describe('AnalisisBatchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AnalisisBatchService]
    });
  });

  it('should be created', inject([AnalisisBatchService], (service: AnalisisBatchService) => {
    expect(service).toBeTruthy();
  }));
});
