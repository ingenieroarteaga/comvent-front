import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AnalisisBatch } from './analisis-batch';


@Injectable()
export class AnalisisBatchService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getAnalisisBatchs(): Observable<AnalisisBatch[]> {
    return this.http.get(this.url + 'analisis-batchs').pipe(
      map(response => response as AnalisisBatch[])
    );
  }

  getAnalisisBatch(id: number): Observable<AnalisisBatch> {
    return this.http.get<AnalisisBatch>(`${this.url + 'analisis-batch'}/${id}`)
  }

  create(analisisBatch: AnalisisBatch): Observable<AnalisisBatch> {
    return this.http.post<AnalisisBatch>(this.url + 'analisis-batch', analisisBatch, { headers: this.httpHeaders })
  }

  update(analisisBatch: AnalisisBatch): Observable<AnalisisBatch> {
    return this.http.put<AnalisisBatch>(this.url + 'analisis-batch', analisisBatch, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<AnalisisBatch> {
    return this.http.delete<AnalisisBatch>(`${this.url + 'analisis-batch'}/${id}`, { headers: this.httpHeaders })
  }

}
