import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalisisBatchComponent } from './analisis-batch.component';

describe('AnalisisBatchComponent', () => {
  let component: AnalisisBatchComponent;
  let fixture: ComponentFixture<AnalisisBatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalisisBatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalisisBatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
