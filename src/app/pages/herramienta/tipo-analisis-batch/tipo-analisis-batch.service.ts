import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { TipoAnalisisBatch } from './tipo-analisis-batch';

@Injectable()
export class TipoAnalisisBatchService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getTipoAnalisisBatchs(): Observable<TipoAnalisisBatch[]> {
    return this.http.get(this.url + 'tipos-analisis-batchs').pipe(
      map(response => response as TipoAnalisisBatch[])
    );
  }

  getTipoAnalisisBatch(id: number): Observable<TipoAnalisisBatch> {
    return this.http.get<TipoAnalisisBatch>(`${this.url + 'tipo-analisis-batch'}/${id}`)
  }

  create(tipoAnalisisBatch: TipoAnalisisBatch): Observable<TipoAnalisisBatch> {
    return this.http.post<TipoAnalisisBatch>(this.url + 'tipo-analisis-batch', tipoAnalisisBatch, { headers: this.httpHeaders })
  }

  update(tipoAnalisisBatch: TipoAnalisisBatch): Observable<TipoAnalisisBatch> {
    return this.http.put<TipoAnalisisBatch>(this.url + 'tipo-analisis-batch', tipoAnalisisBatch, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<TipoAnalisisBatch> {
    return this.http.delete<TipoAnalisisBatch>(`${this.url + 'tipo-analisis-batch'}/${id}`, { headers: this.httpHeaders })
  }
}
