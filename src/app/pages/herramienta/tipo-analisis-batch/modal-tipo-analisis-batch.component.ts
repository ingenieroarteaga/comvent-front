
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import swal from 'sweetalert2'
import { TipoAnalisisBatchService } from './tipo-analisis-batch.service';
import { TipoAnalisisBatch } from './tipo-analisis-batch';
@Component({
    selector: 'modal-tipo-analisis-batch',
    templateUrl: './modal-tipo-analisis-batch.html',
    providers: [TipoAnalisisBatchService]

})

export class ModalTipoAnalisisBatch implements OnInit {
  public tipoAnalisisBatch: TipoAnalisisBatch = new TipoAnalisisBatch();
  public titulo: string = "Crear Tipo de Análisis de Batch";
  public isSaving: boolean = false;

  constructor(
    private tipoAnalisisBatchService: TipoAnalisisBatchService,    
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.tipoAnalisisBatchService.getTipoAnalisisBatch(id).subscribe(tipoAnalisisBatch => (this.tipoAnalisisBatch = tipoAnalisisBatch));
      }
    });
  }

  save(): void {
    this.isSaving = true;
    if (typeof this.tipoAnalisisBatch.id === "undefined" || this.tipoAnalisisBatch.id === null) {
      this.tipoAnalisisBatchService.create(this.tipoAnalisisBatch).subscribe(tipoAnalisisBatch => {
        this.close();
        //swal('Nuevo TipoAnalisisBatch', `TipoAnalisisBatch ${tipoAnalisisBatch.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.tipoAnalisisBatchService.update(this.tipoAnalisisBatch).subscribe(tipoAnalisisBatch => {
        this.close();
        //swal('TipoAnalisisBatch Actualizado', `TipoAnalisisBatch ${tipoAnalisisBatch.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["herramientas/tipos-analisis-batchs"]);
  }

}
