import { Component, OnInit } from "@angular/core";
import { TipoAnalisisBatch } from "./tipo-analisis-batch";
import { TipoAnalisisBatchService } from "./tipo-analisis-batch.service";
import { ActivatedRoute, Params, Router, RouterModule } from "@angular/router";
import { NgbModal, ModalDismissReasons, NgbModule } from "@ng-bootstrap/ng-bootstrap";


@Component({
  selector: "app-tipo-analisis-batch",
  templateUrl: "./tipo-analisis-batch.component.html",
  styleUrls: ["./tipo-analisis-batch.component.css"],
  providers: [TipoAnalisisBatchService]
})
export class TipoAnalisisBatchComponent implements OnInit {
  public titulo: string;
  tipoAnalisisBatchs: TipoAnalisisBatch[];
  p:number;

  constructor(
    private tipoAnalisisBatchService: TipoAnalisisBatchService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) {}

  ngOnInit() {
    this.tipoAnalisisBatchService
      .getTipoAnalisisBatchs()
      .subscribe(
        tipoAnalisisBatchs => (this.tipoAnalisisBatchs = tipoAnalisisBatchs)
      );
  }
}
