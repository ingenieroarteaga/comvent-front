import { AnalisisBatchDetalle } from "../../model/analisis-batch-detalle";

export class TipoAnalisisBatch {
    public id: number;
    public descripcion: string;
    public unidadMedida: string;
    public analisisBatchDetalleList: AnalisisBatchDetalle[];
}
