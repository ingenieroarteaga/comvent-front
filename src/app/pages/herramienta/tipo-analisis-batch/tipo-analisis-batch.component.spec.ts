import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoAnalisisBatchComponent } from './tipo-analisis-batch.component';

describe('TipoAnalisisBatchComponent', () => {
  let component: TipoAnalisisBatchComponent;
  let fixture: ComponentFixture<TipoAnalisisBatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoAnalisisBatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoAnalisisBatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
