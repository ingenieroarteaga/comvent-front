import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BandejaService } from './bandeja.service';
import { Bandeja } from './bandeja';
import { FormControl } from "@angular/forms";

@Component({
  selector: 'app-bandeja',
  templateUrl: './bandeja.component.html',
  styleUrls: ['./bandeja.component.css'],
  providers: [BandejaService]
})
export class BandejaComponent implements OnInit {
  public titulo: String;
  public bandejas: Bandeja[];
  myBandeja: FormControl = new FormControl();
  p : number;

  constructor(
    private bandejaService: BandejaService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.bandejaService.getBandejas().subscribe(
      bandejas => this.bandejas = bandejas
    );
  }
}
