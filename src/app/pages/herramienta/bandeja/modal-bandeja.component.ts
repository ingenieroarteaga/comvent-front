import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Bandeja } from "./bandeja";
import { BandejaService } from "./bandeja.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
import { TipoBandejaService } from "../tipo-bandeja/tipo-bandeja.service";
import { ProveedorBandejaService } from "../../personas/proveedor-bandeja/proveedor-bandeja.service";
import { TipoBandeja } from "../tipo-bandeja/tipo-bandeja";
import { ProveedorBandeja } from "../../personas/proveedor-bandeja/proveedor-bandeja";
@Component({
  selector: "modal-bandeja",
  templateUrl: "./modal-bandeja.html",
  providers: [BandejaService, TipoBandejaService, ProveedorBandejaService]
})
export class ModalBandeja implements OnInit {
  public bandeja: Bandeja = new Bandeja();
  public titulo: string = "Crear Bandeja";
  public isSaving: boolean = false;
  //
  public tipoBandejas: TipoBandeja[];
  public proveedorBandejas: ProveedorBandeja[];

  constructor(
    private bandejaService: BandejaService,
    private tipoBandejaService: TipoBandejaService,
    private proveedorBandejaService: ProveedorBandejaService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.tipoBandejaService.getTipoBandejas().subscribe(
      tipoBandejas => this.tipoBandejas = tipoBandejas);
    this.proveedorBandejaService.getProveedorBandejas().subscribe(
      proveedorBandejas => this.proveedorBandejas = proveedorBandejas);
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.bandejaService.getBandeja(id).subscribe(bandeja => (this.bandeja = bandeja));
      }
    });
  }

  compareTipoBandeja(t1: TipoBandeja, t2: TipoBandeja): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  compareProveedorBandeja(t1: TipoBandeja, t2: TipoBandeja): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }


  save(): void {
    this.isSaving = true;
    if (typeof this.bandeja.id === "undefined" || this.bandeja.id === null) {
      this.bandejaService.create(this.bandeja).subscribe(bandeja => {
        this.close();
        //swal('Nuevo Bandeja', `Bandeja ${bandeja.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.bandejaService.update(this.bandeja).subscribe(bandeja => {
        this.close();
        //swal('Bandeja Actualizado', `Bandeja ${bandeja.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["herramientas/bandejas"]);
  }

}
