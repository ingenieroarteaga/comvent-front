import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Bandeja } from './bandeja';


@Injectable()
export class BandejaService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getBandejas(): Observable<Bandeja[]> {
    return this.http.get(this.url + 'bandejas').pipe(
      map(response => response as Bandeja[])
    );
  }

  getBandeja(id: number): Observable<Bandeja> {
    return this.http.get<Bandeja>(`${this.url + 'bandeja'}/${id}`)
  }

  create(bandeja: Bandeja): Observable<Bandeja> {
    return this.http.post<Bandeja>(this.url + 'bandeja', bandeja, { headers: this.httpHeaders })
  }

  update(bandeja: Bandeja): Observable<Bandeja> {
    return this.http.put<Bandeja>(this.url + 'bandeja', bandeja, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Bandeja> {
    return this.http.delete<Bandeja>(`${this.url + 'bandeja'}/${id}`, { headers: this.httpHeaders })
  }

}