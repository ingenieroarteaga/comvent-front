import { TipoBandeja } from "../tipo-bandeja/tipo-bandeja";
import { ProveedorBandeja } from "../../personas/proveedor-bandeja/proveedor-bandeja";

export class Bandeja {
    public id: number;
    public descripcion: string;
    public capacidad: number;
    public periodo: number;
    public tipoBandeja: TipoBandeja;
    public proveedorBandeja: ProveedorBandeja;

 
}
