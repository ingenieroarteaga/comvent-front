import { Especie } from "../especie/especie";
import { PedidoDetalle } from "../../model/pedido-detalle/pedido-detalle";
import { Proveedor } from "../../personas/proveedor/proveedor";
import { ListaPrecio } from "../../administracion/lista-precio/lista-precio";
import { Lote } from "../../model/lote";

export class Variedad {
    public id: number;
    public descripcion: string;
    public mercado: string;
    public clasificacion: string;
    public loteList: Lote[];
    public listaPrecioList: ListaPrecio[];
    public especie: Especie;
    public proveedor: Proveedor;
    public pedidoDetalleList: PedidoDetalle[];
 
}
