import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Variedad } from './variedad';


@Injectable()
export class VariedadService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getVariedades(): Observable<Variedad[]> {
    return this.http.get(this.url + 'variedades').pipe(
      map(response => response as Variedad[])
    );
  }

  getVariedad(id: number): Observable<Variedad> {
    return this.http.get<Variedad>(`${this.url + 'variedad'}/${id}`)
  }

  create(variedad: Variedad): Observable<Variedad> {
    return this.http.post<Variedad>(this.url + 'variedad', variedad, { headers: this.httpHeaders })
  }

  update(variedad: Variedad): Observable<Variedad> {
    return this.http.put<Variedad>(this.url + 'variedad', variedad, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Variedad> {
    return this.http.delete<Variedad>(`${this.url + 'variedad'}/${id}`, { headers: this.httpHeaders })
  }

}