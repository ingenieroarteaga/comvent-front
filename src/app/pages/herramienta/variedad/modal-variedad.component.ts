import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Variedad } from "./variedad";
import { VariedadService } from "./variedad.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
import { EspecieService } from "../especie/especie.service";
import { ProveedorService } from "../../personas/proveedor/proveedor.service";
import { Especie } from "../especie/especie";
import { Proveedor } from "../../personas/proveedor/proveedor";
//
import { Observable } from "rxjs";
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { FormControl } from "@angular/forms";

@Component({
  selector: "modal-variedad",
  templateUrl: "./modal-variedad.html",
  providers: [VariedadService, EspecieService, ProveedorService]
})
export class ModalVariedad implements OnInit {
  public variedad: Variedad = new Variedad();
  public titulo: string = "Crear Variedad";
  public isSaving: boolean = false;
  public especies: Especie[];
  public proveedores: Proveedor[];

  ////
  filteredEspecies: Observable<any[]>;
  myControl: FormControl = new FormControl();
  valStr:string;

  constructor(
    private variedadService: VariedadService,
    private especieService: EspecieService,
    private proveedorService: ProveedorService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.especieService.getEspecies().subscribe(especies => (this.especies = especies));
    this.proveedorService.getProveedores().subscribe(proveedores => (this.proveedores = proveedores));
    /////// Autocomplete ///////////////////////////////
    this.autoEspecie();
    /////// fin autocomplete /////////
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.variedadService.getVariedad(id).subscribe(variedad => (this.variedad = variedad));
      }
    });
  }

  compareProveedor(t1: Proveedor, t2: Proveedor): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  save(): void {
    this.isSaving = true;
    if (typeof this.variedad.id === "undefined" || this.variedad.id === null) {
      this.variedadService.create(this.variedad).subscribe(variedad => {
        this.close();
        //swal('Nuevo Variedad', `Variedad ${variedad.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.variedadService.update(this.variedad).subscribe(variedad => {
        this.close();
        //swal('Variedad Actualizado', `Variedad ${variedad.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["herramientas/variedades"]);
  }

  ///////// autocomplete /////////////////
  autoEspecie(){
          
    this.filteredEspecies = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filter(val))
      );
  }
  filter(val) {
    if(typeof val.descripcion === 'undefined'){
      this.valStr = val;
    }else{
      this.valStr = val.descripcion;
    }
    if (this.especies) {
      return this.especies.filter(especies => especies.descripcion.toLowerCase().includes(this.valStr.toLowerCase()));
    }
  }

  public getDisplayFn() {
    return (val) => this.display(val);
 }
  private display(esp): string {
    //access component "this" here
    return esp ? esp.descripcion : esp;
 }

  private selected(esp){
    console.log(esp);
  }

  /////////Fin autocomplete //////////////////////////////////////

}
