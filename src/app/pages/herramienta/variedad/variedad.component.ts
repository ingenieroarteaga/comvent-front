import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { VariedadService } from './variedad.service';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Variedad } from './variedad';
import { FormControl } from "@angular/forms";

@Component({
  selector: 'app-variedad',
  templateUrl: './variedad.component.html',
  styleUrls: ['./variedad.component.css'],
  providers: [VariedadService]
})
export class VariedadComponent implements OnInit {
  public titulo: String;
  public variedades: Variedad[];
   myVariedad: FormControl = new FormControl();
   p : number;

  constructor(
    private variedadService: VariedadService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.variedadService.getVariedades().subscribe(
      variedades => this.variedades = variedades
    );
  }

}
