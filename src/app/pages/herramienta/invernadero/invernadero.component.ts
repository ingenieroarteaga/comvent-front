import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { InvernaderoService } from './invernadero.service';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Invernadero } from './invernadero';
import { FormControl } from "@angular/forms";

@Component({
  selector: 'app-invernadero',
  templateUrl: './invernadero.component.html',
  styleUrls: ['./invernadero.component.css'],
  providers: [InvernaderoService]
})
export class InvernaderoComponent implements OnInit {
  public titulo: String;
  public invernaderos: Invernadero[];
  myInvernadero: FormControl = new FormControl();
  p:number;

  constructor(
    private invernaderoService: InvernaderoService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.invernaderoService.getInvernaderos().subscribe(
      invernaderos => this.invernaderos = invernaderos
    );
  }

}
