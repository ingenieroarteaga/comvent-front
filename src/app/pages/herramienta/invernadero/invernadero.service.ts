import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Invernadero } from './invernadero';


@Injectable()
export class InvernaderoService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getInvernaderos(): Observable<Invernadero[]> {
    return this.http.get(this.url + 'invernaderos').pipe(
      map(response => response as Invernadero[])
    );
  }

  getInvernadero(id: number): Observable<Invernadero> {
    return this.http.get<Invernadero>(`${this.url + 'invernadero'}/${id}`)
  }

  create(invernadero: Invernadero): Observable<Invernadero> {
    return this.http.post<Invernadero>(this.url + 'invernadero', invernadero, { headers: this.httpHeaders })
  }

  update(invernadero: Invernadero): Observable<Invernadero> {
    return this.http.put<Invernadero>(this.url + 'invernadero', invernadero, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Invernadero> {
    return this.http.delete<Invernadero>(`${this.url + 'invernadero'}/${id}`, { headers: this.httpHeaders })
  }

}