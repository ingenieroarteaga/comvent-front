import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Invernadero } from "./invernadero";
import { InvernaderoService } from "./invernadero.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
import { SucursalService } from "../sucursal/sucursal.service";
import { Sucursal } from "../sucursal/sucursal";
@Component({
  selector: "modal-invernadero",
  templateUrl: "./modal-invernadero.html",
  providers: [InvernaderoService, SucursalService]
})
export class ModalInvernadero implements OnInit {
  public invernadero: Invernadero = new Invernadero();
  public titulo: string = "Crear Invernadero";
  public isSaving: boolean = false;
  public sucursales: Sucursal[];

  habilitado: number;

  constructor(
    private invernaderoService: InvernaderoService,
    private sucursalService: SucursalService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.sucursalService.getSucursales().subscribe(sucursales => (this.sucursales = sucursales));
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.invernaderoService.getInvernadero(id).subscribe(invernadero => (this.invernadero = invernadero));
      }
    });
  }

  compareSucursal(t1: Sucursal, t2: Sucursal): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  save(): void {
    this.isSaving = true;
    this.invernadero.habilitado = this.habilitado;
    if (typeof this.invernadero.id === "undefined" || this.invernadero.id === null) {
      this.invernaderoService.create(this.invernadero).subscribe(invernadero => {
        this.close();
        //swal('Nuevo Invernadero', `Invernadero ${invernadero.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.invernaderoService.update(this.invernadero).subscribe(invernadero => {
        this.close();
        //swal('Invernadero Actualizado', `Invernadero ${invernadero.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["herramientas/invernaderos"]);
  }

  ////////////////////////////////////

  checkHabilitado(){
    if(!this.invernadero.habilitado){
      this.habilitado = 1;
    }else{
      this.habilitado = 0;
    }
  }

}
