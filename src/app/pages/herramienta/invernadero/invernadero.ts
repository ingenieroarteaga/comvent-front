import { Sucursal } from "../sucursal/sucursal";
import { Siembra } from "../../produccion/siembra/siembra";

export class Invernadero {
    public id: number;
    public descripcion: string;
    public capacidad: number;
    public habilitado: number;
    public observacion: string;
    public sucursal: Sucursal;
    public siembraInvernaderoList: Siembra[];
 
}
