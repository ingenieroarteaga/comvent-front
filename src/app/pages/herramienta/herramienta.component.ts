import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, RouterModule, Router} from '@angular/router';

@Component({
  selector: 'app-herramientas',
  templateUrl: './herramienta.component.html',
  styleUrls: ['./herramienta.component.css']
})
export class HerramientaComponent implements OnInit {
 bodyClasses = 'skin-green sidebar-mini';
  body: HTMLBodyElement = document.getElementsByTagName('body')[0];
  constructor(
    private _route:ActivatedRoute,
    private _router:Router
  ) { }

  ngOnInit() {
    this.body.classList.add('skin-green');
    this.body.classList.add('sidebar-mini');
  }
}
