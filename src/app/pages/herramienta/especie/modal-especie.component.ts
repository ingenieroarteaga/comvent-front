import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Especie } from "./especie";
import { EspecieService } from "./especie.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
@Component({
  selector: "modal-especie",
  templateUrl: "./modal-especie.html",
  providers: [EspecieService]
})
export class ModalEspecie implements OnInit {
  public especie: Especie = new Especie();
  public titulo: string = "Crear Especie";
  public isSaving: boolean = false;

  constructor(
    private especieService: EspecieService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.especieService.getEspecie(id).subscribe(especie => (this.especie = especie));
      }
    });
  }

  save(): void {
    this.isSaving = true;
    if (typeof this.especie.id === "undefined" || this.especie.id === null) {
      this.especieService.create(this.especie).subscribe(especie => {
        this.close();
        //swal('Nuevo Especie', `Especie ${especie.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.especieService.update(this.especie).subscribe(especie => {
        this.close();
        //swal('Especie Actualizado', `Especie ${especie.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["herramientas/especies"]);
  }

}
