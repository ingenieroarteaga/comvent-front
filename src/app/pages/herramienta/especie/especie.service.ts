import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Especie } from './especie';


@Injectable()
export class EspecieService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getEspecies(): Observable<Especie[]> {
    return this.http.get(this.url + 'especies').pipe(
      map(response => response as Especie[])
    );
  }

  getEspecie(id: number): Observable<Especie> {
    return this.http.get<Especie>(`${this.url + 'especie'}/${id}`)
  }

  create(especie: Especie): Observable<Especie> {
    return this.http.post<Especie>(this.url + 'especie', especie, { headers: this.httpHeaders })
  }

  update(especie: Especie): Observable<Especie> {
    return this.http.put<Especie>(this.url + 'especie', especie, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Especie> {
    return this.http.delete<Especie>(`${this.url + 'especie'}/${id}`, { headers: this.httpHeaders })
  }

}