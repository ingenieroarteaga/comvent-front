import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { EspecieService } from './especie.service';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Especie } from './especie';
import { FormControl } from "@angular/forms";


@Component({
  selector: 'app-especie',
  templateUrl: './especie.component.html',
  styleUrls: ['./especie.component.css'],
  providers: [EspecieService]
})
export class EspecieComponent implements OnInit {
  public titulo: String;
  public especies: Especie[];
  myEspecie: FormControl = new FormControl();
  p : number;

  constructor(
    private especieService: EspecieService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.especieService.getEspecies().subscribe(
      especies => this.especies = especies
    );
  }
}
