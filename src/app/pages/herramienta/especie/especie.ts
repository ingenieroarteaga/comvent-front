import { Variedad } from "../variedad/variedad";

export class Especie {
    public id: number;
    public descripcion: string;
    public variedadList: Variedad[];
 
}
