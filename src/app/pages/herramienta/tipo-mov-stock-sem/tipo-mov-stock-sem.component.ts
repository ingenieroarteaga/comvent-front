import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router, RouterModule } from "@angular/router";
import { NgbModal, ModalDismissReasons, NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { TipoMovStockSemService } from "./tipo-mov-stock-sem.service";
import { TipoMovStockSem } from "./tipo-mov-stock-sem";


@Component({
  selector: "app-tipo-mov-stock-sem",
  templateUrl: "./tipo-mov-stock-sem.component.html",
  styleUrls: ["./tipo-mov-stock-sem.component.css"],
  providers: [TipoMovStockSemService]
})
export class TipoMovStockSemComponent implements OnInit {
  public titulo: string;
  tipoMovStockSems: TipoMovStockSem[];
  p:number;

  constructor(
    private tipoMovStockSemService: TipoMovStockSemService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) {}

  ngOnInit() {
    this.tipoMovStockSemService
      .getTipoMovStockSems()
      .subscribe(
        tipoMovStockSems => (this.tipoMovStockSems = tipoMovStockSems)
      );
  }
}
