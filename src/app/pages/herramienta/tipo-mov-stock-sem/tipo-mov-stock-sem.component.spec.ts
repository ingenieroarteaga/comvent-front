import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TipoMovStockSemComponent } from './tipo-mov-stock-sem.component';

describe('TipoMovStockSemComponent', () => {
  let component: TipoMovStockSemComponent;
  let fixture: ComponentFixture<TipoMovStockSemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoMovStockSemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoMovStockSemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
