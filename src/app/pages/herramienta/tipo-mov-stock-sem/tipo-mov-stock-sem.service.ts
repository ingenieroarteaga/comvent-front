import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { TipoMovStockSem } from './tipo-mov-stock-sem';

@Injectable()
export class TipoMovStockSemService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getTipoMovStockSems(): Observable<TipoMovStockSem[]> {
    return this.http.get(this.url + 'tipos-movs-stocks-sem').pipe(
      map(response => response as TipoMovStockSem[])
    );
  }

  getTipoMovStockSem(id: number): Observable<TipoMovStockSem> {
    return this.http.get<TipoMovStockSem>(`${this.url + 'tipo-mov-stock-sem'}/${id}`)
  }

  create(tipoMovStockSem: TipoMovStockSem): Observable<TipoMovStockSem> {
    return this.http.post<TipoMovStockSem>(this.url + 'tipo-mov-stock-sem', tipoMovStockSem, { headers: this.httpHeaders })
  }

  update(tipoMovStockSem: TipoMovStockSem): Observable<TipoMovStockSem> {
    return this.http.put<TipoMovStockSem>(this.url + 'tipo-mov-stock-sem', tipoMovStockSem, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<TipoMovStockSem> {
    return this.http.delete<TipoMovStockSem>(`${this.url + 'tipo-mov-stock-sem'}/${id}`, { headers: this.httpHeaders })
  }
}
