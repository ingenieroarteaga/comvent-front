
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import swal from 'sweetalert2'
import { TipoMovStockSem } from './tipo-mov-stock-sem';
import { TipoMovStockSemService } from './tipo-mov-stock-sem.service';


@Component({
    selector: 'modal-tipo-mov-stock-sem',
    templateUrl: './modal-tipo-mov-stock-sem.html',
    providers: [TipoMovStockSemService]

})

export class ModalTipoMovStockSem implements OnInit {
  public tipoMovStockSem: TipoMovStockSem = new TipoMovStockSem();
  public titulo: string = "Crear Tipo Mov. Stock Semilla";
  public isSaving: boolean = false;

  constructor(
    private tipoMovStockSemService: TipoMovStockSemService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.tipoMovStockSemService.getTipoMovStockSem(id).subscribe(tipoMovStockSem => (this.tipoMovStockSem = tipoMovStockSem));
      }
    });
  }

  save(): void {
    this.isSaving = true;
    if (typeof this.tipoMovStockSem.id === "undefined" || this.tipoMovStockSem.id === null) {
      this.tipoMovStockSemService.create(this.tipoMovStockSem).subscribe(tipoMovStockSem => {
        this.close();
        //swal('Nuevo TipoMovStockSem', `TipoMovStockSem ${tipoMovStockSem.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.tipoMovStockSemService.update(this.tipoMovStockSem).subscribe(tipoMovStockSem => {
        this.close();
        //swal('TipoMovStockSem Actualizado', `TipoMovStockSem ${tipoMovStockSem.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["herramientas/tipos-movs-stocks-sem"]);
  }

}
