import { Domicilio } from "../../model/domicilio";

export class Entidad {
    public id: number;
    public cuit: number;
    public nombre: string;
    public inicioAct: Date;
    public ingBrutos: string;
    public telefono: string;
    public mail: string;
    public fax: string;
    public domicilio: Domicilio;
    
 
}
