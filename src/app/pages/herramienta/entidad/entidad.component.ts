import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { EntidadService } from './entidad.service';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Entidad } from './entidad';
import { FormControl } from "@angular/forms";


@Component({
  selector: 'app-entidad',
  templateUrl: './entidad.component.html',
  styleUrls: ['./entidad.component.css'],
  providers: [EntidadService]
})
export class EntidadComponent implements OnInit {

  public titulo: String;
  public entidades: Entidad[];
  myEntidad: FormControl = new FormControl();
  p:number;

  constructor(
    private entidadService: EntidadService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.entidadService.getEntidades().subscribe(
      entidades => this.entidades = entidades
    );
  }
}
