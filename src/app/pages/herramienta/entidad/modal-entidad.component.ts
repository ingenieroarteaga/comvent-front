import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Entidad } from "./entidad";
import { EntidadService } from "./entidad.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
import { DistritoService } from "../../clasificadores/distrito/distrito.service";
import { Distrito } from "../../clasificadores/distrito/distrito";
import { Domicilio } from "../../model/domicilio";
import { Observable } from "rxjs";
import { FormControl } from "@angular/forms";
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { TipoDomicilioService } from "../../clasificadores/tipo-domicilio/tipo-domicilio.service";
import { TipoDomicilio } from "../../clasificadores/tipo-domicilio/tipo-domicilio";

@Component({
  selector: "modal-entidad",
  templateUrl: "./modal-entidad.html",
  providers: [EntidadService, DistritoService, TipoDomicilioService]
})
export class ModalEntidad implements OnInit {
  public entidad: Entidad = new Entidad();
  public domicilio: Domicilio = new Domicilio();
  public titulo: string = "Crear Entidad";
  public isSaving: boolean = false;
  public distritos: Distrito[];
  public tipoDomicilios: TipoDomicilio[];
  
  filteredDistritos: Observable<any[]>;
  myControl: FormControl = new FormControl();
  valStr:string;

  constructor(
    private entidadService: EntidadService,
    private distritoService: DistritoService,
    private tipoDomicilioService: TipoDomicilioService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.distritoService.getDistritos().subscribe(distritos => (this.distritos = distritos));
    this.tipoDomicilioService.getTipoDomicilios().subscribe(tipoDomicilios => (this.tipoDomicilios = tipoDomicilios));
    /////// Autocomplete ///////////////////////////////
    this.autoDistrito();
    /////// fin autocomplete /////////
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.entidadService.getEntidad(id).subscribe(entidad => (this.entidad = entidad, this.domicilio = this.entidad.domicilio));
      }
    });
  }

  compareTipoDomicilio(t1: TipoDomicilio, t2: TipoDomicilio): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  save(): void {
    this.entidad.domicilio = this.domicilio;
    this.isSaving = true;
    if (typeof this.entidad.id === "undefined" || this.entidad.id === null) {
      this.entidadService.create(this.entidad).subscribe(entidad => {
        this.close();
        //swal('Nuevo Entidad', `Entidad ${entidad.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.entidadService.update(this.entidad).subscribe(entidad => {
        this.close();
        //swal('Entidad Actualizado', `Entidad ${entidad.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["herramientas/entidades"]);
  }

  ///////// autocomplete /////////////////
  autoDistrito(){
          
    this.filteredDistritos = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filter(val))
      );
  }
  filter(val) {
    if(typeof val.descripcion === 'undefined'){
      this.valStr = val;
    }else{
      this.valStr = val.descripcion;
    }
    if (this.distritos) {
      return this.distritos.filter(distritos =>
        distritos.descripcion.toLowerCase().includes(this.valStr.toLowerCase())
        || distritos.localidad.descripcion.toLowerCase().includes(this.valStr.toLowerCase())
        || distritos.localidad.provincia.descripcion.toLowerCase().includes(this.valStr.toLowerCase())
      );
    }
  }

  public getDisplayFn() {
    return (val) => this.display(val);
 }
  private display(dis): string {
    //access component "this" here
    return dis ? dis.descripcion : dis;
 }

  private selected(dis){
    console.log(dis);
  }

  /////////Fin autocomplete //////////////////////////////////////

}
