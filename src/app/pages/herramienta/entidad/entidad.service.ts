import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Entidad } from './entidad';


@Injectable()
export class EntidadService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getEntidades(): Observable<Entidad[]> {
    return this.http.get(this.url + 'entidades').pipe(
      map(response => response as Entidad[])
    );
  }

  getEntidad(id: number): Observable<Entidad> {
    return this.http.get<Entidad>(`${this.url + 'entidad'}/${id}`)
  }

  create(entidad: Entidad): Observable<Entidad> {
    return this.http.post<Entidad>(this.url + 'entidad', entidad, { headers: this.httpHeaders })
  }

  update(entidad: Entidad): Observable<Entidad> {
    return this.http.put<Entidad>(this.url + 'entidad', entidad, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Entidad> {
    return this.http.delete<Entidad>(`${this.url + 'entidad'}/${id}`, { headers: this.httpHeaders })
  }

}