import { Invernadero } from "../invernadero/invernadero";
import { Entidad } from "../entidad/entidad";
import { Domicilio } from "../../model/domicilio";
import { BandejaStock } from "../../model/bandeja-stock/bandeja-stock";
import { Pedido } from "../../produccion/pedido/pedido";
import { Siembra } from "../../produccion/siembra/siembra";
import { StockSemilla } from "../../produccion/stock-semilla/stock-semilla";


export class Sucursal {
    public id: number;
    public descripcion: string;
    public mail: string;
    public telefono: string;
    public stockSemillaList: StockSemilla[];
    public siembraList: Siembra[];
    public pedidoList: Pedido[];
    public bandejaStockList: BandejaStock[];
    public invernaderoList: Invernadero[];
    public domicilio: Domicilio;
    public entidad: Entidad;
 
}
