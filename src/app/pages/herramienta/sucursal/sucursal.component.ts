import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SucursalService } from './sucursal.service';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Sucursal } from './sucursal';
import { FormControl } from "@angular/forms";


@Component({
  selector: 'app-sucursal',
  templateUrl: './sucursal.component.html',
  styleUrls: ['./sucursal.component.css'],
  providers: [SucursalService]
})
export class SucursalComponent implements OnInit {
  public titulo: String;
  public sucursales: Sucursal[];
  mySucursal: FormControl = new FormControl();
  p:number;

  constructor(
    private sucursalService: SucursalService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.sucursalService.getSucursales().subscribe(
      sucursales => this.sucursales = sucursales
    );
  }
}
