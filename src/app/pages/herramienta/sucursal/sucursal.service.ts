import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Sucursal } from './sucursal';


@Injectable()
export class SucursalService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getSucursales(): Observable<Sucursal[]> {
    return this.http.get(this.url + 'sucursales').pipe(
      map(response => response as Sucursal[])
    );
  }

  getSucursal(id: number): Observable<Sucursal> {
    return this.http.get<Sucursal>(`${this.url + 'sucursal'}/${id}`)
  }

  create(sucursal: Sucursal): Observable<Sucursal> {
    return this.http.post<Sucursal>(this.url + 'sucursal', sucursal, { headers: this.httpHeaders })
  }

  update(sucursal: Sucursal): Observable<Sucursal> {
    return this.http.put<Sucursal>(this.url + 'sucursal', sucursal, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Sucursal> {
    return this.http.delete<Sucursal>(`${this.url + 'sucursal'}/${id}`, { headers: this.httpHeaders })
  }

}