import { Component, OnInit } from "@angular/core";
import { Sucursal } from "./sucursal";
import { SucursalService } from "./sucursal.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { EntidadService } from "../entidad/entidad.service";
import { Entidad } from "../entidad/entidad";
import { Distrito } from "../../clasificadores/distrito/distrito";
import { DistritoService } from "../../clasificadores/distrito/distrito.service";
import { Observable } from "rxjs";
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { FormControl } from "@angular/forms";
import { TipoDomicilioService } from "../../clasificadores/tipo-domicilio/tipo-domicilio.service";
import { TipoDomicilio } from "../../clasificadores/tipo-domicilio/tipo-domicilio";
import { Domicilio } from "../../model/domicilio";

@Component({
  selector: "modal-sucursal",
  templateUrl: "./modal-sucursal.html",
  providers: [SucursalService, EntidadService, DistritoService, TipoDomicilioService]
})
export class ModalSucursal implements OnInit {
  public sucursal: Sucursal = new Sucursal();
  public titulo: string = "Crear Sucursal";
  public isSaving: boolean = false;
  public entidades: Entidad[];
  public distritos: Distrito[];
  public tipoDomicilios: TipoDomicilio[];
  public domicilio: Domicilio = new Domicilio();

  filteredDistritos: Observable<any[]>;
  myControl: FormControl = new FormControl();
  valStr:string;

  constructor(
    private sucursalService: SucursalService,
    private entidadService: EntidadService,
    private distritoService: DistritoService,
    private tipoDomicilioService: TipoDomicilioService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.distritoService.getDistritos().subscribe(distritos => (this.distritos = distritos));
    this.entidadService.getEntidades().subscribe(entidades => (this.entidades = entidades));
    this.tipoDomicilioService.getTipoDomicilios().subscribe(tipoDomicilios => (this.tipoDomicilios = tipoDomicilios));
    /////// Autocomplete ///////////////////////////////
    this.autoDistrito();
    /////// fin autocomplete /////////
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.sucursalService.getSucursal(id).subscribe(sucursal => (this.sucursal = sucursal, this.domicilio = this.sucursal.domicilio));
      }
    });
  }

  compareEntidad(t1: Entidad, t2: Entidad): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  compareTipoDomicilio(t1: TipoDomicilio, t2: TipoDomicilio): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  save(): void {
    this.isSaving = true;
    this.sucursal.domicilio = this.domicilio;
    if (typeof this.sucursal.id === "undefined" || this.sucursal.id === null) {
      this.sucursalService.create(this.sucursal).subscribe(sucursal => {
        this.close();
        //swal('Nuevo Sucursal', `Sucursal ${sucursal.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.sucursalService.update(this.sucursal).subscribe(sucursal => {
        this.close();
        //swal('Sucursal Actualizado', `Sucursal ${sucursal.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["herramientas/sucursales"]);
  }

  ///////// autocomplete /////////////////
  autoDistrito(){
          
    this.filteredDistritos = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filter(val))
      );
  }
  filter(val) {
    if(typeof val.descripcion === 'undefined'){
      this.valStr = val;
    }else{
      this.valStr = val.descripcion;
    }
    if (this.distritos) {
      return this.distritos.filter(distritos =>
        distritos.descripcion.toLowerCase().includes(this.valStr.toLowerCase())
        || distritos.localidad.descripcion.toLowerCase().includes(this.valStr.toLowerCase())
        || distritos.localidad.provincia.descripcion.toLowerCase().includes(this.valStr.toLowerCase())
      );
    }
  }

  public getDisplayFn() {
    return (val) => this.display(val);
 }
  private display(dis): string {
    //access component "this" here
    return dis ? dis.descripcion : dis;
 }

  private selected(dis){
    console.log(dis);
  }

  /////////Fin autocomplete //////////////////////////////////////


}
