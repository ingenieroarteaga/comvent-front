import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoBandejaComponent } from './tipo-bandeja.component';

describe('TipoBandejaComponent', () => {
  let component: TipoBandejaComponent;
  let fixture: ComponentFixture<TipoBandejaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoBandejaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoBandejaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
