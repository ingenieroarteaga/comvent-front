import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { TipoBandejaService } from './tipo-bandeja.service';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TipoBandeja } from './tipo-bandeja';
import { FormControl } from "@angular/forms";

@Component({
  selector: 'app-tipo-bandeja',
  templateUrl: './tipo-bandeja.component.html',
  styleUrls: ['./tipo-bandeja.component.css'],
  providers: [TipoBandejaService]
})
export class TipoBandejaComponent implements OnInit {
  public titulo: String;
  public tipoBandejas: TipoBandeja[];
  myTipoBandeja: FormControl = new FormControl();
  p:number;

  constructor(
    private tipoBandejaService: TipoBandejaService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.tipoBandejaService.getTipoBandejas().subscribe(
      tipoBandejas => this.tipoBandejas = tipoBandejas
    );
  }
}

