
import { GLOBAL } from '../../global';
import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { TipoBandeja } from './tipo-bandeja';
import { TipoBandejaService } from './tipo-bandeja.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import swal from 'sweetalert2'
@Component({
    selector: 'modal-tipo-bandeja',
    templateUrl: './modal-tipo-bandeja.html',
    providers: [TipoBandejaService]

})

export class ModalTipoBandeja implements OnInit {
  public tipoBandeja: TipoBandeja = new TipoBandeja();
  public titulo: string = "Crear Tipo de Bandeja";
  public isSaving: boolean = false;

  constructor(
    private tipoBandejaService: TipoBandejaService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.tipoBandejaService.getTipoBandeja(id).subscribe(tipoBandeja => (this.tipoBandeja = tipoBandeja));
      }
    });
  }

  save(): void {
    this.isSaving = true;
    if (typeof this.tipoBandeja.id === "undefined" || this.tipoBandeja.id === null) {
      this.tipoBandejaService.create(this.tipoBandeja).subscribe(tipoBandeja => {
        this.close();
        //swal('Nuevo TipoBandeja', `TipoBandeja ${tipoBandeja.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.tipoBandejaService.update(this.tipoBandeja).subscribe(tipoBandeja => {
        this.close();
        //swal('TipoBandeja Actualizado', `TipoBandeja ${tipoBandeja.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["herramientas/tipos-bandejas"]);
  }

}
