import { Bandeja } from "../bandeja/bandeja";

export class TipoBandeja {
    public id: number;
    public descripcion: string;
    public bandejaList: Bandeja[];
 
}
