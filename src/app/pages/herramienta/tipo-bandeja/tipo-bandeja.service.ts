import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { TipoBandeja } from './tipo-bandeja';


@Injectable()
export class TipoBandejaService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getTipoBandejas(): Observable<TipoBandeja[]> {
    return this.http.get(this.url + 'tipos-bandejas').pipe(
      map(response => response as TipoBandeja[])
    );
  }

  getTipoBandeja(id: number): Observable<TipoBandeja> {
    return this.http.get<TipoBandeja>(`${this.url + 'tipo-bandeja'}/${id}`)
  }

  create(tipoBandeja: TipoBandeja): Observable<TipoBandeja> {
    return this.http.post<TipoBandeja>(this.url + 'tipo-bandeja', tipoBandeja, { headers: this.httpHeaders })
  }

  update(tipoBandeja: TipoBandeja): Observable<TipoBandeja> {
    return this.http.put<TipoBandeja>(this.url + 'tipo-bandeja', tipoBandeja, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<TipoBandeja> {
    return this.http.delete<TipoBandeja>(`${this.url + 'tipo-bandeja'}/${id}`, { headers: this.httpHeaders })
  }

}