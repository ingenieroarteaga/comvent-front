import { Siembra } from "../../produccion/siembra/siembra";
import { Batch } from "../../model/batch/batch";
import { Identificador } from "../../produccion/identificador/identificador";

export class Calendario {
    public id: number;
    public periodo: number;
    public desde: Date;
    public hasta: Date;
    public siembraList: Siembra[];
    public batchList: Batch[];
    public identificadorList: Identificador[];
    
}
