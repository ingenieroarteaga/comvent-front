import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Calendario } from './calendario';

@Injectable()
export class CalendarioService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor( private http: HttpClient ) { 
    this.url = GLOBAL.url;
  }

  getCalendarios(): Observable<Calendario[]> {
    return this.http.get(this.url + 'calendarios').pipe(
      map(response => response as Calendario[])
    );
  }

  getCalendario(id: number): Observable<Calendario> {
    return this.http.get<Calendario>(`${this.url + 'calendario'}/${id}`)
  }

  create(calendario: Calendario): Observable<Calendario> {
    return this.http.post<Calendario>(this.url + 'calendario', calendario, { headers: this.httpHeaders })
  }

  update(calendario: Calendario): Observable<Calendario> {
    return this.http.put<Calendario>(this.url + 'calendario', calendario, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Calendario> {
    return this.http.delete<Calendario>(`${this.url + 'calendario'}/${id}`, { headers: this.httpHeaders })
  }

}
