import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Calendario } from "./calendario";
import { CalendarioService } from "./calendario.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
@Component({
  selector: "modal-calendario",
  templateUrl: "./modal-calendario.html",
  providers: [CalendarioService]
})
export class ModalCalendario implements OnInit {
  public calendario: Calendario = new Calendario();
  public titulo: string = "Crear Calendario";
  public isSaving: boolean = false;

  constructor(
    private calendarioService: CalendarioService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.calendarioService.getCalendario(id).subscribe(calendario => (this.calendario = calendario));
      }
    });
  }

  save(): void {
    this.isSaving = true;
    if (typeof this.calendario.id === "undefined" || this.calendario.id === null) {
      this.calendarioService.create(this.calendario).subscribe(calendario => {
        this.close();
        //swal('Nuevo Calendario', `Calendario ${calendario.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.calendarioService.update(this.calendario).subscribe(calendario => {
        this.close();
        //swal('Calendario Actualizado', `Calendario ${calendario.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["herramientas/calendarios"]);
  }

}
