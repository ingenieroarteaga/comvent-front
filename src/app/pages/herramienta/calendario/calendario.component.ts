import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CalendarioService } from './calendario.service';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Calendario } from './calendario';
import { FormControl } from "@angular/forms";


@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.component.html',
  styleUrls: ['./calendario.component.css'],
  providers: [CalendarioService]
})
export class CalendarioComponent implements OnInit {
  public titulo: String;
  public calendarios: Calendario[];
  myCalendario: FormControl = new FormControl();
  p : number;


  constructor(
    private calendarioService: CalendarioService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.calendarioService.getCalendarios().subscribe(
      calendarios => this.calendarios = calendarios
    );
  }
}
