import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FincaService } from './finca.service';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Finca } from './finca';
import { FormControl } from "@angular/forms";

@Component({
  selector: 'app-finca',
  templateUrl: './finca.component.html',
  styleUrls: ['./finca.component.css'],
  providers: [FincaService]
})
export class FincaComponent implements OnInit {

  public titulo: String;
  public fincas: Finca[];
  myFinca: FormControl = new FormControl();
  p : number;

  constructor(
    private fincaService: FincaService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.fincaService.getFincas().subscribe(
      fincas => this.fincas = fincas
    );
  }

}
