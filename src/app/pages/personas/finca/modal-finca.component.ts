import { GLOBAL } from "../../global";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Finca } from "./finca";
import { FincaService } from "./finca.service";
import { DistritoService } from "../../clasificadores/distrito/distrito.service";
import { Distrito } from "../../clasificadores/distrito/distrito";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { Observable } from "rxjs/Observable";
import swal from "sweetalert2";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatButtonModule, MatCheckboxModule } from "@angular/material";
import { FormControl } from "@angular/forms";
import { MatInputModule } from "@angular/material";
import { startWith } from "rxjs/operators/startWith";
import { map } from "rxjs/operators/map";
import { TipoDomicilio } from "../../clasificadores/tipo-domicilio/tipo-domicilio";
import { TipoDomicilioService } from "../../clasificadores/tipo-domicilio/tipo-domicilio.service";
import { ClienteService } from "../cliente/cliente.service";
import { Cliente } from "../cliente/cliente";
import { Domicilio } from "../../model/domicilio";


@Component({
  selector: "modal-finca",
  templateUrl: "./modal-finca.html",
  providers: [
    FincaService,
    DistritoService,
    TipoDomicilioService,
    ClienteService
  ]
})
export class ModalFinca implements OnInit {
  public finca: Finca = new Finca();
  public domicilio: Domicilio = new Domicilio();
  public cliente: Cliente = new Cliente();
  public titulo: string = "Crear Finca";
  public isSaving: boolean = false;
  public distritos: Distrito[];
  public tipoDomicilios: TipoDomicilio[];
  public clientes: Cliente[];
  /// autocomplete distrito
  myControl: FormControl = new FormControl();
  filteredDistritos: Observable<any[]>;
  valStr: string;
  /// autocomplete cliente
  myControlCli: FormControl = new FormControl();
  filteredClientes: Observable<any[]>;
  valStrCli: string;

  constructor(
    private fincaService: FincaService,
    private distritoService: DistritoService,
    private tipoDomicilioService: TipoDomicilioService,
    private clienteService: ClienteService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.distritoService
      .getDistritos()
      .subscribe(distritos => (this.distritos = distritos));
    this.tipoDomicilioService
      .getTipoDomicilios()
      .subscribe(tipoDomicilios => (this.tipoDomicilios = tipoDomicilios));
    this.clienteService
      .getClientes()
      .subscribe(clientes => (this.clientes = clientes));
    /////// Autocomplete ///////////////////////////////
    this.autoDistrito();
    this.autoCliente();
    /////// fin autocomplete /////////
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.fincaService.getFinca(id).subscribe(finca => (this.finca = finca, this.cliente = finca.cliente, this.domicilio = finca.domicilio));
      }
    });
  }

  compareTipoDomicilio(t1: TipoDomicilio, t2: TipoDomicilio): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  save(): void {
    this.isSaving = true;
    this.finca.cliente = this.cliente;
    this.finca.domicilio = this.domicilio;
    if (typeof this.finca.id === "undefined" || this.finca.id === null) {
      this.fincaService.create(this.finca).subscribe(finca => {
        this.close();
        //swal('Nuevo Finca', `Finca ${finca.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.fincaService.update(this.finca).subscribe(finca => {
        this.close();
        //swal('Finca Actualizado', `Finca ${finca.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["personas/fincas"]);
  }

  ///////// autocomplete /////////////////
  autoDistrito() {
    this.filteredDistritos = this.myControl.valueChanges.pipe(
      startWith(""),
      map(val => this.filter(val))
    );
  }
  filter(val) {
    if (typeof val.descripcion === "undefined") {
      this.valStr = val;
    } else {
      this.valStr = val.descripcion;
    }
    if (this.distritos) {
      return this.distritos.filter(
        distritos =>
          distritos.descripcion
            .toLowerCase()
            .includes(this.valStr.toLowerCase()) ||
          distritos.localidad.descripcion
            .toLowerCase()
            .includes(this.valStr.toLowerCase()) ||
          distritos.localidad.provincia.descripcion
            .toLowerCase()
            .includes(this.valStr.toLowerCase())
      );
    }
  }

  public getDisplayFn() {
    return val => this.display(val);
  }
  private display(dis): string {
    //access component "this" here
    return dis ? dis.descripcion : dis;
  }

  private selected(dis) {
    console.log(dis);
  }

  /////////Fin autocomplete //////////////////////////////////////

  ///////// autocomplete Cliente /////////////////
  autoCliente() {
    this.filteredClientes = this.myControlCli.valueChanges.pipe(
      startWith(""),
      map(val => this.filterCli(val))
    );
  }
  filterCli(val) {
    this.valStrCli = val.toString();

    if (this.clientes) {
      return this.clientes.filter(
        clientes =>
          clientes.persona.nombre
            .toLowerCase()
            .includes(this.valStrCli.toLowerCase()) ||
          ( clientes.persona.apellido !== null && clientes.persona.apellido
            .toLowerCase().includes(this.valStrCli.toLowerCase()) 
          ) ||
          clientes.persona.cuit.toString()
            .includes(this.valStrCli.toLowerCase())
      );
    }
  }

  public getDisplayFnCli() {
    return val => this.displayCli(val);
  }
  private displayCli(cli): string {
    //access component "this" here
    if (cli === null || typeof cli.persona === "undefined") {
      return null;
    } else {
      return cli ? cli.persona.cuit + " - " + cli.persona.nombre + " " + cli.persona.apellido : cli;
    }
  }
  private selectedCli(cli) {
    console.log(cli);
  }

  /////////Fin autocomplete Clientes //////////////////////////////////////
}
