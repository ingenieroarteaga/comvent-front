import { Cliente } from "../cliente/cliente";
import { Domicilio } from "../../model/domicilio";
import { Remito } from "../../produccion/remito/remito";

export class Finca {
    public id: number;
    public descripcion: string;
    public telefono: string;
    public observaciones: string;
    public remitoList: Remito[];
    public cliente: Cliente;
    public domicilio: Domicilio;
 
}
