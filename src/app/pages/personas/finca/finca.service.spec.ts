import { TestBed, inject } from '@angular/core/testing';

import { FincaService } from './finca.service';

describe('FincaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FincaService]
    });
  });

  it('should be created', inject([FincaService], (service: FincaService) => {
    expect(service).toBeTruthy();
  }));
});
