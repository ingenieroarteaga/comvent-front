import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Finca } from './finca';

@Injectable()
export class FincaService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getFincas(): Observable<Finca[]> {
    return this.http.get(this.url + 'fincas').pipe(
      map(response => response as Finca[])
    );
  }

  getFinca(id: number): Observable<Finca> {
    return this.http.get<Finca>(`${this.url + 'finca'}/${id}`)
  }

  create(finca: Finca): Observable<Finca> {
    return this.http.post<Finca>(this.url + 'finca', finca, { headers: this.httpHeaders })
  }

  update(finca: Finca): Observable<Finca> {
    return this.http.put<Finca>(this.url + 'finca', finca, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Finca> {
    return this.http.delete<Finca>(`${this.url + 'finca'}/${id}`, { headers: this.httpHeaders })
  }

}
