import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-persona',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.css']
})
export class PersonasComponent implements OnInit {
  bodyClasses = 'skin-green sidebar-mini';
  body: HTMLBodyElement = document.getElementsByTagName('body')[0];
  public titulo: string;
  constructor(
    private _route:ActivatedRoute,
    private _router:Router
  ) {
    this.titulo = 'Listado de Productos';
  }

  ngOnInit() {
    console.log('CARGADO');
    this.body.classList.add('skin-green');
    this.body.classList.add('sidebar-mini');
  }

}
