import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Empresa } from "./empresa";
import { EmpresaService } from "./empresa.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
import { TipoContactoService } from "../../clasificadores/tipo-contacto/tipo-contacto.service";
import { TipoDomicilioService } from "../../clasificadores/tipo-domicilio/tipo-domicilio.service";
import { Domicilio } from "../../model/domicilio";
import { Contacto } from "../../model/contacto";
import { TipoDomicilio } from "../../clasificadores/tipo-domicilio/tipo-domicilio";
import { TipoContacto } from "../../clasificadores/tipo-contacto/tipo-contacto";
import { DistritoService } from "../../clasificadores/distrito/distrito.service";
import { Distrito } from "../../clasificadores/distrito/distrito";
//
import { Observable } from "rxjs";
import { FormControl } from "@angular/forms";
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';

@Component({
  selector: "modal-empresa",
  templateUrl: "./modal-empresa.html",
  providers: [EmpresaService, TipoContactoService, TipoDomicilioService, DistritoService]
})
export class ModalEmpresa implements OnInit {
  public empresa: Empresa = new Empresa();
  public domicilio: Domicilio = new Domicilio();
  public contacto: Contacto = new Contacto();
  public titulo: string = "Crear Empresa";
  public isSaving: boolean = false;
  //
  public tipoDomicilios: TipoDomicilio[];
  public tipoContactos: TipoContacto[];
  public distritos: Distrito[];
  //
  filteredDistritos: Observable<any[]>;
  myControl: FormControl = new FormControl();
  valStr:string;

  constructor(
    private empresaService: EmpresaService,
    private tipoContactoService: TipoContactoService,
    private tipoDomicilioService: TipoDomicilioService,
    private distritoService: DistritoService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.distritoService.getDistritos().subscribe(distritos => (this.distritos = distritos));
    this.tipoDomicilioService.getTipoDomicilios().subscribe(tipoDomicilios => (this.tipoDomicilios = tipoDomicilios));
    this.tipoContactoService.getTipoContactos().subscribe(tipoContactos => (this.tipoContactos = tipoContactos));
    /////// Autocomplete ///////////////////////////////
    this.autoDistrito();
    /////// fin autocomplete /////////
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.empresaService.getEmpresa(id).subscribe(empresa => (this.empresa = empresa, 
                                                                this.domicilio = empresa.domicilio, 
                                                                this.contacto = empresa.contactoList[0]));
      }
    });
  }

  compareTipoContacto(t1: TipoContacto, t2: TipoContacto): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  compareTipoDomicilio(t1: TipoDomicilio, t2: TipoDomicilio): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  save(): void {
    this.isSaving = true;
    this.empresa.domicilio = this.domicilio;
    this.empresa.contactoList = [];
    this.empresa.contactoList.push(this.contacto);
    if (typeof this.empresa.id === "undefined" || this.empresa.id === null) {
      this.empresaService.create(this.empresa).subscribe(empresa => {
        this.close();
        //swal('Nuevo Empresa', `Empresa ${empresa.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.empresaService.update(this.empresa).subscribe(empresa => {
        this.close();
        //swal('Empresa Actualizado', `Empresa ${empresa.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["personas/empresas"]);
  }


  ///////// autocomplete /////////////////
  autoDistrito(){
          
    this.filteredDistritos = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filter(val))
      );
  }
  filter(val) {
    if(typeof val.descripcion === 'undefined'){
      this.valStr = val;
    }else{
      this.valStr = val.descripcion;
    }
    if (this.distritos) {
      return this.distritos.filter(distritos =>
        distritos.descripcion.toLowerCase().includes(this.valStr.toLowerCase())
        || distritos.localidad.descripcion.toLowerCase().includes(this.valStr.toLowerCase())
        || distritos.localidad.provincia.descripcion.toLowerCase().includes(this.valStr.toLowerCase())
      );
    }
  }

  public getDisplayFn() {
    return (val) => this.display(val);
 }
  private display(dis): string {
    //access component "this" here
    return dis ? dis.descripcion + " - " + dis.localidad.descripcion + " - " + dis.localidad.provincia.descripcion: dis;
 }

  private selected(dis){
    console.log(dis);
  }

  /////////Fin autocomplete //////////////////////////////////////

}
