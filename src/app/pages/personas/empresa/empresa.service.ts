import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Empresa } from './empresa';

@Injectable()
export class EmpresaService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getEmpresas(): Observable<Empresa[]> {
    return this.http.get(this.url + 'empresas').pipe(
      map(response => response as Empresa[])
    );
  }

  getEmpresa(id: number): Observable<Empresa> {
    return this.http.get<Empresa>(`${this.url + 'empresa'}/${id}`)
  }

  create(empresa: Empresa): Observable<Empresa> {
    return this.http.post<Empresa>(this.url + 'empresa', empresa, { headers: this.httpHeaders })
  }

  update(empresa: Empresa): Observable<Empresa> {
    return this.http.put<Empresa>(this.url + 'empresa', empresa, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Empresa> {
    return this.http.delete<Empresa>(`${this.url + 'empresa'}/${id}`, { headers: this.httpHeaders })
  }

}
