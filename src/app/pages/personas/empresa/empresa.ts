import { ClienteEmpresa } from "../../model/cliente-empresa/cliente-empresa";
import { ListaPrecio } from "../../administracion/lista-precio/lista-precio";
import { Contacto } from "../../model/contacto";
import { Domicilio } from "../../model/domicilio";

export class Empresa {
    public id: number;
    public razonSocial: string;
    public cuit: number;
    public telefono: string;
    public mail: string;
    public contactoReferencia: string;
    public domicilio: Domicilio;
    public contactoList: Contacto[];
    public listaPrecioList: ListaPrecio[];
    public clienteEmpresaList: ClienteEmpresa[];
 
}
