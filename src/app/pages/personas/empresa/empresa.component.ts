import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { EmpresaService } from './empresa.service';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Empresa } from './empresa';
import { FormControl } from "@angular/forms";


@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css'],
  providers: [EmpresaService]
})
export class EmpresaComponent implements OnInit {

  public titulo: String;
  public empresas: Empresa[];
  myEmpresa: FormControl = new FormControl();
  p : number;

  constructor(
    private empresaService: EmpresaService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.empresaService.getEmpresas().subscribe(
      empresas => this.empresas = empresas
    );
  }

}
