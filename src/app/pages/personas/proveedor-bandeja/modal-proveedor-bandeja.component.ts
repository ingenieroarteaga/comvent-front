import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { ProveedorBandeja } from "./proveedor-bandeja";
import { ProveedorBandejaService } from "./proveedor-bandeja.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
import { TipoPersona } from "../../model/tipo-persona/tipo-persona";
import { TipoPersonaService } from "../../model/tipo-persona/tipo-persona.service";
import { TipoDocumentoService } from "../../clasificadores/tipo-documento/tipo-documento.service";
import { TipoDocumento } from "../../clasificadores/tipo-documento/tipo-documento";
import { Domicilio } from "../../model/domicilio";
import { DistritoService } from "../../clasificadores/distrito/distrito.service";
import { TipoDomicilioService } from "../../clasificadores/tipo-domicilio/tipo-domicilio.service";
import { TipoDomicilio } from "../../clasificadores/tipo-domicilio/tipo-domicilio";
import { Distrito } from "../../clasificadores/distrito/distrito";
import { Observable } from "rxjs";
import { FormControl } from "@angular/forms";
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { PersonaDomicilio } from "../../model/persona-domicilio";
import { Persona } from "../../model/persona/persona";
import { TipoContactoService } from "../../clasificadores/tipo-contacto/tipo-contacto.service";
import { TipoContacto } from "../../clasificadores/tipo-contacto/tipo-contacto";
import { Contacto } from "../../model/contacto";

@Component({
  selector: "modal-proveedor-bandeja",
  templateUrl: "./modal-proveedor-bandeja.html",
  providers: [ProveedorBandejaService, TipoPersonaService, TipoDocumentoService, TipoDomicilioService, DistritoService, TipoContactoService]
})
export class ModalProveedorBandeja implements OnInit {
  public proveedorBandeja: ProveedorBandeja = new ProveedorBandeja();
  public titulo: string = "Crear ProveedorBandeja";
  public isSaving: boolean = false;
  public domicilio: Domicilio = new Domicilio();
  public persona: Persona = new Persona();
  public contacto: Contacto = new Contacto();
  public tipoPersonas: TipoPersona[];
  public tipoDocumentos: TipoDocumento[];
  public tipoDomicilios: TipoDomicilio[];
  public distritos: Distrito[];
  public tipoContactos: TipoContacto[];
  //
  filteredDistritos: Observable<any[]>;
  myControl: FormControl = new FormControl();
  valStr:string;
  //
  public lblNombre: String = "Nombre";
  public showTipoPersona: boolean = true;

  constructor(
    private proveedorBandejaService: ProveedorBandejaService,
    private tipoPersonaService: TipoPersonaService,
    private tipoDocumentoService: TipoDocumentoService,
    private tipoDomicilioService: TipoDomicilioService,
    private distritoService: DistritoService,
    private tipoContactoService: TipoContactoService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.tipoPersonaService.getTipoPersonas().subscribe(tipoPersonas => (this.tipoPersonas = tipoPersonas));
    this.tipoDocumentoService.getTipoDocumentos().subscribe(tipoDocumentos => (this.tipoDocumentos = tipoDocumentos));
    this.distritoService.getDistritos().subscribe(distritos => (this.distritos = distritos));
    this.tipoDomicilioService.getTipoDomicilios().subscribe(tipoDomicilios => (this.tipoDomicilios = tipoDomicilios));
    this.tipoContactoService.getTipoContactos().subscribe(tipoContactos => (this.tipoContactos = tipoContactos));
    /////// Autocomplete ///////////////////////////////
    this.autoDistrito();
    /////// fin autocomplete /////////
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.proveedorBandejaService.getProveedorBandeja(id).subscribe(proveedorBandeja => (this.proveedorBandeja = proveedorBandeja,
                                                                                            this.persona = proveedorBandeja.persona,
                                                                                            this.domicilio = proveedorBandeja.persona.personaDomicilioList[0].domicilio,
                                                                                            this.contacto = proveedorBandeja.persona.contactoList[0],
                                                                                            this.changeTipoPersona()));
      }
    });
  }

  compareTipoPersona(t1: TipoPersona, t2: TipoPersona): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  compareTipoDocumento(t1: TipoDocumento, t2: TipoDocumento): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  compareTipoDomicilio(t1: TipoDomicilio, t2: TipoDomicilio): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  compareTipoContacto(t1: TipoContacto, t2: TipoContacto): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  save(): void {
    //this.isSaving = true;
    this.proveedorBandeja.persona = this.persona;
    var domiList: PersonaDomicilio = new PersonaDomicilio();
    domiList.domicilio = this.domicilio;
    this.proveedorBandeja.persona.personaDomicilioList = [];
    this.proveedorBandeja.persona.personaDomicilioList.push(domiList);
    //
    this.proveedorBandeja.persona.contactoList = [];
    this.proveedorBandeja.persona.contactoList.push(this.contacto);

    if (typeof this.proveedorBandeja.id === "undefined" || this.proveedorBandeja.id === null) {
      this.proveedorBandejaService.create(this.proveedorBandeja).subscribe(proveedorBandeja => {
        this.close();
        //swal('Nuevo ProveedorBandeja', `ProveedorBandeja ${proveedor-bandeja.descripcion} creado con éxito!`, 'success');
        this.refresh();
      });
    } else {
      this.proveedorBandejaService.update(this.proveedorBandeja).subscribe(proveedorBandeja => {
        this.close();
        //swal('ProveedorBandeja Actualizado', `ProveedorBandeja ${proveedor-bandeja.descripcion} actualizado con éxito!`, 'success');
        this.refresh();
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["personas/proveedores-bandejas"]);
  }


  ////////// Show Tipo de persona /////////
  changeTipoPersona(){
    if(this.persona.tipoPersona.id === 1){
      this.lblNombre = "Nombre";
      this.showTipoPersona = true;
    }else{
      this.lblNombre = "Razón Social";
      this.showTipoPersona = false;
    }
  }

  ///////// autocomplete /////////////////
  autoDistrito(){
          
    this.filteredDistritos = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filter(val))
      );
  }
  filter(val) {
    if(typeof val.descripcion === 'undefined'){
      this.valStr = val;
    }else{
      this.valStr = val.descripcion;
    }
    if (this.distritos) {
      return this.distritos.filter(distritos =>
        distritos.descripcion.toLowerCase().includes(this.valStr.toLowerCase())
        || distritos.localidad.descripcion.toLowerCase().includes(this.valStr.toLowerCase())
        || distritos.localidad.provincia.descripcion.toLowerCase().includes(this.valStr.toLowerCase())
      );
    }
  }

  public getDisplayFn() {
    return (val) => this.display(val);
 }
  private display(dis): string {
    //access component "this" here
    return dis ? dis.descripcion : dis;
 }

  private selected(dis){
    console.log(dis);
  }

  /////////Fin autocomplete //////////////////////////////////////

}
