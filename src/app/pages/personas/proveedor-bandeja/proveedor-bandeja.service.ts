import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ProveedorBandeja } from './proveedor-bandeja';

@Injectable()
export class ProveedorBandejaService {
  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getProveedorBandejas(): Observable<ProveedorBandeja[]> {
    return this.http.get(this.url + 'proveedores-bandejas').pipe(
      map(response => response as ProveedorBandeja[])
    );
  }

  getProveedorBandeja(id: number): Observable<ProveedorBandeja> {
    return this.http.get<ProveedorBandeja>(`${this.url + 'proveedor-bandeja'}/${id}`)
  }

  create(proveedorBandeja: ProveedorBandeja): Observable<ProveedorBandeja> {
    return this.http.post<ProveedorBandeja>(this.url + 'proveedor-bandeja', proveedorBandeja, { headers: this.httpHeaders })
  }

  update(proveedorBandeja: ProveedorBandeja): Observable<ProveedorBandeja> {
    return this.http.put<ProveedorBandeja>(this.url + 'proveedor-bandeja', proveedorBandeja, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<ProveedorBandeja> {
    return this.http.delete<ProveedorBandeja>(`${this.url + 'proveedor-bandeja'}/${id}`, { headers: this.httpHeaders })
  }

  getProveedorBandejaByCuit(cuit: number): Observable<ProveedorBandeja> {
    return this.http.get<ProveedorBandeja>(`${this.url + 'proveedor-bandeja-cuit'}/${cuit}`)
  }

}
