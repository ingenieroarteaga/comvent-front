import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ProveedorBandejaService } from './proveedor-bandeja.service';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProveedorBandeja } from './proveedor-bandeja';
import { FormControl } from "@angular/forms";


@Component({
  selector: 'app-proveedor-bandeja',
  templateUrl: './proveedor-bandeja.component.html',
  styleUrls: ['./proveedor-bandeja.component.css'],
  providers: [ProveedorBandejaService]
})
export class ProveedorBandejaComponent implements OnInit {
  public titulo: String;
  public proveedorBandejas: ProveedorBandeja[];
  myProveedorBandeja: FormControl = new FormControl();
  p : number;


  constructor(
    private proveedorBandejaService: ProveedorBandejaService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.proveedorBandejaService.getProveedorBandejas().subscribe(
      proveedorBandejas => this.proveedorBandejas = proveedorBandejas
    );
  }
}
