import { Persona } from "../../model/persona/persona";
import { Bandeja } from "../../herramienta/bandeja/bandeja";

export class ProveedorBandeja {
    public id: number;
    public persona: Persona;
    public bandejaList: Bandeja[];
    
}
