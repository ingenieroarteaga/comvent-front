import { TestBed, inject } from '@angular/core/testing';

import { ProveedorBandejaService } from './proveedor-bandeja.service';

describe('ProveedorBandejaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProveedorBandejaService]
    });
  });

  it('should be created', inject([ProveedorBandejaService], (service: ProveedorBandejaService) => {
    expect(service).toBeTruthy();
  }));
});
