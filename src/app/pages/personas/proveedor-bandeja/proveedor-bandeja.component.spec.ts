import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProveedorBandejaComponent } from './proveedor-bandeja.component';

describe('ProveedorBandejaComponent', () => {
  let component: ProveedorBandejaComponent;
  let fixture: ComponentFixture<ProveedorBandejaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProveedorBandejaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProveedorBandejaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
