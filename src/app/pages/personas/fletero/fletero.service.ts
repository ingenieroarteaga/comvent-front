import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Fletero } from './fletero';

@Injectable()
export class FleteroService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getFleteros(): Observable<Fletero[]> {
    return this.http.get(this.url + 'fleteros').pipe(
      map(response => response as Fletero[])
    );
  }

  getFletero(id: number): Observable<Fletero> {
    return this.http.get<Fletero>(`${this.url + 'fletero'}/${id}`)
  }

  create(fletero: Fletero): Observable<Fletero> {
    return this.http.post<Fletero>(this.url + 'fletero', fletero, { headers: this.httpHeaders })
  }

  update(fletero: Fletero): Observable<Fletero> {
    return this.http.put<Fletero>(this.url + 'fletero', fletero, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Fletero> {
    return this.http.delete<Fletero>(`${this.url + 'fletero'}/${id}`, { headers: this.httpHeaders })
  }

  getFleteroByCuit(cuit: number): Observable<Fletero> {
    return this.http.get<Fletero>(`${this.url + 'fletero-cuit'}/${cuit}`)
  }

}
