import { Persona } from "../../model/persona/persona";
import { CuentaFletero } from "../../administracion/cuenta-fletero/cuenta-fletero";
import { CuentaFleteroBand } from "../../administracion/cuenta-fletero-band/cuenta-fletero-band";
import { Remito } from "../../produccion/remito/remito";

export class Fletero {
    public id: number;
    public persona: Persona;
    public remitoList: Remito[];
    public cuentaFleteroBandList: CuentaFleteroBand[];
    public cuentaFleteroList: CuentaFletero[];
 
}
