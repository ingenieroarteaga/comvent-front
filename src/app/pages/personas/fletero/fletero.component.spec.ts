import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FleteroComponent } from './fletero.component';

describe('FleteroComponent', () => {
  let component: FleteroComponent;
  let fixture: ComponentFixture<FleteroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FleteroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FleteroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
