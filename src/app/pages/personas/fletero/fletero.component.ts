import { ActivatedRoute, Params, Router, RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FleteroService } from './fletero.service';
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Fletero } from './fletero';
import { FormControl } from "@angular/forms";

@Component({
  selector: 'app-fletero',
  templateUrl: './fletero.component.html',
  styleUrls: ['./fletero.component.css'],
  providers: [FleteroService]
})
export class FleteroComponent implements OnInit {
  public titulo: String;
  public fleteros: Fletero[];
  myFletero: FormControl = new FormControl();
  p : number;

  constructor(
    private fleteroService: FleteroService,
    private modalService: NgbModal,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.fleteroService.getFleteros().subscribe(
      fleteros => this.fleteros = fleteros
    );
  }

}