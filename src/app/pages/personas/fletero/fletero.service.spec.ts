import { TestBed, inject } from '@angular/core/testing';

import { FleteroService } from './fletero.service';

describe('FleteroService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FleteroService]
    });
  });

  it('should be created', inject([FleteroService], (service: FleteroService) => {
    expect(service).toBeTruthy();
  }));
});
