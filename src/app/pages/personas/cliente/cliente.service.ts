import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Cliente } from './cliente';


@Injectable()
export class ClienteService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getClientes(): Observable<Cliente[]> {
    return this.http.get(this.url + 'clientes').pipe(
      map(response => response as Cliente[])
    );
  }

  getCliente(id: number): Observable<Cliente> {
    return this.http.get<Cliente>(`${this.url + 'cliente'}/${id}`)
  }
 
  create(cliente: Cliente): Observable<Cliente> {
    return this.http.post<Cliente>(this.url + 'cliente', cliente, { headers: this.httpHeaders })
  }

  update(cliente: Cliente): Observable<Cliente> {
    return this.http.put<Cliente>(this.url + 'cliente', cliente, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Cliente> {
    return this.http.delete<Cliente>(`${this.url + 'cliente'}/${id}`, { headers: this.httpHeaders })
  }

  getClienteByCuit(cuit: number): Observable<Cliente> {
    return this.http.get<Cliente>(`${this.url + 'cliente-cuit'}/${cuit}`)
  }

}
