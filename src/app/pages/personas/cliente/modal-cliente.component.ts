import { GLOBAL } from "../../global";
import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Cliente } from "./cliente";
import { ClienteService } from "./cliente.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import swal from "sweetalert2";
import { TipoPersona } from "../../model/tipo-persona/tipo-persona";
import { TipoPersonaService } from "../../model/tipo-persona/tipo-persona.service";
import { TipoDocumentoService } from "../../clasificadores/tipo-documento/tipo-documento.service";
import { TipoDocumento } from "../../clasificadores/tipo-documento/tipo-documento";
import { Domicilio } from "../../model/domicilio";
import { DistritoService } from "../../clasificadores/distrito/distrito.service";
import { TipoDomicilioService } from "../../clasificadores/tipo-domicilio/tipo-domicilio.service";
import { TipoDomicilio } from "../../clasificadores/tipo-domicilio/tipo-domicilio";
import { Distrito } from "../../clasificadores/distrito/distrito";
import { Observable } from "rxjs";
import { FormControl } from "@angular/forms";
import { startWith } from "rxjs/operators/startWith";
import { map } from "rxjs/operators/map";
import { PersonaDomicilio } from "../../model/persona-domicilio";
import { Persona } from "../../model/persona/persona";
import { EmpresaService } from "../empresa/empresa.service";
import { Empresa } from "../empresa/empresa";
import { ClienteEmpresa } from "../../model/cliente-empresa/cliente-empresa";
import { forEach } from "@angular/router/src/utils/collection";

@Component({
  selector: "modal-cliente",
  templateUrl: "./modal-cliente.html",
  providers: [
    ClienteService,
    TipoPersonaService,
    TipoDocumentoService,
    TipoDomicilioService,
    DistritoService,
    EmpresaService
  ]
})
export class ModalCliente implements OnInit {
  public cliente: Cliente = new Cliente();
  public titulo: string = "Crear Cliente";
  public isSaving: boolean = false;
  public domicilio: Domicilio = new Domicilio();
  public persona: Persona = new Persona();
  public empresa: Empresa = new Empresa();
  public tipoPersonas: TipoPersona[];
  public tipoDocumentos: TipoDocumento[];
  public tipoDomicilios: TipoDomicilio[];
  public distritos: Distrito[];
  public empresas: Empresa[];
  //
  filteredDistritos: Observable<any[]>;
  myControl: FormControl = new FormControl();
  valStr: string;
  //
  filteredEmpresas: Observable<any[]>;
  myControlEmp: FormControl = new FormControl();
  valStrEmp: string;
  public selectedClienteEmpresas: ClienteEmpresa[] = new Array();
  //
  public lblNombre: String = "Nombre";
  public showTipoPersona: boolean = true;

  constructor(
    private clienteService: ClienteService,
    private tipoPersonaService: TipoPersonaService,
    private tipoDocumentoService: TipoDocumentoService,
    private tipoDomicilioService: TipoDomicilioService,
    private distritoService: DistritoService,
    private empresaService: EmpresaService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.tipoPersonaService
      .getTipoPersonas()
      .subscribe(tipoPersonas => (this.tipoPersonas = tipoPersonas));
    this.tipoDocumentoService
      .getTipoDocumentos()
      .subscribe(tipoDocumentos => (this.tipoDocumentos = tipoDocumentos));
    this.distritoService
      .getDistritos()
      .subscribe(distritos => (this.distritos = distritos));
    this.tipoDomicilioService
      .getTipoDomicilios()
      .subscribe(tipoDomicilios => (this.tipoDomicilios = tipoDomicilios));
    this.empresaService
      .getEmpresas()
      .subscribe(empresas => (this.empresas = empresas));
    /////// Autocomplete ///////////////////////////////
    this.autoDistrito();
    this.autoEmpresa();
    /////// fin autocomplete /////////
    this.cargar();
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params["id"];
      if (id) {
        this.clienteService
          .getCliente(id)
          .subscribe(
            cliente => (
              (this.cliente = cliente),
              (this.domicilio =
                cliente.persona.personaDomicilioList[0].domicilio),
              (this.persona = cliente.persona),
              (this.selectedClienteEmpresas = cliente.clienteEmpresaList),
              this.changeTipoPersona()
            )
          );
      }
    });
  }

  compareTipoPersona(t1: TipoPersona, t2: TipoPersona): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  compareTipoDocumento(t1: TipoDocumento, t2: TipoDocumento): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  compareTipoDomicilio(t1: TipoDomicilio, t2: TipoDomicilio): boolean {
    return t1 && t2 ? t1.id === t2.id : t1 === t2;
  }

  /////////////////// SAVE //////////////////////////////////////////////////////////////
  save(): void {
    if (this.selectedClienteEmpresas.length > 0) {
      this.isSaving = true;
      this.cliente.persona = this.persona;
      /// domicilio
      var domiList: PersonaDomicilio = new PersonaDomicilio();
      domiList.domicilio = this.domicilio;
      this.cliente.persona.personaDomicilioList = [];
      this.cliente.persona.personaDomicilioList.push(domiList);
      /// empresas
      this.cliente.clienteEmpresaList = this.selectedClienteEmpresas;

      if (typeof this.cliente.id === "undefined" || this.cliente.id === null) {
        this.clienteService.create(this.cliente).subscribe(cliente => {
          this.close();
          //swal('Nuevo Cliente', `Cliente ${cliente.descripcion} creado con éxito!`, 'success');
          this.refresh();
        });
      } else {
        this.clienteService.update(this.cliente).subscribe(cliente => {
          this.close();
          //swal('Cliente Actualizado', `Cliente ${cliente.descripcion} actualizado con éxito!`, 'success');
          this.refresh();
        });
      }
    } else {
      swal("Cliente", `Debe completar todos los campos obligatorios`, "error");
    }
  }
  //////////////////////// FIN SAVE ///////////////////////////////////////////////////

  refresh(): void {
    window.location.reload();
  }

  onKeydown(event) {
    if (event.key === "Escape") {
      this.close();
    }
  }

  close(): void {
    this.router.navigate(["personas/clientes"]);
  }

  ////////// Show Tipo de persona /////////
  changeTipoPersona() {
    if (this.persona.tipoPersona.id === 1) {
      this.lblNombre = "Nombre";
      this.showTipoPersona = true;
    } else {
      this.lblNombre = "Razón Social";
      this.showTipoPersona = false;
    }
  }

  ///////// autocomplete /////////////////
  autoDistrito() {
    this.filteredDistritos = this.myControl.valueChanges.pipe(
      startWith(""),
      map(val => this.filter(val))
    );
  }
  filter(val) {
    if (typeof val.descripcion === "undefined") {
      this.valStr = val;
    } else {
      this.valStr = val.descripcion;
    }
    if (this.distritos) {
      return this.distritos.filter(
        distritos =>
          distritos.descripcion
            .toLowerCase()
            .includes(this.valStr.toLowerCase()) ||
          distritos.localidad.descripcion
            .toLowerCase()
            .includes(this.valStr.toLowerCase()) ||
          distritos.localidad.provincia.descripcion
            .toLowerCase()
            .includes(this.valStr.toLowerCase())
      );
    }
  }

  public getDisplayFn() {
    return val => this.display(val);
  }
  private display(dis): string {
    //access component "this" here
    return dis ? dis.descripcion + " - " + dis.localidad.descripcion + " - " + dis.localidad.provincia.descripcion: dis;
  }

  private selected(dis) {
    console.log(dis);
  }

  /////////Fin autocomplete //////////////////////////////////////

  ///////// autocomplete Empresas /////////////////
  autoEmpresa() {
    this.filteredEmpresas = this.myControlEmp.valueChanges.pipe(
      startWith(""),
      map(val => this.filterEmp(val))
    );
  }
  filterEmp(val) {
    if (typeof val.razonSocial === "undefined") {
      this.valStrEmp = val;
    } else {
      this.valStrEmp = val.razonSocial;
    }
    if (this.empresas) {
      return this.empresas.filter(
        empresas =>
          empresas.razonSocial
            .toLowerCase()
            .includes(this.valStrEmp.toLowerCase()) ||
          empresas.cuit
            .toString()
            .toLowerCase()
            .includes(this.valStrEmp.toLowerCase())
      );
    }
  }

  public getDisplayFnEmp() {
    return val => this.displayEmp(val);
  }
  private displayEmp(emp): string {
    //access component "this" here
    var v_clienteEmpresa: ClienteEmpresa = new ClienteEmpresa();
    v_clienteEmpresa.empresa = emp;
    v_clienteEmpresa.fechaAlta = new Date();
    if (
      typeof emp !== "undefined" &&
      emp !== null &&
      typeof emp.id !== "undefined"
    ) {
      this.selectedClienteEmpresas.push(v_clienteEmpresa);
    }
    //return emp ? emp.cuit + " - " + emp.razonSocial : emp;
    return null;
  }

  private selectedEmp(emp) {
    console.log(emp);
  }

  /// propio de la lista
  public removeEmpresa(index) {
    if (
      typeof this.selectedClienteEmpresas !== "undefined" &&
      this.selectedClienteEmpresas !== null &&
      this.selectedClienteEmpresas.length > 0
    ) {
      this.selectedClienteEmpresas.splice(index, 1);
    }
  }

  /////////Fin autocomplete Empresas //////////////////////////////////////
}
