import { Persona } from "../../model/persona/persona";
import { Finca } from "../finca/finca";
import { ClienteEmpresa } from "../../model/cliente-empresa/cliente-empresa";

export class Cliente {
    public id: number;
    public persona: Persona;
    public clienteEmpresaList: ClienteEmpresa[];
    public fincaList: Finca[];
 
}
