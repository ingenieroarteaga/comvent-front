import { Variedad } from "../../herramienta/variedad/variedad";
import { Persona } from "../../model/persona/persona";
import { Compra } from "../../produccion/compra/compra";

export class Proveedor {
    public id: number;
    public variedadList: Variedad[];
    public compraList: Compra[];
    public persona: Persona;
 
}
