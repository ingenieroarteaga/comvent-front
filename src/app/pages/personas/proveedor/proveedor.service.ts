import { GLOBAL } from '../../global';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Proveedor } from './proveedor';

@Injectable()
export class ProveedorService {

  private url: string;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })



  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getProveedores(): Observable<Proveedor[]> {
    return this.http.get(this.url + 'proveedores').pipe(
      map(response => response as Proveedor[])
    );
  }

  getProveedor(id: number): Observable<Proveedor> {
    return this.http.get<Proveedor>(`${this.url + 'proveedor'}/${id}`)
  }

  create(proveedor: Proveedor): Observable<Proveedor> {
    return this.http.post<Proveedor>(this.url + 'proveedor', proveedor, { headers: this.httpHeaders })
  }

  update(proveedor: Proveedor): Observable<Proveedor> {
    return this.http.put<Proveedor>(this.url + 'proveedor', proveedor, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Proveedor> {
    return this.http.delete<Proveedor>(`${this.url + 'proveedor'}/${id}`, { headers: this.httpHeaders })
  }

  getProveedorByCuit(cuit: number): Observable<Proveedor> {
    return this.http.get<Proveedor>(`${this.url + 'proveedor-cuit'}/${cuit}`)
  }

}
