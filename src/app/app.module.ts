import { APP_ROUTES } from './app.routes';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { PagesModule } from './pages/pages.module';
import { PAGES_ROUTES } from './pages/pages.routes';
import { FormsModule } from '@angular/forms';
import { NgbModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from './auth/auth.service';
import { TokenStorage } from './auth/token.storage';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Interceptor } from './auth/inteceptor';
import { UsuarioService } from './auth/usuario.service';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
   
    
   

  ],
  imports: [
    BrowserModule,
    APP_ROUTES,
    PagesModule,
    PAGES_ROUTES,
    FormsModule,
    NgbModule.forRoot(), 

  ],
  providers: [NgbModal, AuthService, TokenStorage, UsuarioService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
