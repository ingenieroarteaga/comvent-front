import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
  name: "filterPedidoDetalle"
})
export class FilterPedidoDetallePipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if (!items) return [];
    if (!searchText) return items;
    searchText = searchText.toString().toLowerCase();
    return items.filter(it => {
      return it.pedido.clienteEmpresa.cliente.persona.cuit.toString().toLowerCase().includes(searchText) ||
          it.pedido.clienteEmpresa.cliente.persona.nombre.toString().toLowerCase().includes(searchText) ||
          (
            it.pedido.clienteEmpresa.cliente.persona.apellido !== null &&
            it.pedido.clienteEmpresa.cliente.persona.apellido.toString().toLowerCase().includes(searchText)
          ) ||
          it.pedido.clienteEmpresa.empresa.cuit.toString().toLowerCase().includes(searchText) ||
          it.pedido.clienteEmpresa.empresa.razonSocial.toString().toLowerCase().includes(searchText) ||
          it.variedad.descripcion.toString().toLowerCase().includes(searchText) ||
          it.bandejaStock.bandeja.capacidad.toString().toLowerCase().includes(searchText)
          ;
      ;
    });
  }
}
