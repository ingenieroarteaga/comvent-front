import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: "filterProvincia"
})
export class FilterProvincia implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toString().toLowerCase();
        return items.filter(it => {
            return it.pais.descripcion.toString().toLowerCase().includes(searchText) ||
                it.descripcion.toString().toLowerCase().includes(searchText);
        });
    }
}