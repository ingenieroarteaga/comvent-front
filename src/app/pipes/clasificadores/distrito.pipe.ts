import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: "filterDistrito"
})
export class FilterDistrito implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toString().toLowerCase();
        return items.filter(it => {
            return it.localidad.provincia.pais.descripcion.toString().toLowerCase().includes(searchText) ||
                it.localidad.provincia.descripcion.toString().toLowerCase().includes(searchText) ||
                it.localidad.descripcion.toString().toLowerCase().includes(searchText) ||
                it.localidad.zona.descripcion.toString().toLowerCase().includes(searchText) ||
                it.descripcion.toString().toLowerCase().includes(searchText) ||
                it.codigoPostal.toString().toLowerCase().includes(searchText);
        });
    }
}