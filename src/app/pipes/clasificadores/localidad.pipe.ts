import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: "filterLocalidad"
})
export class FilterLocalidad implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toString().toLowerCase();
        return items.filter(it => {
            return it.provincia.pais.descripcion.toString().toLowerCase().includes(searchText) ||
                it.provincia.descripcion.toString().toLowerCase().includes(searchText) ||
                it.descripcion.toString().toLowerCase().includes(searchText) ||
                it.zona.descripcion.toString().toLowerCase().includes(searchText);
        });
    }
}