import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: "filterSucursal"
})
export class FilterSucursal implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toString().toLowerCase();
        return items.filter(it => {
            return it.entidad.nombre.toString().toLowerCase().includes(searchText) ||
                it.descripcion.toString().toLowerCase().includes(searchText) ||
                it.mail.toString().toLowerCase().includes(searchText) ||
                it.telefono.toString().toLowerCase().includes(searchText) ||
                it.domicilio.calle.toString().toLowerCase().includes(searchText);
        });
    }
}