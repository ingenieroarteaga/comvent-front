import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: "filterCalendario"
})
export class FilterCalendario implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toString().toLowerCase();
      
            return items.filter(it => {
                return it.periodo.toString().toLowerCase().includes(searchText) ||
                    it.desde.toString().toLowerCase().includes(searchText) ||
                    it.hasta.toString().toLowerCase().includes(searchText);
            });
    }
}