import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: "filterInvernadero"
})
export class FilterInvernadero implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toString().toLowerCase();
        return items.filter(it => {
            return it.sucursal.descripcion.toString().toLowerCase().includes(searchText) ||
                it.descripcion.toString().toLowerCase().includes(searchText) ||
                (
                    it.capacidad !== null &&
                    it.capacidad.toString().toLowerCase().includes(searchText)
                ) ||
                it.observacion.toString().toLowerCase().includes(searchText);
        });
    }
}