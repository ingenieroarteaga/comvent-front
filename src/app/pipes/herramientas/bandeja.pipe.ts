import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: "filterBandeja"
})
export class FilterBandeja implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toString().toLowerCase();
        return items.filter(it => {
            return it.tipoBandeja.descripcion.toString().toLowerCase().includes(searchText) ||
                it.descripcion.toString().toLowerCase().includes(searchText) ||
                it.periodo.toString().toLowerCase().includes(searchText) ||
                it.proveedorBandeja.persona.cuit.toString().toLowerCase().includes(searchText) ||
                it.proveedorBandeja.persona.nombre.toString().toLowerCase().includes(searchText) ||
                it.proveedorBandeja.persona.apellido.toString().toLowerCase().includes(searchText);

        });
    }
}