import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: "filterVariedad"
})
export class FilterVariedad implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toString().toLowerCase();
        return items.filter(it => {
            return it.especie.descripcion.toString().toLowerCase().includes(searchText) ||
                it.descripcion.toString().toLowerCase().includes(searchText) ||
                it.proveedor.persona.cuit.toString().toLowerCase().includes(searchText) ||
                it.proveedor.persona.nombre.toString().toLowerCase().includes(searchText) ||
                (
                    it.proveedor.persona.apellido !== null &&
                    it.proveedor.persona.apellido.toString().toLowerCase().includes(searchText)
                ) ||
                (
                    it.mercado !== null &&
                    it.mercado.toString().toLowerCase().includes(searchText)
                ) ||
                it.clasificacion.toString().toLowerCase().includes(searchText);

        });
    }
}