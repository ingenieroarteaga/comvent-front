import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: "filterEmpresa"
})
export class FilterEmpresa implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toString().toLowerCase();
        return items.filter(it => {
            return it.cuit.toString().toLowerCase().includes(searchText) ||
                it.razonSocial.toString().toLowerCase().includes(searchText) ||
                it.telefono.toString().toLowerCase().includes(searchText) ||
                it.mail.toString().toLowerCase().includes(searchText) ||
                it.domicilio.calle.toString().toLowerCase().includes(searchText) ||
                it.domicilio.numero.toString().toLowerCase().includes(searchText);
        });
    }
}