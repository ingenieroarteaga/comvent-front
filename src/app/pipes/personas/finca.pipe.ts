import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: "filterFinca"
})
export class FilterFinca implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toString().toLowerCase();
        return items.filter(it => {
            return it.cliente.persona.cuit.toString().toLowerCase().includes(searchText) ||
                it.cliente.persona.nombre.toString().toLowerCase().includes(searchText) ||
                it.cliente.persona.apellido.toString().toLowerCase().includes(searchText) ||
                it.domicilio.calle.toString().toLowerCase().includes(searchText) ||
                it.descripcion.toString().toLowerCase().includes(searchText) ||
                it.telefono.toString().toLowerCase().includes(searchText);
        });
    }
}