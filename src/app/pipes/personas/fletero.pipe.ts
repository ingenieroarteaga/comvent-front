import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: "filterFletero"
})
export class FilterFletero implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toString().toLowerCase();
        return items.filter(it => {
            return it.persona.tipoPersona.descripcion.toString().toLowerCase().includes(searchText) ||
                it.persona.cuit.toString().toLowerCase().includes(searchText) ||
                it.persona.nombre.toString().toLowerCase().includes(searchText) ||
                it.persona.apellido.toString().toLowerCase().includes(searchText) ||
                it.persona.telefono.toString().toLowerCase().includes(searchText);
        });
    }
}