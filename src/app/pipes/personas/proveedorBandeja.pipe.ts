import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: "filterProveedorBandeja"
})
export class FilterProveedorBandeja implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toString().toLowerCase();
        return items.filter(it => {
            return it.persona.tipoPersona.descripcion.toString().toLowerCase().includes(searchText) ||
                it.persona.cuit.toString().toLowerCase().includes(searchText) ||
                it.persona.nombre.toString().toLowerCase().includes(searchText) ||
                it.persona.apellido.toString().toLowerCase().includes(searchText) ||
                (
                    it.persona.telefono !== null &&
                    it.persona.telefono.toString().toLowerCase().includes(searchText)
                );
        });
    }
}