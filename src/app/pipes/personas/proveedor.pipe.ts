import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: "filterProveedor"
})
export class FilterProveedor implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toString().toLowerCase();
        return items.filter(it => {
            return it.persona.tipoPersona.descripcion.toString().toLowerCase().includes(searchText) ||
                it.persona.cuit.toString().toLowerCase().includes(searchText) ||
                it.persona.nombre.toString().toLowerCase().includes(searchText) ||
                (
                    it.persona !== null &&
                    it.persona.toString().toLowerCase().includes(searchText)
                );
        });
    }
}