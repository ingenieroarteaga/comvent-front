import { Usuario } from './usuario';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { GLOBAL } from '../pages/global';


@Injectable()
export class UsuarioService {
    public url: string;

    /* private urlEndPoint: string = 'http://192.168.48.23:8080/usuarios';
    private urlGuardarUsuario: string = 'http://192.168.48.23:8080/usuario'; */

    private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

    constructor(private http: HttpClient
    ) {
        this.url = GLOBAL.url;
    }

    getUsuarios(): Observable<Usuario[]> {
        //retorna (UsuarioS);
        return this.http.get(this.url + 'usuarios').pipe(
            map(response => response as Usuario[])
        );
    }

    create(usuario: Usuario): Observable<Usuario> {
        return this.http.post<Usuario>(this.url + 'usuario', usuario, { headers: this.httpHeaders })
    }

    getUsuario(id): Observable<Usuario> {
        return this.http.get<Usuario>(`${this.url + 'usuario'}/${id}`)
    }

    update(usuario: Usuario): Observable<Usuario> {
        return this.http.put<Usuario>(`${this.url + 'usuarios'}/${usuario.id}`, usuario, { headers: this.httpHeaders })
    }

    delete(id: number): Observable<Usuario> {
        return this.http.delete<Usuario>(`${this.url + 'usuarios'}/${id}`, { headers: this.httpHeaders })
    }

}
