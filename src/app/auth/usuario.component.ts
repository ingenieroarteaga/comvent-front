import { Component, OnInit } from '@angular/core';
import { Usuario } from './usuario';
import { UsuarioService } from './usuario.service';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

export class UsuarioComponent implements OnInit {
    usuarios: Usuario[];
    public usuario: Usuario = new Usuario();

    constructor(public usuarioService: UsuarioService,
        public modalService: NgbModal,
        public activatedRoute: ActivatedRoute
    ) { }

    ngOnInit() {
        this.usuarioService.getUsuarios().subscribe(
            usuarios => this.usuarios = usuarios
        );

    }
    cargarUsuario(): void {
        this.activatedRoute.params.subscribe(params => {
            let id = params['id']
            if (id) {
                this.usuarioService.getUsuario(id).subscribe((usuario) => this.usuario = usuario)
            }
        })
    }




}



