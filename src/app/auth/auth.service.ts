import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GLOBAL } from '../pages/global';
import { map } from 'rxjs/operators';


@Injectable()
export class AuthService {
  public url: string;


  // baseUrl: 'http://192.168.0.110:8080/email2sms/';
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })
  constructor(private http: HttpClient) {

    this.url = GLOBAL.url;
    
  }


  attemptAuth(username: string, password: string): Observable<any> {
    const credentials = { username: username, password: password };
    console.log('attempAuth ::');
    return this.http.post<any>(this.url + 'token/generate-token', credentials);
  }



}
